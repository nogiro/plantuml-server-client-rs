#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"

# shellcheck source=utility/shared/shared.sh
source "${SCRIPT_DIR}/shared.sh"

if [ "$(current_versions "${SCRIPT_DIR}/testdata/")" != '0.5.1' ]; then
  echo 'test failed: current_versions' >&2
  exit 1
fi
