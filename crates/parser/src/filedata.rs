use crate::Error;
use crate::ParseContainer;
use crate::PlantUmlContent;
use crate::{IncludeToken, PlantUmlLine};
use std::sync::Arc;

/// PlantUML diagrams in the file.
///
/// # Examples
///
/// ```
/// use plantuml_parser::PlantUmlFileData;
///
/// # fn main() -> anyhow::Result<()> {
/// let data = r#"
/// @startuml diagram_0
/// Alice -> Bob: Hello
/// @enduml
///
/// @startuml
/// Bob -> Alice: Hi
/// @enduml
///
/// @startuml diagram_2
/// Alice -> Bob: How are you?
/// @enduml
/// "#;
///
/// let filedata = PlantUmlFileData::parse_from_str(data)?;
/// assert_eq!(filedata.is_empty(), false);
/// assert_eq!(filedata.len(), 3);
/// assert_eq!(
///     filedata.iter().map(|x| x.inner()).collect::<Vec<_>>(),
///     [
///         "Alice -> Bob: Hello\n",
///         "Bob -> Alice: Hi\n",
///         "Alice -> Bob: How are you?\n",
///     ]
/// );
///
/// let content_1 = filedata.get(1).unwrap();
/// assert_eq!(content_1.inner(), "Bob -> Alice: Hi\n");
/// let content_0 = filedata.get_by_id("diagram_0").unwrap();
/// assert_eq!(content_0.inner(), "Alice -> Bob: Hello\n");
/// let content_2 = filedata.get_by_id("diagram_2").unwrap();
/// assert_eq!(content_2.inner(), "Alice -> Bob: How are you?\n");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct PlantUmlFileData {
    contents: Vec<PlantUmlContent>,
}

impl PlantUmlFileData {
    /// Creates a new [`PlantUmlFileData`] from the string.
    ///
    /// * `input` - a string to be parsed.
    pub fn parse_from_str<S>(input: S) -> Result<Self, Error>
    where
        S: Into<String>,
    {
        let input = ParseContainer::new(Arc::new(input.into()));
        Self::parse(input)
    }

    /// Creates a new [`PlantUmlFileData`] from the inner type ([`ParseContainer`]).
    ///
    /// * `input` - a string to be parsed.
    pub fn parse(input: ParseContainer) -> Result<Self, Error> {
        let mut contents = vec![];
        let mut input = input;
        let (mut rest, (_, mut line)) = PlantUmlLine::parse(input.clone())?;

        'outer: while !rest.is_empty() {
            while line.empty().is_some() {
                (rest, (_, line)) = PlantUmlLine::parse(input.clone())?;
                if line.empty().is_none() {
                    break;
                }
                if rest.is_empty() {
                    break 'outer;
                }
                input = rest;
            }

            let (tmp_rest, content) = PlantUmlContent::parse(input)?;
            input = tmp_rest;
            contents.push(content);
            if input.is_empty() {
                break;
            }
            (rest, (_, line)) = PlantUmlLine::parse(input.clone())?;
        }

        Ok(Self { contents })
    }

    /// Returns the one PlantUML diagram specified by the token.
    ///
    /// * `token` - A [`IncludeToken`] to get [`PlantUmlContent`] in the [`PlantUmlFileData`].
    pub fn get_by_token(&self, token: &IncludeToken) -> Option<&PlantUmlContent> {
        if let Some(content) = token.id().and_then(|id| self.get_by_id(id)) {
            Some(content)
        } else if let Some(content) = token.index().and_then(|index| self.get(index)) {
            Some(content)
        } else {
            self.get(0)
        }
    }

    /// Returns the reference to [`PlantUmlContent`] of index.
    pub fn get(&self, index: usize) -> Option<&PlantUmlContent> {
        self.contents.get(index)
    }

    /// Returns the reference to [`PlantUmlContent`] of `id`.
    pub fn get_by_id(&self, id: &str) -> Option<&PlantUmlContent> {
        self.contents.iter().find(|x| x.id() == Some(id))
    }

    /// Returns an iterator visiting all [`PlantUmlContent`] in the [`PlantUmlFileData`].
    pub fn iter(&self) -> std::slice::Iter<'_, PlantUmlContent> {
        self.contents.iter()
    }

    /// Returns `true` if the [`PlantUmlFileData`] has no [`PlantUmlContent`].
    pub fn is_empty(&self) -> bool {
        self.contents.is_empty()
    }

    /// Returns the number of [`PlantUmlContent`] in the [`PlantUmlFileData`].
    pub fn len(&self) -> usize {
        self.contents.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;

    #[test]
    fn test_parse_plant_uml_file_data() -> Result<()> {
        // OK
        let testdata = r#"@startuml
        @enduml

        @startuml a
            a -> b
        @enduml


        @startuml(id=b)
            b -> a
        @enduml

        "#;
        let parsed = PlantUmlFileData::parse(testdata.into())?;
        assert_eq!(parsed.contents.len(), 3);

        let content_0 = parsed.get(0).unwrap();
        assert!(content_0.id().is_none());

        let content_1 = parsed.get(1).unwrap();
        assert_eq!(content_1.id(), Some("a"));
        assert_eq!(content_1.inner(), "            a -> b\n");

        let content_a = parsed.get_by_id("a").unwrap();
        assert_eq!(content_a.id(), Some("a"));
        assert_eq!(content_a.inner(), "            a -> b\n");

        let content_2 = parsed.get(2).unwrap();
        assert_eq!(content_2.id(), Some("b"));
        assert_eq!(content_2.inner(), "            b -> a\n");

        let content_b = parsed.get_by_id("b").unwrap();
        assert_eq!(content_b.id(), Some("b"));
        assert_eq!(content_b.inner(), "            b -> a\n");

        assert!(parsed.get(3).is_none());

        Ok(())
    }

    #[test]
    fn test_unclosed() -> Result<()> {
        // OK
        let testdata = r#"@startuml
        @e

        "#;

        let result = PlantUmlFileData::parse_from_str(testdata);
        assert!(result.is_err());

        Ok(())
    }
}
