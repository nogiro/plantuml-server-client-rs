use crate::{ParseContainer, ParseResult, wr};
use nom::Parser;
use nom::branch::alt;
use nom::bytes::complete::{tag, take_until};
use nom::character::complete::{line_ending, not_line_ending, space0};
use nom::combinator::{eof, map};
use nom::multi::many1;

/// A token sequence that is an close [`BlockComment`][`crate::BlockComment`] line.
///
/// * `"end '/\n"`
///
/// # Examples
///
/// ```
/// use plantuml_parser::{BlockCommentCloseLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "end '/\n";
/// let (rest, (raws, _token)) = BlockCommentCloseLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "end '/\n");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct BlockCommentCloseLine;

impl BlockCommentCloseLine {
    /// Tries to parse [`BlockCommentCloseLine`]. (e.g. `"end '/\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        // Tries to parse into line.
        let (_, parsed_line) = map(
            (wr!(not_line_ending), alt((wr!(eof), wr!(line_ending)))),
            |(line, end)| (&[line, end]).into(),
        )
        .parse(input.clone())?;

        let (_, parsed): (_, ParseContainer) = map(
            (
                many1((wr!(take_until("'/")), wr!(tag("'/")))),
                wr!(space0),
                alt((wr!(eof), wr!(line_ending))),
            ),
            |(v, sp, end)| {
                v.into_iter()
                    .flat_map(|(a1, a2)| vec![a1, a2])
                    .chain([sp, end])
                    .collect::<Vec<_>>()
                    .into()
            },
        )
        .parse(parsed_line)?;

        let rest = input.split_at(parsed.len()).0;

        Ok((rest, (parsed, Self)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = [
            "'/",
            "'/\n",
            "'/\t ",
            "'/ \t  \n",
            "end '/  ",
            "end '/ \n",
            "end '/ '/",
            "end '/ '/\n",
            "end '/ '/ '/",
            "end '/ '/ '/\n",
        ];

        for testdata in testdata.into_iter() {
            println!("testdata = {testdata:?}");

            let (rest, (parsed, _)) = BlockCommentCloseLine::parse(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(parsed, testdata);
        }

        Ok(())
    }

    #[test]
    fn test_failed() -> anyhow::Result<()> {
        let testdata = [
            // nothing.
            "",
            // A close tag is in next line.
            "\n'/",
            "\n'/ \t ",
            "\n '/ \t ",
            "\n end '/",
            "\n end '/ \t ",
            " \n end '/ \t ",
        ];

        for testdata in testdata.into_iter() {
            println!("testdata = {testdata:?}");

            let result = BlockCommentCloseLine::parse(testdata.into());
            assert!(result.is_err());
        }

        Ok(())
    }
}
