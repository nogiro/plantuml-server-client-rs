use crate::{MetadataItem, MetadataV2};
use anyhow::{Result, bail};
use indexmap::IndexMap;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

/// A structure for storing and updating metadata of processed files while preserving order.
///
/// `MetadataStore` manages a collection of metadata for input files that are monitored for changes.
/// It does not generate metadata itself but receives externally created metadata and updates only the entries corresponding to modified files.
/// [`MetadataItem`] for unchanged files remains unaltered.
pub struct MetadataStore {
    stored: Arc<Mutex<IndexMap<PathBuf, MetadataItem>>>,
}

impl MetadataStore {
    pub fn new() -> Self {
        let stored = Arc::new(Mutex::new(IndexMap::new()));
        Self { stored }
    }

    pub fn update(&self, metadatas: Vec<MetadataItem>) -> Result<()> {
        tracing::debug!("update MetadataStore: metadatas = {metadatas:?}");

        let mut stored = match self.stored.lock() {
            Ok(ok) => ok,
            Err(e) => {
                bail!("failed to lock to update metadata store: {e:?}");
            }
        };

        for metadata in metadatas.into_iter() {
            stored.entry(metadata.path().clone()).insert_entry(metadata);
        }

        Ok(())
    }

    pub fn metadata(&self) -> Result<MetadataV2> {
        let stored = match self.stored.lock() {
            Ok(ok) => ok,
            Err(e) => {
                bail!("failed to lock to get stored metadata: {e:?}");
            }
        };

        let metadata = stored.values().cloned().collect::<Vec<_>>().into();

        Ok(metadata)
    }
}
