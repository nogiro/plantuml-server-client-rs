use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone, Copy, Debug, Default, Eq, PartialEq)]
pub enum MetadataVersion {
    #[default]
    #[serde(rename = "v1")]
    V1,

    #[serde(rename = "v2")]
    V2,
}

#[derive(Deserialize, Serialize, Clone, Copy, Debug, Default)]
pub enum MetadataVersionV1 {
    #[default]
    #[serde(rename = "v1")]
    V1,
}

#[derive(Deserialize, Serialize, Clone, Copy, Debug, Default)]
pub enum MetadataVersionV2 {
    #[default]
    #[serde(rename = "v2")]
    V2,
}

impl std::str::FromStr for MetadataVersion {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> anyhow::Result<Self> {
        match s.to_string().to_lowercase().as_str() {
            "v1" => Ok(Self::V1),
            "v2" => Ok(Self::V2),
            _ => anyhow::bail!("unsupported metadata_version"),
        }
    }
}

impl MetadataVersion {
    pub fn is_v1(&self) -> bool {
        matches!(self, MetadataVersion::V1)
    }
}
