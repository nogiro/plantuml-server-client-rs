use crate::{ParseContainer, ParseResult, wr};
use nom::Parser;
use nom::bytes::complete::{tag, take_until};
use nom::character::complete::space0;

/// A token sequence that is an inline block comment. (like `"/' comment '/"`. not like `"/' one '/ /' two '/"`)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{InlineBlockCommentToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = " /' comment '/  footer  EXAMPLE FOOTER   "; /// The last part of "footer  EXAMPLE FOOTER   " is rest part.
/// let (rest, (raws, token)) = InlineBlockCommentToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "footer  EXAMPLE FOOTER   ");
/// assert_eq!(combined_raw, " /' comment '/  ");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct InlineBlockCommentToken {
    comment: ParseContainer,
}

impl InlineBlockCommentToken {
    /// Tries to parse [`InlineBlockCommentToken`]
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, parsed) = (
            wr!(space0),
            wr!(tag("/'")),
            wr!(take_until("'/")),
            wr!(tag("'/")),
            wr!(space0),
        )
            .parse(input)?;

        let parsed = Vec::from(<[ParseContainer; 5]>::from(parsed));
        let ret = Self {
            comment: parsed.into(),
        };

        Ok((rest, (ret.comment.clone(), ret)))
    }

    pub fn comment(&self) -> &str {
        self.comment.as_str()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = [
            ("/' comment '/   ", ""),
            ("    /' comment '/", ""),
            ("  /' comment '/   ", ""),
            (" /' comment '/ rest ", "rest "),
            (
                " /' comment '/  /' comment '/ rest  ",
                "/' comment '/ rest  ",
            ),
        ];

        for (testdata, expected_rest) in testdata.into_iter() {
            let (rest, (parsed, comment)) = InlineBlockCommentToken::parse(testdata.into())?;
            assert_eq!(parsed, comment.comment);
            assert_eq!(rest, expected_rest);
            assert_eq!(parsed, &testdata[0..testdata.len() - rest.len()]);
        }

        Ok(())
    }
}
