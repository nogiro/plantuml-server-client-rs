use crate::TitleToken;
use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr, wr2};
use nom::character::complete::space0;
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a [`TitleToken`]. (like `"\ttitle EXAMPLE TITLE  \n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{TitleLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = " title EXAMPLE TITLE\n";
/// let (rest, (raws, token)) = TitleLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, " title EXAMPLE TITLE\n");
/// assert_eq!(token.title(), "EXAMPLE TITLE");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct TitleLine {
    token: TitleToken,
    ibc: Option<InlineBlockCommentToken>,
}

impl TitleLine {
    /// Tries to parse [`TitleLine`]. (e.g. `" title EXAMPLE TITLE \n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (token, ibc) = lwc.into();

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self { token, ibc };

        Ok((rest, (ret0, ret1)))
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

impl std::ops::Deref for TitleLine {
    type Target = TitleToken;
    fn deref(&self) -> &Self::Target {
        &self.token
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, TitleToken)> {
    map(
        (wr!(space0), wr2!(TitleToken::parse), wr!(space0)),
        |(p0, (p1, token), p2)| (vec![p0, p1, p2], token),
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = [
            ("title TITLE\n", None),
            (" \ttitle TITLE\n", None),
            ("title TITLE \t\n", None),
            (" \ttitle TITLE \t\n", None),
            ("Title TITLE\n", None),
            ("TITLE TITLE\n", None),
            ("tiTle TITLE\n", None),
            (" /' comment '/ title TITLE\n", Some(" /' comment '/ ")),
            ("title TITLE /' comment '/ \n", Some("/' comment '/ ")),
        ];

        for (testdata, expected_comment) in testdata.into_iter() {
            let (rest, (parsed, TitleLine { token, ibc })) = TitleLine::parse(testdata.into())?;

            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(token.title(), "TITLE");
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);
        }

        Ok(())
    }
}
