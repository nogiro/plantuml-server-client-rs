# CHANGELOG

## 0.6.0

### feature

- Implement support of block comment to avoid unnecessary includes. !55 !57
- add support of inline block comment !51
- add `watch` mode to CLI !46
  - watch the parent directory to detect inode changes !50
- add to read multiple input files !45
  - support multiple metadata for multiple input files !58
  - fix to treat --metadata and always empty is supplied to MetadataStore !59
  - add warning to use v1 metadata format !60
- add ci for test shellscript (and fix trivial) !42 !43

### improvement

- support delimiter for title and header and footer !44

### refactor

- tweak: unify mod name !52

### chore

- Update README.md to reflect the current !61
- Changed to use `CI_JOB_TOKEN` !53
- update crates !48 !49 !54
  - and update Rust edition !56
- adjust log level !47
- Tidy up Cargo.toml !41

## 0.5.1

### fix

- fix: the error occured when the input file path is an absolute path !39
- add: one missing `ParseResult`'s doc comment [d389c9b](https://gitlab.com/nogiro/plantuml-server-client-rs/-/commit/d389c9b35cced7da58410bb370c759701d178700)

### documentation

- edit the range of date in LICENSE [811d31e8](https://gitlab.com/nogiro/plantuml-server-client-rs/-/commit/811d31e82a61e0a204573e72e3abe3cbdd61981d)

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.5.0...v0.5.1

## 0.5.0

### fix

- fix: infinite loop occured when pscr is given unclosed PlantUML content !28

### chore

- update crates !37

### refactor

- refactor: merge `ClientConfig` and `RequestConfig` to `Config` !36
- refactor: `src/client.rs` !35
- (refactor) remove needless move in `IncludeLine::token()` !33
- impl Deref for FooLine -> FooToken !32
- [BREAKING CHANGE] Unify `plantuml-parser`'s `T::parse()` function interfaces: `parse(input: ParseContainer) -> ParseResult<T>` !31
- (BREAKING CHANGE) delete `PlantUmlFile` in `plantuml-parser` !30
- refactor: `Locate::read_from_stdin()`: remove `pub` and readability !26
- refactor: `src/client.rs` need `PathBuf` but `src/locate.rs` provide `Result<String>` !25

### documentation

- update README !34
- enhance doc comment in `plantuml-parser` !29
- enhance doc comments for `plantuml-server-client-rs` !27

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.4.0...v0.5.0

## 0.4.0

- (modify) change loglevel by `-v` args count !19
- (add) feature to output combined PlantUML data (pre-processed data such as `!include`) !20
- (add) feature to output metadata (include file list, PlantUML metadata such as `title`) !21
- (chore) modify CI cacheing !22
- (refactor) capsule `HashMap` in `plantuml-parser` !23

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.3.0...v0.4.0

## 0.3.0

- (add) ignore comment in empty line (block comment is not support)
- (modify) If args specified the config file path and failed to read it, pscr occur an error.
- (fix) config file required full field -> config file read option field

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.2.1...v0.3.0

## 0.2.1

- (fix) `cargo-binstall` metadata

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.2.0...v0.2.1

## 0.2.0

- (add) support `!include_many`
- (add) limited support `!include_once`
- (chore) support `cargo-binstall`

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/v0.1.5...v0.2.0

## 0.1.5

- (add) configuration from file: #2
- (add) CHANGELOG: #4

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/86420ec52af17214a06862ad3c6710d977654d46...v0.1.5

## 0.1.4

features are not included

- (chore) add Docker Compose File
- (test) add test uses Docker container
- (chore) add utility script that bump version

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/e0fdc10254ca63bdd5b3389b0ccfa3f69dd27681...86420ec52af17214a06862ad3c6710d977654d46

## 0.1.3

- (add) support `!include`

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/827f59549c13195a6b3b9666b25bf2bcfa186326...e0fdc10254ca63bdd5b3389b0ccfa3f69dd27681

## 0.1.1 - 0.1.2

features are not included

- (chore) add auto publish CI
- (chore) add CI about Rust
- (chore) cargo publish

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/bc4bfed6e33ff50aa40de089ed0ec559b10f722f...827f59549c13195a6b3b9666b25bf2bcfa186326

## 0.1.0

initial commit

- (add) request multiple diagrams in one file

https://gitlab.com/nogiro/plantuml-server-client-rs/-/compare/a8306404c75dca938f59b4df5a180e3d86a0eb0a...bc4bfed6e33ff50aa40de089ed0ec559b10f722f
