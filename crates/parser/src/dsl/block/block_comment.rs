use crate::PlantUmlLine;
use crate::{ParseContainer, ParseError, ParseResult};
use std::sync::Arc;

/// Lines that are representation as block comment. (like `"/' begin \n end '/\n"`.)
///
/// * " /' begin \n end '/\n"
///
/// # Examples
///
/// ```
/// use plantuml_parser::{BlockComment, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = r#"  /' begin comment
/// in comment
/// end comment '/
/// outer"#;
/// let (rest, (parsed, comment)) = BlockComment::parse(input.into())?;
/// assert_eq!(rest, "outer");
/// assert_eq!(parsed, r#"  /' begin comment
/// in comment
/// end comment '/
/// "#);
/// assert!(comment.is_completed());
///
/// let input = "  /' not ending comment \n";
/// let (rest, (parsed, comment)) = BlockComment::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed, "  /' not ending comment \n");
/// assert!(!comment.is_completed());
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct BlockComment {
    lines: Vec<Arc<PlantUmlLine>>,
    completed: bool,
}

impl BlockComment {
    /// Tries to parse [`BlockComment`]. (e.g. `"/' begin \n end '/\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (mut rest, (_, line)) = PlantUmlLine::parse(input.clone())?;

        if line.block_comment_open().is_none() {
            return Err(ParseError::fail(input));
        }

        let mut lines = vec![Arc::new(line)];
        let mut completed = false;

        loop {
            let (rest_inner, (_, mut line_inner)) = PlantUmlLine::parse(rest)?;

            rest = rest_inner;

            let is_closed = line_inner.block_comment_close().is_some();
            if !is_closed {
                line_inner = line_inner.into_in_comment();
            }

            lines.push(Arc::new(line_inner));

            if is_closed {
                completed = true;
                break;
            }

            if rest.is_empty() {
                break;
            }
        }

        let parsed = input.split_at(input.len() - rest.len()).1;
        Ok((rest, (parsed, Self { lines, completed })))
    }

    /// Returns whether the [`BlockCommentCloseLine`][`crate::BlockCommentCloseLine`] was found.
    pub fn is_completed(&self) -> bool {
        self.completed
    }

    /// Returns the reference of the list of lines in this block.
    pub fn lines(&self) -> &[Arc<PlantUmlLine>] {
        self.lines.as_slice()
    }
}

impl std::fmt::Display for BlockComment {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        for line in self.lines.iter() {
            line.raw_str().fmt(formatter)?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() -> anyhow::Result<()> {
        let input = ParseContainer::from(
            r#"/'
                comment
            '/"#,
        );

        let (rest0, (parsed0, comment)) = BlockComment::parse(input.clone())?;
        println!("rest0 = {rest0}");
        println!("parsed0 = {parsed0}");
        println!("comment = {comment:?}");
        println!("comment (raw) = {comment}");
        assert_eq!(rest0, "");
        assert_eq!(parsed0, input);
        assert!(comment.is_completed());

        Ok(())
    }

    #[test]
    fn test_1() -> anyhow::Result<()> {
        let input = ParseContainer::from(
            r#"/' incompleted
                comment
            "#,
        );

        let (rest0, (parsed0, comment)) = BlockComment::parse(input)?;
        println!("rest0 = {rest0}");
        println!("parsed0 = {parsed0}");
        println!("comment = {comment:?}");
        println!("comment (raw) = {comment}");
        assert!(!comment.is_completed());

        Ok(())
    }
}
