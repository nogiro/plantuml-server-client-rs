use plantuml_parser as parser;

use crate::config::ConfigTrait;
use crate::{Config, Format, Locate, MetadataVersion, Method, Mode};
use crate::{PKG_NAME, PKG_VERSION};
use clap::Parser;
use const_format::concatcp;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

/// The vertion string for `--version` output
const VERSION: &str = concatcp!(
    "\n",
    concatcp!("  ", PKG_NAME, "=", PKG_VERSION, "\n"),
    concatcp!("  ", parser::PKG_NAME, "=", parser::PKG_VERSION,)
);

/// The string about `plantuml-server-client` for `--help`
const ABOUT: &str = std::include_str!("../../docs/include/about.md");

/// The definition of program arguments.
/// [`Args`] has a higher priority than [`FileConfig`](crate::FileConfig).
/// If no value is specified for a configuration item in either [`Args`] or [`FileConfig`](crate::FileConfig), the default value is used.
#[derive(Clone, Debug, Parser)]
#[command(author, version = VERSION, about = ABOUT)]
pub struct Args {
    /// (deprecated) input file path. if not specified, use stdin.
    #[arg(short, long)]
    pub input: Option<PathBuf>,

    /// output file prefix. if both of `[INPUT_FILES]` and `--output` are not specified, use stdout.
    #[arg(short, long)]
    pub output: Option<PathBuf>,

    /// config file path. if `--config` option is specified and the configuration file fails to load, this program will exit with an error for security reasons.
    #[arg(short, long)]
    pub config: Option<PathBuf>,

    /// HTTP Method: `"get"` | `"post"`
    #[arg(short, long)]
    pub method: Option<Method>,

    /// Server's URL Prefix. default: `"http://www.plantuml.com/plantuml/"`. example: `"http://localhost:8080/"`.
    #[arg(short, long)]
    pub url_prefix: Option<String>,

    /// output format: `"svg"` | `"png"` | `"txt"`
    #[arg(short, long)]
    pub format: Option<Format>,

    /// output pre-proccessed data has `.puml` extension
    #[arg(short = 'C', long)]
    pub combined: bool,

    /// output path for metadata
    #[arg(long)]
    pub metadata: Option<PathBuf>,

    /// watch over input files
    #[arg(short = 'W', long)]
    pub watch: bool,

    /// watch throttle msec
    #[arg(short = 'T', long, default_value_t = 50)]
    pub watch_throttle: u64,

    /// increase log level: `-v` -> info, `-vv` -> debug.
    /// if the `PSCR_LOG` environment variable is specified, this `-v` option will ignore.
    /// related: [`init_logger`][`crate::init_logger`]
    #[arg(short, long, action = clap::ArgAction::Count)]
    pub verbose: u8,

    /// input file paths. if not specified, use stdin.
    pub input_files: Vec<PathBuf>,

    /// The version of metadata
    #[arg(long, default_value = "v1")]
    pub metadata_version: MetadataVersion,
}

impl Args {
    /// Creates a [`Config`] from [`Args`].
    pub async fn config(self) -> anyhow::Result<Config> {
        let mut config = Config::load(&self).await?;
        config.metadata = self.metadata;

        Ok(config)
    }

    /// Returns the mode of execution.
    pub fn mode(&self, current_dir: PathBuf) -> Mode {
        let input_files = self.input_files();

        if input_files.is_empty() {
            return Mode::Stdin;
        }

        if self.watch {
            let throttle = Duration::from_millis(self.watch_throttle);
            Mode::new_watch(current_dir, input_files, throttle)
        } else {
            Mode::NoWatch(input_files)
        }
    }

    /// Returns the locate of output.
    pub fn output_arc(&self) -> Arc<Locate> {
        Arc::new(self.output.clone().into())
    }

    fn input_files(&self) -> Vec<PathBuf> {
        if self.input.is_some() {
            tracing::warn!(
                "The `--input [INPUT]` option is deprecated. Simply remove `--input` from `--input [INPUT]` and give it as an extra argument (`[INPUT_FILES]`)."
            );
        }

        self.input_files
            .clone()
            .into_iter()
            .chain(self.input.clone())
            .collect()
    }
}

impl ConfigTrait for Args {
    /// Returns HTTP Request Method ([`Method::Get`] or [`Method::Post`])
    fn method(&self) -> Option<Method> {
        self.method.clone()
    }

    /// Returns URL prefix of the PlantUML Server (e.g. `http://localhost:8080/`)
    fn url_prefix(&self) -> Option<String> {
        self.url_prefix.clone()
    }

    /// Returns output format ([`Format::Svg`] or [`Format::Png`] or [`Format::Ascii`] (`txt`))
    fn format(&self) -> Option<Format> {
        self.format.clone()
    }

    /// Returns whether to output a `combined` diagram.
    fn combined(&self) -> Option<bool> {
        self.combined.then_some(true)
    }

    fn metadata_version(&self) -> Option<MetadataVersion> {
        Some(self.metadata_version)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use clap::CommandFactory;

    #[test]
    fn test_args_debug_assert() {
        Args::command().debug_assert();
    }
}
