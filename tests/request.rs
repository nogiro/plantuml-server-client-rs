use plantuml_server_client_rs as pscr;

mod common;

use crate::common::config;
use pscr::{Client, Locate};
use tokio::fs::read_to_string;

#[tokio::test]
async fn simple() -> anyhow::Result<()> {
    let input = Locate::new(Some("./tests/assets/simple.puml".into()));
    let output = Locate::new(Some("./outputs/tests/request".into()));
    let config = config!();

    Client::new(config).request(input, &output).await?;

    let snapshot = read_to_string("./tests/snapshot/request/simple/0.svg").await?;
    let generated = read_to_string("./outputs/tests/request/simple/0.svg").await?;

    assert_eq!(snapshot, generated);

    Ok(())
}

#[tokio::test]
async fn multiple() -> anyhow::Result<()> {
    let input = Locate::new(Some("./tests/assets/multiple.puml".into()));
    let output = Locate::new(Some("./outputs/tests/request".into()));
    let config = config!();

    Client::new(config).request(input, &output).await?;

    let files = [
        "request/multiple/figure_multiple_1.svg",
        "request/multiple/figure_multiple_2.svg",
    ];

    for file in files {
        let snapshot = read_to_string(format!("./tests/snapshot/{file}")).await?;
        let generated = read_to_string(format!("./outputs/tests/{file}")).await?;
        assert_eq!(snapshot, generated);
    }

    Ok(())
}

#[tokio::test]
async fn include() -> anyhow::Result<()> {
    let input = Locate::new(Some("./tests/assets/include.puml".into()));
    let output = Locate::new(Some("./outputs/tests/request".into()));
    let config = config!();

    Client::new(config).request(input, &output).await?;

    let files = [
        "request/include/figure_include_1.svg",
        "request/include/figure_include_2.svg",
        // "request/include/figure_include_3.svg", unimplemented
    ];

    for file in files {
        let snapshot = read_to_string(format!("./tests/snapshot/{file}")).await?;
        let generated = read_to_string(format!("./outputs/tests/{file}")).await?;
        assert_eq!(snapshot, generated);
    }

    Ok(())
}
