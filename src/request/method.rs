/// HTTP Method to request
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Method {
    Post,
    Get,
}

impl std::str::FromStr for Method {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> anyhow::Result<Self> {
        match s.to_string().to_lowercase().as_str() {
            "post" => Ok(Self::Post),
            "get" => Ok(Self::Get),
            _ => anyhow::bail!("unsupported method"),
        }
    }
}
