use crate::mode::no_watch;
use crate::{Client, Locate};
use anyhow::{Context as _, Result, bail};
use itertools::Itertools as _;
use std::collections::HashSet;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use watchexec::Watchexec;
use watchexec::action::ActionHandler;
use watchexec_events::FileType;

/// The parameters of the "watch and requests".
pub struct WatchInner {
    input_files: Vec<PathBuf>,
    throttle: Duration,
}

/// The common state of each loop.
#[derive(Clone)]
struct Handler {
    idx: Arc<Mutex<u64>>,
    client: Arc<Client>,
    inputs: Arc<HashSet<PathBuf>>,
    output: Arc<Locate>,
}

impl From<(Vec<PathBuf>, Duration)> for WatchInner {
    /// Constracts from [`Args`][`crate::Args`].
    fn from((input_files, throttle): (Vec<PathBuf>, Duration)) -> Self {
        Self {
            input_files,
            throttle,
        }
    }
}

impl WatchInner {
    pub fn new(current_dir: PathBuf, input_files: Vec<PathBuf>, throttle: Duration) -> Self {
        let input_files: Vec<_> = input_files
            .into_iter()
            .map(|p| {
                if p.is_relative() {
                    let mut ret = current_dir.clone();
                    ret.push(p);
                    ret
                } else {
                    p
                }
            })
            .unique()
            .collect();

        Self {
            input_files,
            throttle,
        }
    }

    /// Executes the "watch and requests" process.
    pub async fn run(self, client: Arc<Client>, output: Arc<Locate>) -> Result<()> {
        // first request
        no_watch(client.clone(), self.input_files.clone(), output.clone())
            .await
            .context("failed first request.")?;

        let parent_paths = self.parent_paths();
        let inputs: HashSet<_> = self.input_files.into_iter().collect();

        let handler = Handler::new(inputs, client, output);

        let wx = Watchexec::new_async(move |action| {
            let handler = handler.clone();
            Box::new(async move { handler.handle(action).await })
        })
        .context("failed to initialize Watchexec")?;

        wx.config.throttle(self.throttle);
        wx.config.pathset(parent_paths);

        // watch and request
        wx.main()
            .await
            .context("failed to start to watch.")?
            .context("an error occured in watch loop.")?;

        Ok(())
    }

    /// Watch directory to change inode.
    fn parent_paths(&self) -> Vec<PathBuf> {
        self.input_files
            .clone()
            .into_iter()
            .map(|mut p| {
                p.pop();
                p
            })
            .filter(|p| p.is_relative() || p.parent().is_some())
            .unique()
            .collect()
    }
}

impl Handler {
    fn new(inputs: HashSet<PathBuf>, client: Arc<Client>, output: Arc<Locate>) -> Self {
        let idx = Arc::new(Mutex::new(0u64));
        let inputs = Arc::new(inputs);

        Self {
            idx,
            client,
            inputs,
            output,
        }
    }

    /// Handles each loop.
    async fn handle(&self, mut action: ActionHandler) -> ActionHandler {
        if action.signals().next().is_some() {
            let signals: Vec<_> = action.signals().collect();
            tracing::debug!("received stop signals: signals = {signals:?}",);
            action.quit();
            return action;
        }

        let Ok(idx) = self.count_idx().inspect_err(|e| {
            tracing::warn!("failed to take mutex lock to count watch loop: {e:?}")
        }) else {
            action.quit();
            return action;
        };

        let inputs = self.to_input_paths_from_action(&action);
        if inputs.is_empty() {
            tracing::trace!("inputs are empty: watch loop count = {idx}");
            return action;
        }

        for event in action.events.iter() {
            tracing::trace!("watch loop count = {idx}, event = {event:?}");
        }

        let result = no_watch(self.client.clone(), inputs, self.output.clone())
            .await
            .context("failed to request.");
        if result.is_err() {
            tracing::warn!("error in watch request: {result:?}");
            action.quit();
        }

        action
    }

    /// Adds loop count.
    fn count_idx(&self) -> Result<u64> {
        let Ok(mut idx) = self.idx.lock() else {
            bail!("failed to take mutex lock");
        };
        *idx += 1;
        Ok(*idx)
    }

    /// Extracts paths.
    fn to_input_paths_from_action(&self, action: &ActionHandler) -> Vec<PathBuf> {
        action
            .paths()
            .flat_map(|(path, ty)| {
                if let Some(FileType::File) = ty {
                    Some(path.to_path_buf())
                } else {
                    None
                }
            })
            .filter(|p| self.inputs.contains(p))
            .unique()
            .collect()
    }
}
