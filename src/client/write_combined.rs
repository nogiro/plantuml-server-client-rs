use crate::client::request_single::RequestSingleClient;
use crate::{Config, Locate};
use anyhow::{Context as _, Result};
use std::path::PathBuf;

pub(super) struct WriteCombinedClient<'a> {
    pub config: &'a Config,
}

impl WriteCombinedClient<'_> {
    pub async fn write_combined_and_then_return_path(
        &self,
        identifier: &str,
        data: String,
        input: &Locate,
        output: &Locate,
    ) -> Result<Option<PathBuf>> {
        let output_combined = self.config.combined;

        if output_combined {
            let path = output
                .combined_output_path(input, identifier)
                .context("When output to `stdout`, cannot locate to write combined data.")?;
            tracing::info!("combined_output_path = {path:?}");

            let data = futures::stream::iter(vec![Ok(data.into())]);
            let pathbuf = path.to_path_buf();
            path.write(data)
                .await
                .context("failed to write combined data")?;

            Ok(Some(pathbuf))
        } else {
            Ok(None)
        }
    }
}

impl<'a> From<&'a RequestSingleClient<'a>> for WriteCombinedClient<'a> {
    fn from(client: &'a RequestSingleClient<'a>) -> Self {
        Self {
            config: client.config,
        }
    }
}
