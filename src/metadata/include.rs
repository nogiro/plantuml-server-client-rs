use anyhow::Result;
use derive_getters::Getters;
use normalize_path::NormalizePath;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::path::PathBuf;
use typed_builder::TypedBuilder;

/// A metadata about files collected for includes
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct IncludesMetadata(
    #[serde(with = "btreemap_vec_with")] BTreeMap<PathBuf, Vec<IncludesMetadataItem>>,
);

/// A metadata about file collected for includes
///
/// * `path` - The path constructed from `base_path` and `relative_path`
/// * `normalize_path` - The normalized `path`
/// * `base_path` - The path of the file from which to include
/// * `relative_path` - The relative path from the source file to include
/// * `index` - The specified index to include
/// * `id` - The specified id to include
#[derive(Deserialize, Serialize, TypedBuilder, Getters, Clone, Debug)]
#[builder(mutators(
    pub fn path(&mut self, path: PathBuf){
        self.normalized_path = path.normalize();
        self.path = path
    }
))]
pub struct IncludesMetadataItem {
    #[builder(via_mutators)]
    path: PathBuf,
    #[builder(via_mutators)]
    normalized_path: PathBuf,
    base_path: PathBuf,
    relative_path: PathBuf,
    #[serde(skip_serializing_if = "Option::is_none")]
    index: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    id: Option<String>,
}

impl IncludesMetadata {
    /// Returns `true` if the metadata has a length of 0.
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns the number of elements in the metadata.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns an iterator over the metadata item.
    pub fn iter(&self) -> impl std::iter::Iterator<Item = (&PathBuf, &Vec<IncludesMetadataItem>)> {
        self.0.iter()
    }

    /// Returns a flatten iterator over the metadata item.
    pub fn flat_iter(&self) -> impl std::iter::Iterator<Item = (&PathBuf, &IncludesMetadataItem)> {
        self.0
            .iter()
            .flat_map(|(k, v)| v.iter().map(move |x| (k, x)))
    }
}

impl From<BTreeMap<PathBuf, Vec<IncludesMetadataItem>>> for IncludesMetadata {
    fn from(inner: BTreeMap<PathBuf, Vec<IncludesMetadataItem>>) -> Self {
        Self(inner)
    }
}

impl btreemap_vec_with::Key for IncludesMetadataItem {
    type Key = PathBuf;
    fn key(&self) -> Self::Key {
        self.path.normalize()
    }
}

mod btreemap_vec_with {
    use super::*;

    use serde::ser::SerializeSeq;
    use serde::{Deserializer, Serializer};

    pub trait Key {
        type Key;
        fn key(&self) -> Self::Key;
    }

    pub fn deserialize<'de, D, K, V>(deserializer: D) -> Result<BTreeMap<K, Vec<V>>, D::Error>
    where
        D: Deserializer<'de>,
        K: Ord,
        V: Deserialize<'de> + Key<Key = K>,
    {
        let v = Vec::<V>::deserialize(deserializer)?;
        let mut map = BTreeMap::new();
        for item in v {
            map.entry(item.key()).or_insert(vec![]).push(item);
        }
        Ok(map)
    }

    pub fn serialize<S, K, V>(map: &BTreeMap<K, Vec<V>>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
        V: Serialize,
    {
        let len = map.values().flatten().collect::<Vec<_>>().len();
        let mut seq = serializer.serialize_seq(Some(len))?;
        for element in map.values().flatten() {
            seq.serialize_element(element)?;
        }
        seq.end()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_serialize_btreemap_values() -> anyhow::Result<()> {
        #[derive(Serialize)]
        struct TestStruct {
            #[serde(with = "btreemap_vec_with")]
            map: BTreeMap<usize, Vec<String>>,
        }

        let testdata = TestStruct {
            map: BTreeMap::from([
                (1, (0..1).map(|_| "1".into()).collect()),
                (2, (0..2).map(|_| "2".into()).collect()),
                (3, (0..3).map(|_| "3".into()).collect()),
                (4, (0..4).map(|_| "4".into()).collect()),
            ]),
        };

        let serialized = serde_json::to_string(&testdata)?;
        let mut deserialized = serde_json::from_str::<serde_json::Value>(&serialized)?
            .get("map")
            .map(|x| serde_json::from_value::<Vec<String>>(x.clone()))
            .unwrap()?;
        deserialized.sort();
        assert_eq!(
            deserialized,
            ["1", "2", "2", "3", "3", "3", "4", "4", "4", "4"]
        );

        Ok(())
    }
}
