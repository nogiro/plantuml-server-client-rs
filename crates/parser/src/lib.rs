#![doc = include_str!("../docs/include/about-plantuml-parser.md")]

mod container;
mod dsl;
mod error;
mod filedata;
mod path;

pub use container::{ParseContainer, ParseError, ParseResult};
pub use dsl::block::{BlockComment, PlantUmlBlock, PlantUmlBlockKind};
pub use dsl::content::{IncludesCollections, PlantUmlContent};
pub use dsl::line::{
    BlockCommentCloseLine, BlockCommentOpenLine, EmptyLine, EndLine, FooterLine, HeaderLine,
    IncludeLine, PlantUmlLine, PlantUmlLineKind, StartLine, TitleLine,
};
pub use dsl::token::{
    DiagramIdToken, FooterToken, HeaderToken, IncludeKind, IncludeSpecifierToken, IncludeToken,
    InlineBlockCommentToken, StartDiagramToken, TitleToken,
};
pub use error::Error;
pub use filedata::PlantUmlFileData;
pub use path::{PathResolver, PathResolverError};

pub(crate) use container::{wr, wr2};

/// The package name from Cargo.toml: [`plantuml-parser`](https://docs.rs/plantuml-parser/)
pub const PKG_NAME: &str = env!("CARGO_PKG_NAME");
/// The package version from Cargo.toml
pub const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
