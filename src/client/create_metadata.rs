use crate::{Client, CollectedContainer, MetadataItem, PlantUmlMetadataItem};
use anyhow::Result;

pub(super) struct CreateMetadataClient;

impl CreateMetadataClient {
    pub async fn create_metadata(
        &self,
        collected: CollectedContainer,
        plantuml_metadata: Vec<PlantUmlMetadataItem>,
    ) -> Result<MetadataItem> {
        let metadata = MetadataItem::builder()
            .path(collected.path().clone())
            .includes(collected.include_metadata().clone())
            .plantuml(plantuml_metadata.into())
            .build();

        Ok(metadata)
    }
}

impl From<&Client> for CreateMetadataClient {
    fn from(_client: &Client) -> Self {
        Self
    }
}
