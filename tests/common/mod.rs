use plantuml_server_client_rs as pscr;

use pscr::{Config, MetadataVersion, Method};
use std::path::PathBuf;

macro_rules! config {
    () => {
        $crate::common::client_config(None, false)
    };

    ($config:expr_2021) => {
        $crate::common::client_config(Some($config), false)
    };

    ($config:expr_2021, "v2") => {
        $crate::common::client_config(Some($config), true)
    };
}
pub(crate) use config;

pub fn client_config(metadata: Option<PathBuf>, v2: bool) -> Config {
    let url_prefix =
        std::env::var("PSCR_URL_PREFIX").unwrap_or_else(|_| "http://localhost:8080/".into());

    let metadata_version = if v2 {
        MetadataVersion::V2
    } else {
        MetadataVersion::V1
    };

    Config {
        method: Method::Post,
        url_prefix,
        metadata,
        metadata_version,
        ..Default::default()
    }
}
