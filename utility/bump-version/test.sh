#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"
ROOT_DIR="${SCRIPT_DIR}/../../"

# shellcheck source=utility/bump-version/functions.sh
source "${SCRIPT_DIR}/functions.sh"

function error_exit() {
  MSG="${1:-}"
  rm -rf "${TMPDIR}"
  echo "${MSG}" >&2
  exit 1
}

# edit_versions
# -- preset
TMPDIR="$(mktemp -d "$(realpath --canonicalize-missing "${ROOT_DIR}/outputs/test-XXXXXX")")" \
  || exit 1

cp -r "${SCRIPT_DIR}/testdata/" "${TMPDIR}/"
mkdir "${TMPDIR}/testdata/edit_versions/outputs/" # temporary file's directory

# -- test
edit_versions "${TMPDIR}/testdata/edit_versions/" "hogehoge" \
  || error_exit 'test failed: execute edit_versions'
if ! diff "${TMPDIR}/testdata/edit_versions/Cargo.toml" "${TMPDIR}/testdata/edit_versions/expected.toml"; then
  error_exit 'test failed: result of edit_versions'
fi

# -- cleanup
rm -rf "${TMPDIR}"

# semver_parts
if [ "$(semver_parts major 1.2.3)" != "1" ]; then
  error_exit 'test failed: semver_parts major 1.2.3 = 1'
fi

if [ "$(semver_parts minor 1.2.3)" != "2" ]; then
  error_exit 'test failed: semver_parts major 1.2.3 = 2'
fi

if [ "$(semver_parts patch 1.2.3)" != "3" ]; then
  error_exit 'test failed: semver_parts patch 1.2.3 = 3'
fi

# next_semver
if [ "$(next_semver major 1.2.3)" != "2.0.0" ]; then
  error_exit 'test failed: next_semver major 1.2.3 = 2.0.0'
fi

if [ "$(next_semver minor 1.2.3)" != "1.3.0" ]; then
  error_exit 'test failed: next_semver major 1.2.3 = 1.3.0'
fi

if [ "$(next_semver patch 1.2.3)" != "1.2.4" ]; then
  error_exit 'test failed: next_semver patch 1.2.3 = 1.2.4'
fi
