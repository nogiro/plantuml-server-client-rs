use base64::Engine as _;
use base64::alphabet::Alphabet;
use base64::engine::{GeneralPurpose, GeneralPurposeConfig};

/// An encoder for PlantUML Server's GET request
pub struct Encoder {
    alphabet: Alphabet,
}

impl Encoder {
    /// Initializes an [`Encoder`]
    pub fn new() -> Self {
        // https://plantuml.com/en/text-encoding#82af841589057aa8
        let alphabet =
            Alphabet::new("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_")
                .unwrap();

        Self { alphabet }
    }

    /// Encodes data
    pub fn encode(&self, data: &[u8]) -> String {
        let engine = self.engine();
        engine.encode(data)
    }

    fn engine(&self) -> GeneralPurpose {
        let config = GeneralPurposeConfig::new().with_encode_padding(false);
        GeneralPurpose::new(&self.alphabet, config)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use anyhow::Result;
    use deflate::deflate_bytes;
    use std::fs::read_to_string;

    #[tokio::test]
    async fn test_encode() -> Result<()> {
        let encoder = Encoder::new();

        let encoded = encoder.encode(b"123 abc");
        assert_eq!(encoded, "CJ8p865YOm");

        let input = read_to_string("./tests/assets/simple.puml")?;
        let compressed = deflate_bytes(input.as_bytes());
        let encoded = encoder.encode(&compressed);
        assert_eq!(
            encoded,
            "SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IG80"
        );

        Ok(())
    }
}
