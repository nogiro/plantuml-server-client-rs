#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"

mkdir -p "${SCRIPT_DIR}/../../outputs/" # create temporary directory

find "${SCRIPT_DIR}/../" -type f -name 'test.sh' -exec bash '{}' ';'
