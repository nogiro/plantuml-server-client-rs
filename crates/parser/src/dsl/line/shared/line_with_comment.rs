use crate::{InlineBlockCommentToken, ParseContainer, wr, wr2};
use nom::branch::alt;
use nom::character::complete::{line_ending, space0};
use nom::combinator::{eof, map};
use nom::{IResult, Parser};

/// A generic type line with comment. (like `"/' comment '/ @enduml\n"`)
#[derive(Clone, Debug)]
pub struct LineWithComment<T> {
    not_comment: T,
    ibc: Option<InlineBlockCommentToken>,
}

impl<T> LineWithComment<T> {
    pub fn parse<F>(
        parser: F,
        input: ParseContainer,
    ) -> IResult<ParseContainer, (Vec<ParseContainer>, LineWithComment<T>)>
    where
        F: FnMut(ParseContainer) -> IResult<ParseContainer, (Vec<ParseContainer>, T)>,
    {
        let (rest, (parsed, not_comment, ibc)) = parse_inner(parser, input)?;

        let ret = LineWithComment { not_comment, ibc };
        Ok((rest, (parsed, ret)))
    }
}

impl<T> From<LineWithComment<T>> for (T, Option<InlineBlockCommentToken>) {
    fn from(lwc: LineWithComment<T>) -> Self {
        (lwc.not_comment, lwc.ibc)
    }
}

fn parse_inner<F, T>(
    mut parser: F,
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, T, Option<InlineBlockCommentToken>)>
where
    F: FnMut(ParseContainer) -> IResult<ParseContainer, (Vec<ParseContainer>, T)>,
{
    if let Ok(res) = map(
        (
            wr!(space0),
            &mut parser,
            wr2!(InlineBlockCommentToken::parse),
            alt((wr!(eof), wr!(line_ending))),
        ),
        |(sp, (parsed, additional), (ibc_parsed, ibc), ending)| {
            let parsed = [vec![sp], parsed, vec![ibc_parsed, ending]]
                .into_iter()
                .flatten()
                .collect();
            (parsed, additional, Some(ibc))
        },
    )
    .parse(input.clone())
    {
        return Ok(res);
    }

    if let Ok(res) = map(
        (
            wr2!(InlineBlockCommentToken::parse),
            &mut parser,
            wr!(space0),
            alt((wr!(eof), wr!(line_ending))),
        ),
        |((ibc_parsed, ibc), (parsed, additional), sp, ending)| {
            let parsed = [vec![ibc_parsed], parsed, vec![sp, ending]]
                .into_iter()
                .flatten()
                .collect();
            (parsed, additional, Some(ibc))
        },
    )
    .parse(input.clone())
    {
        return Ok(res);
    }

    map(
        (wr!(space0), &mut parser, alt((wr!(eof), wr!(line_ending)))),
        |(sp, (parsed, additional), ending)| {
            let parsed = [vec![sp], parsed, vec![ending]]
                .into_iter()
                .flatten()
                .collect();
            (parsed, additional, None)
        },
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    use nom::character::complete::alpha1;

    #[test]
    fn test_parse_line_with_comment() -> anyhow::Result<()> {
        fn alpha1_parser(
            input: ParseContainer,
        ) -> IResult<ParseContainer, (Vec<ParseContainer>, ParseContainer)> {
            map(wr!(alpha1), |p: ParseContainer| (vec![p.clone()], p)).parse(input)
        }

        let testdata = [
            ("abc\n", alpha1_parser, "abc", None),
            (
                "abc /' comment'/\n",
                alpha1_parser,
                "abc",
                Some(" /' comment'/"),
            ),
            (
                " /' comment'/  abc  \n",
                alpha1_parser,
                "abc",
                Some(" /' comment'/  "),
            ),
        ];

        for (testdata, parser, expected_additional, expected_comment) in testdata.into_iter() {
            println!("try: testdata = {testdata:?}");

            let (rest, (parsed, lwc)) = LineWithComment::parse(parser, testdata.into())?;
            let (additional, ibc) = lwc.into();

            let parsed = ParseContainer::from(parsed);
            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(additional, expected_additional);
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);
        }

        Ok(())
    }
}
