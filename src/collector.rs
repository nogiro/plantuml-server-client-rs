use crate::{IncludesMetadata, IncludesMetadataItem};
use anyhow::{Context as _, Result};
use derive_getters::Getters;
use plantuml_parser::{IncludeToken, IncludesCollections, PathResolver, PlantUmlFileData};
use std::collections::{BTreeMap, HashMap, HashSet};
use std::fs::read_to_string;
use std::path::PathBuf;

/// A [`Collector`] collect [`IncludesMetadataItem`] and include files.
pub struct Collector;

/// A return data type for [`Collector::collect`]
#[derive(Getters, Debug)]
pub struct CollectedContainer {
    path: PathBuf,
    includes: IncludesCollections,
    include_metadata: IncludesMetadata,
}

impl Collector {
    /// Collects files to include recursively
    pub fn collect(path: PathBuf, data: &PlantUmlFileData) -> Result<CollectedContainer> {
        let mut includes = HashMap::new();
        let mut include_metadata = BTreeMap::new();
        let mut negative = HashSet::new();

        Self::collect_includes_inner(
            path.clone(),
            data,
            &mut includes,
            &mut include_metadata,
            &mut negative,
        )?;

        tracing::debug!("includes = {includes:?}");
        tracing::debug!("include_metadata = {include_metadata:?}");

        Ok(CollectedContainer {
            path,
            includes: IncludesCollections::new(includes),
            include_metadata: include_metadata.into(),
        })
    }

    /// Collects files to include recursively
    fn collect_includes_inner(
        path: PathBuf,
        data: &PlantUmlFileData,
        includes: &mut HashMap<PathBuf, PlantUmlFileData>,
        include_metadata: &mut BTreeMap<PathBuf, Vec<IncludesMetadataItem>>,
        negative: &mut HashSet<PathBuf>,
    ) -> Result<()> {
        tracing::info!("collect path = {path:?}");

        let resolver = PathResolver::new(path);
        tracing::debug!("resolver = {resolver:?}");

        for data in data.iter() {
            for include in data.includes().iter() {
                let mut inner_resolver = resolver.clone();
                let relative_path: PathBuf = include.filepath().into();
                inner_resolver.add(relative_path.clone());
                let path = inner_resolver
                    .build()
                    .context("failed to resolve path to collect files")?;
                tracing::debug!("path from inner_resolver = {path:?}");

                Self::insert_metadata_if_needed(
                    include,
                    &resolver,
                    &inner_resolver,
                    include_metadata,
                )?;

                // avoid infinite loop
                if includes.contains_key(&path) || negative.contains(&path) {
                    continue;
                }

                let Ok(data) = read_to_string(&path) else {
                    tracing::warn!("failed to read file: path = {path:?}");
                    negative.insert(path);
                    continue;
                };

                let Ok(content) = PlantUmlFileData::parse_from_str(data) else {
                    tracing::warn!("failed to parse PlantUML content: path = {path:?}");
                    negative.insert(path);
                    continue;
                };

                includes.insert(path.clone(), content.clone());
                Self::collect_includes_inner(path, &content, includes, include_metadata, negative)?;
            }
        }

        Ok(())
    }

    fn insert_metadata_if_needed(
        include: &IncludeToken,
        resolver: &PathResolver,
        inner_resolver: &PathResolver,
        include_metadata: &mut BTreeMap<PathBuf, Vec<IncludesMetadataItem>>,
    ) -> Result<()> {
        let relative_path: PathBuf = include.filepath().into();
        let path = inner_resolver.build()?;
        let raw_path = inner_resolver.build_without_normalize()?;
        let base_path = resolver
            .build()
            .context("failed to resolve path to metadata base_path")?;

        let metadata = IncludesMetadataItem::builder()
            .path(raw_path)
            .base_path(base_path)
            .relative_path(relative_path)
            .index(include.index())
            .id(include.id().map(|x| x.into()))
            .build();
        include_metadata
            .entry(path.clone())
            .or_default()
            .push(metadata);

        Ok(())
    }
}
