#!/usr/bin/env bash
#
# Extracts embedded source codes in markdown files for document.
#
# * `docs/include/about-plantuml-parser.md` -> `examples/generated-parser-readme-*.rs`

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"
ROOT_DIR="${SCRIPT_DIR}/../../"

TARGETS_JSON='[
  {"source":"./docs/include/about-plantuml-parser.md", "prefix": "./examples/generated-parser-readme-"}
]'

# shellcheck disable=SC2016
AWS_SCRIPT='
  BEGIN {
    IDX = 0;
    IS_CODEBLOCK = 0;
    if (PREFIX == "") {
      exit 1;
    }
  }

  # leave (do not print from this line)
  IS_CODEBLOCK == 1 && /^```/ {
    IS_CODEBLOCK = 0;
    IDX += 1;
  }

  # print in codeblock
  IS_CODEBLOCK == 1 {
    print $0 > FILE;
  }

  # enter (print from next line)
  IS_CODEBLOCK == 0 && /^```/ {
    IS_CODEBLOCK = 1;
    FILE = PREFIX""IDX".rs"
  }
'

echo "${TARGETS_JSON}" \
  | jq -r \
    --arg root "${ROOT_DIR}" \
    --arg awk "${AWS_SCRIPT}" \
    '.[] | "
mkdir -p \"$(dirname \"" + $root + .prefix + "\")\" \\
  && cat \"" + .root + .source + "\" | awk -v '\''PREFIX=" + .root + .prefix + "'\'' '\''" + $awk + "'\''"
    ' \
  | sh || {
    echo error in extract
    exit 1
  }
