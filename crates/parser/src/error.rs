use crate::{EndLine, StartLine};
use crate::{ParseError, PathResolverError};

/// The error type for [`plantuml-parser`](https://docs.rs/plantuml-parser/)'s operations.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("parse error: {0}")]
    Parse(#[from] ParseError),

    #[error("PathResolver error: {0}")]
    PathResolver(#[from] PathResolverError),

    #[error(
        "The diagram kind in The start keyword and the diagram kind in the end keyword are not match: start = {0:?}, end = {1:?}"
    )]
    DiagramKindNotMatch(StartLine, EndLine),

    #[error("An end keyword is not found in PlantUmlContent: {0}")]
    ContentUnclosed(String),

    #[error("An end keyword is not found in PlantUmlContent ")]
    IsNotBlockComment,

    #[error("Unreachable: {0}")]
    Unreachable(String),
}
