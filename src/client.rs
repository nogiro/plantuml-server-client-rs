mod create_metadata;
mod load_metadata;
mod metadata_store;
mod request_single;
mod write_combined;

use crate::encode::Encoder;
use crate::{Collector, Config, Locate, Metadata, MetadataItem, MetadataVersion};
use anyhow::{Context as _, Result};
use metadata_store::MetadataStore;
use plantuml_parser::PlantUmlFileData;

/// A Client to request to PlantUML Server.
pub struct Client {
    config: Config,

    client: reqwest::Client,
    encoder: Encoder,

    metadata_store: MetadataStore,
}

impl Client {
    /// Creates a new [`Client`].
    ///
    /// * `config` - A config of `plantuml-server-client`.
    pub fn new(config: Config) -> Self {
        let client = reqwest::Client::new();
        let encoder = Encoder::new();
        let metadata_store = MetadataStore::new();

        Self {
            config,
            client,
            encoder,
            metadata_store,
        }
    }

    /// Requests by the client
    ///
    /// * `input` - A [`Locate`] of input.
    /// * `output` - A [`Locate`] of output.
    pub async fn request(&self, input: Locate, output: &Locate) -> Result<MetadataItem> {
        tracing::info!("method = {:?}", self.config.method);
        tracing::info!("url_prefix = {:?}", self.config.url_prefix);
        tracing::info!("format = {:?}", self.config.format);
        tracing::info!("input = {input:?}");
        tracing::info!("output = {output:?}");

        let filedata = input.read()?;
        let base = input.to_path_buf();
        let filedata = PlantUmlFileData::parse_from_str(filedata)?;
        let collected = Collector::collect(base.clone(), &filedata)
            .context("failed to collect files to include")?;
        tracing::debug!("collected = {collected:?}");

        let mut plantuml_metadata = vec![];

        for (idx, data) in filedata.iter().enumerate() {
            request_single::RequestSingleClient::from(self)
                .request_single(
                    &mut plantuml_metadata,
                    idx,
                    data,
                    &collected,
                    &input,
                    output,
                )
                .await
                .with_context(|| format!("failed to request: idx = {idx}"))?;
        }

        tracing::debug!("plantuml_metadata = {plantuml_metadata:?}");
        let metadata = create_metadata::CreateMetadataClient::from(self)
            .create_metadata(collected, plantuml_metadata)
            .await
            .context("failed to write metadata")?;
        tracing::debug!("metadata = {metadata:?}");

        Ok(metadata)
    }

    pub async fn write_metadata_if_needed(&self, metadatas: Vec<MetadataItem>) -> Result<()> {
        if let Some(metadata_path) = &self.config.metadata {
            self.metadata_store
                .update(metadatas)
                .context("failed to update metadata store.")?;
            let metadata = self
                .metadata_store
                .metadata()
                .context("failed to get metadata from store.")?;

            let data = match &self.config.metadata_version {
                MetadataVersion::V1 => serde_json::to_string(&Metadata::from(metadata)),
                MetadataVersion::V2 => serde_json::to_string(&metadata),
            }
            .context("failed to serialize metadata")?;
            let data = futures::stream::iter(vec![Ok(data.into())]);
            let path = Locate::new(Some(metadata_path.clone()));
            path.write(data).await.context("failed to write metadata")?;
        }

        Ok(())
    }
}
