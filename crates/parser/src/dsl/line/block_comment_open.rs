use crate::{ParseContainer, ParseResult, wr};
use nom::Parser;
use nom::branch::alt;
use nom::bytes::complete::{tag, take_till};
use nom::character::complete::{line_ending, space0};
use nom::combinator::eof;

/// A token sequence that is an open [`BlockComment`][`crate::BlockComment`] line.
///
/// * `"/' begin\n"`
///
/// # Examples
///
/// ```
/// use plantuml_parser::{BlockCommentOpenLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "/' begin\n";
/// let (rest, (raws, _token)) = BlockCommentOpenLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "/' begin\n");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct BlockCommentOpenLine;

impl BlockCommentOpenLine {
    /// Tries to parse [`BlockCommentOpenLine`]. (e.g. `"/' begin\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, parsed) = (
            wr!(space0),
            wr!(tag("/'")),
            wr!(take_till(|c| c == '\n' || c == '\r')),
            alt((wr!(eof), wr!(line_ending))),
        )
            .parse(input)?;

        let parsed = Vec::from(<[ParseContainer; 4]>::from(parsed));
        Ok((rest, (parsed.into(), Self)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_0() -> anyhow::Result<()> {
        let input = ParseContainer::from(
            r#"/'
                comment
            '/"#,
        );

        let (rest0, (parsed0, _)) = BlockCommentOpenLine::parse(input.clone())?;
        println!("rest0 = {rest0}");
        println!("parsed0 = {parsed0}");
        assert_eq!(rest0, "                comment\n            '/");
        assert_eq!(parsed0, "/'\n");

        Ok(())
    }

    #[test]
    fn test_1() -> anyhow::Result<()> {
        let input = ParseContainer::from(
            r#"/' begin
                comment
            end '/"#,
        );

        let (rest0, (parsed0, _)) = BlockCommentOpenLine::parse(input.clone())?;
        println!("rest0 = {rest0}");
        println!("parsed0 = {parsed0}");
        assert_eq!(rest0, "                comment\n            end '/");
        assert_eq!(parsed0, "/' begin\n");

        Ok(())
    }

    #[test]
    fn test_2() -> anyhow::Result<()> {
        let inputs = [
            "/'",
            "/'\n",
            "/' begin",
            "/' begin\n",
            "/' begin \t",
            "/' begin \t\n",
            "  \t /' begin \t",
            " \t   /' begin \t\n",
            "/'/' begin\n",
            "/' /' begin\n",
            "/' /' /' begin\n",
        ];

        for input in inputs.into_iter() {
            let container = ParseContainer::from(input);
            let (rest0, (parsed0, _)) = BlockCommentOpenLine::parse(container)?;
            println!("rest0 = {rest0}");
            println!("parsed0 = {parsed0}");
            assert_eq!(rest0, "");
            assert_eq!(parsed0, input);
        }

        Ok(())
    }
}
