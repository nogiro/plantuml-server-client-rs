use crate::dsl::token::shared::{Comment, Delimiter};
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::Parser;
use nom::bytes::complete::tag_no_case;

/// A token sequence that is around the footer keyword. (like `"footer EXAMPLE FOOTER"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{FooterToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "footer  EXAMPLE FOOTER   "; /// The last "   " is rest parts
/// let (rest, (raws, token)) = FooterToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "   ");
/// assert_eq!(combined_raw, "footer  EXAMPLE FOOTER");
/// assert_eq!(token.footer(), "EXAMPLE FOOTER");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct FooterToken {
    footer: Comment,
    delimiter: Delimiter,
}

impl FooterToken {
    /// Tries to parse [`FooterToken`]. (e.g. `"footer EXAMPLE FOOTER"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (footer_tag, (delimiter_raw, delimiter), (footer_raw, footer))) = (
            wr!(tag_no_case("footer")),
            wr2!(Delimiter::parse),
            wr2!(Comment::parse),
        )
            .parse(input)?;

        let ret0 = ParseContainer::from(vec![footer_tag, delimiter_raw, footer_raw]);
        let ret1 = Self { footer, delimiter };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns footer's text
    pub fn footer(&self) -> &str {
        self.footer.as_str()
    }

    /// Returns delimiter
    pub fn delimiter(&self) -> Option<&str> {
        self.delimiter.delimiter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "footer EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "footer EXAMPLE \t FOOTER  \t";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "  \t");
        assert_eq!("footer EXAMPLE \t FOOTER", parsed);
        assert_eq!("EXAMPLE \t FOOTER", token.footer());

        Ok(())
    }

    #[test]
    fn test_parse_case_insensitive() -> anyhow::Result<()> {
        let testdata = "Footer EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "FOOTER EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "foOter EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        Ok(())
    }

    #[test]
    fn test_parse_with_delimiter() -> anyhow::Result<()> {
        let testdata = "footer : EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "footer:\t EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "footer \t:EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        let testdata = "footer:EXAMPLE FOOTER";
        let (rest, (parsed, token)) = FooterToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE FOOTER", token.footer());

        Ok(())
    }
}
