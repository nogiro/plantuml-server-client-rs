use plantuml_server_client_rs as pscr;

use clap::Parser;
use pscr::{Args, Config, Format, Method};

#[tokio::test]
async fn read() -> anyhow::Result<()> {
    let mut args = Args::parse();
    args.config = Some("./tests/assets/config/.pscr.conf".into());
    println!("args = {args:?}");

    let config = Config::load(&args).await?;
    assert_eq!(config.method, Method::Post);
    assert_eq!(config.url_prefix, "http://localhost:8080/");
    assert_eq!(config.format, Format::Svg);

    Ok(())
}
