#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"
ROOT_DIR="${SCRIPT_DIR}/../../"

function list_old_crates() {
  {
    cargo search plantuml-server-client-rs | grep ^plantuml-server-client-rs
    cargo search plantuml-parser | grep ^plantuml-parser
  } \
  | grep -v -f <(
    cargo run -q -- --version \
      | sed -n '/^  /s#  \(.*\)=\(.*\)#\1 = "\2"#p'
  )
}

if [ "$(list_old_crates | wc -l)" -eq 0 ]; then
  echo no update >&2
  exit 0
fi

echo update exists >&2

cargo login "${CARGO_PUBLISH_TOKEN}"

cargo publish --manifest-path "${ROOT_DIR}/crates/parser/Cargo.toml"
cargo publish --manifest-path "${ROOT_DIR}/Cargo.toml"
