use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr};
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, space0};
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a end keyword (`"@endXYX"`). (like `"@enduml\n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{EndLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = " @enduml \n";
/// let (rest, (raws, token)) = EndLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, " @enduml \n");
/// assert!(token.eq_diagram_kind("uml"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct EndLine {
    diagram_kind: ParseContainer,
    ibc: Option<InlineBlockCommentToken>,
}

impl EndLine {
    /// Tries to parse [`EndLine`]. (e.g. `" @enduml\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (diagram_kind, ibc) = lwc.into();
        let ret = Self { diagram_kind, ibc };

        Ok((rest, (parsed.into(), ret)))
    }

    pub fn eq_diagram_kind(&self, diagram_kind: &str) -> bool {
        self.diagram_kind == diagram_kind
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, ParseContainer)> {
    map(
        (wr!(space0), wr!(tag("@end")), wr!(alpha1), wr!(space0)),
        |parsed| {
            let diagram_kind = parsed.2.clone();
            let parsed = Vec::from(<[ParseContainer; 4]>::from(parsed));
            (parsed, diagram_kind)
        },
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_end_line() -> anyhow::Result<()> {
        let testdata = [
            ("@enduml\n", None),
            (" \t@enduml\n", None),
            ("@enduml \t\n", None),
            (" \t@enduml \t\n", None),
            (" /' comment '/ @enduml \t\n", Some(" /' comment '/ ")),
            ("  @enduml   /' comment '/ \n", Some("/' comment '/ ")),
        ];

        for (testdata, expected_comment) in testdata.into_iter() {
            println!("try: testdata = {testdata:?}");
            let (rest, (parsed, EndLine { diagram_kind, ibc })) = EndLine::parse(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(diagram_kind, "uml");
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);
        }

        Ok(())
    }
}
