use crate::StartDiagramToken;
use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr, wr2};
use nom::character::complete::space0;
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a start keyword (`"@startXYZ"`) parsed by [`StartDiagramToken`]. (like `"@startuml\n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{StartLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "@startuml\n";
/// let (rest, (raws, token)) = StartLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "@startuml\n");
/// assert_eq!(token.diagram_kind(), "uml");
/// assert_eq!(token.id(), None);
///
/// let input = "  @startXYZ diagram_0\n";
/// let (rest, (raws, token)) = StartLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "  @startXYZ diagram_0\n");
/// assert_eq!(token.diagram_kind(), "XYZ");
/// assert_eq!(token.id(), Some("diagram_0"));
///
/// let input = "@startsalt(id=diagram_1)  \n";
/// let (rest, (raws, token)) = StartLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "@startsalt(id=diagram_1)  \n");
/// assert_eq!(token.diagram_kind(), "salt");
/// assert_eq!(token.id(), Some("diagram_1"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct StartLine {
    token: StartDiagramToken,
    ibc: Option<InlineBlockCommentToken>,
}

impl StartLine {
    /// Tries to parse [`StartDiagramToken`]. (e.g. `" @startuml\n"`, `" @startuml ID\n"`, `" @startuml(id=ID)\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (token, ibc) = lwc.into();

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self { token, ibc };

        Ok((rest, (ret0, ret1)))
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

impl std::ops::Deref for StartLine {
    type Target = StartDiagramToken;
    fn deref(&self) -> &Self::Target {
        &self.token
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, StartDiagramToken)> {
    map(
        (wr!(space0), wr2!(StartDiagramToken::parse), wr!(space0)),
        |(p0, (p1, sdt), p2)| {
            let parsed = vec![p0, p1, p2];
            (parsed, sdt)
        },
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_start_line() -> anyhow::Result<()> {
        let testdata = [
            ("@startuml\n", (None, None)),
            (" \t@startuml\n", (None, None)),
            ("@startuml \t\n", (None, None)),
            (" \t@startuml \t\n", (None, None)),
            (" \t@startuml id_foo\t\n", (Some("id_foo"), None)),
            (" \t@startuml(id=id_bar)\t\n", (Some("id_bar"), None)),
            (
                " /' comment '/ \t@startuml id_buz\t\n",
                (Some("id_buz"), Some(" /' comment '/ \t")),
            ),
            (
                "  @startuml id_qux\t /' comment '/   \n",
                (Some("id_qux"), Some("/' comment '/   ")),
            ),
        ];

        for (testdata, expected) in testdata.into_iter() {
            let (expected_id, expected_ibc) = expected;

            let (rest, (parsed, StartLine { token, ibc })) = StartLine::parse(testdata.into())?;

            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(token.diagram_kind(), "uml");
            assert_eq!(token.id(), expected_id);
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_ibc);
        }

        Ok(())
    }
}
