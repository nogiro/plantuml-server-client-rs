use crate::{Client, Locate};
use anyhow::{Context as _, Result};
use std::path::PathBuf;
use std::sync::Arc;

/// Requests multiple input files in parallel.
pub async fn no_watch(
    client: Arc<Client>,
    input_files: Vec<PathBuf>,
    output: Arc<Locate>,
) -> Result<()> {
    let tasks = input_files.into_iter().map(|input| {
        let client = client.clone();
        let output = output.clone();
        tokio::spawn(async move { client.request(input.into(), &output).await })
    });

    let mut results = vec![];
    for task in tasks.into_iter() {
        results.push(task.await?);
    }
    let metadatas = results
        .into_iter()
        .inspect(|x| {
            let _ = x
                .as_ref()
                .inspect_err(|e| tracing::warn!("failed request detail: {e:?}"));
        })
        .filter(|result| result.is_ok())
        .collect::<Result<Vec<_>, _>>()
        .context("failed to request from files")?;
    tracing::debug!("requested: metadatas = {metadatas:?}");

    client
        .write_metadata_if_needed(metadatas)
        .await
        .context("failed to write metadata")?;

    Ok(())
}
