use plantuml_server_client_rs as pscr;

mod common;

use crate::common::config;
use pscr::{Client, Locate, Metadata};
use std::path::PathBuf;
use tokio::fs::{read_to_string, remove_file};

const OUTPUT: &str = "./outputs/tests/metadata";

#[tokio::test]
async fn simple() -> anyhow::Result<()> {
    const INPUT: &str = "./tests/assets/simple.puml";

    let input = Locate::new(Some(INPUT.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/simple.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/simple.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;

    assert_eq!(metadata.path(), &PathBuf::from(INPUT));
    assert_eq!(metadata.includes().len(), 0);
    assert_eq!(metadata.plantuml().len(), 1);
    let output_path = metadata.plantuml().get(0).unwrap().output_path();
    assert_eq!(
        output_path,
        &PathBuf::from(format!("{OUTPUT}/simple/0.svg"))
    );

    let snapshot = read_to_string("./tests/snapshot/request/simple/0.svg").await?;
    let generated = read_to_string(&output_path).await?;

    assert_eq!(snapshot, generated);

    Ok(())
}

#[tokio::test]
async fn multiple() -> anyhow::Result<()> {
    const INPUT: &str = "./tests/assets/multiple.puml";

    let input = Locate::new(Some(INPUT.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/multiple.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/multiple.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;

    assert_eq!(metadata.path(), &PathBuf::from(INPUT));
    assert_eq!(metadata.includes().len(), 0);
    assert_eq!(metadata.plantuml().len(), 2);

    let id_0 = metadata.plantuml().get(0).unwrap().id();
    let id_1 = metadata.plantuml().get(1).unwrap().id();
    assert_eq!(id_0, &Some("figure_multiple_1".to_string()));
    assert_eq!(id_1, &Some("figure_multiple_2".to_string()));

    let files = [
        "multiple/figure_multiple_1.svg",
        "multiple/figure_multiple_2.svg",
    ];
    for file in files {
        let snapshot = read_to_string(format!("./tests/snapshot/request/{file}")).await?;
        let generated = read_to_string(format!("{OUTPUT}/{file}")).await?;
        assert_eq!(snapshot, generated);
    }

    // same as above (from metadata version)
    for plantuml in metadata.plantuml().iter() {
        let output_path = plantuml.output_path();
        let snapshot_path = {
            let suffix = output_path
                .to_string_lossy()
                .to_string()
                .split_off(format!("{OUTPUT}/").len());
            format!("./tests/snapshot/request/{suffix}")
        };

        let snapshot = read_to_string(&snapshot_path).await?;
        let generated = read_to_string(&output_path).await?;
        assert_eq!(snapshot, generated);
    }

    Ok(())
}
#[tokio::test]
async fn include() -> anyhow::Result<()> {
    const INPUT: &str = "./tests/assets/include.puml";

    let input = Locate::new(Some(INPUT.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/include.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/include.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;

    assert_eq!(metadata.path(), &PathBuf::from(INPUT));
    assert_eq!(metadata.includes().len(), 1);
    assert_eq!(metadata.plantuml().len(), 3);

    let files = [
        "include/figure_include_1.svg",
        "include/figure_include_2.svg",
        // "include/figure_include_3.svg", unimplemented
    ];

    for file in files {
        let snapshot = read_to_string(format!("./tests/snapshot/request/{file}")).await?;
        let generated = read_to_string(format!("{OUTPUT}/{file}")).await?;
        assert_eq!(snapshot, generated);
    }

    // same as above (from metadata version)
    for plantuml in metadata.plantuml().iter() {
        if let Some("figure_include_3") = plantuml.id().as_ref().map(|x| x.as_str()) {
            // unimplemented
            continue;
        }

        let output_path = plantuml.output_path();
        let snapshot_path = {
            let suffix = output_path
                .to_string_lossy()
                .to_string()
                .split_off(format!("{OUTPUT}/").len());
            format!("./tests/snapshot/request/{suffix}")
        };

        let snapshot = read_to_string(&snapshot_path).await?;
        let generated = read_to_string(&output_path).await?;
        assert_eq!(snapshot, generated);
    }

    Ok(())
}

#[tokio::test]
async fn metadata() -> anyhow::Result<()> {
    const INPUT: &str = "./tests/assets/metadata/a.puml";

    let input = Locate::new(Some(INPUT.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/a.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/a.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;
    assert_eq!(metadata.path(), &PathBuf::from(INPUT));
    assert_eq!(metadata.includes().len(), 3);
    assert_eq!(metadata.plantuml().len(), 1);

    // ---------
    println!("includes: {:?}", metadata.includes());
    let mut iter = metadata.includes().iter();
    let (p1, i1) = iter.next().unwrap();
    let (p2, i2) = iter.next().unwrap();
    let (p3, i3) = iter.next().unwrap();
    assert!(iter.next().is_none());
    assert_eq!(i1.len(), 1);
    assert_eq!(i2.len(), 2);
    assert_eq!(i3.len(), 3);

    let i1_1 = i1.first().unwrap().clone();
    let i2_1 = i2.first().unwrap().clone();
    let i2_2 = i2.get(1).unwrap().clone();
    let i3_1 = i3.first().unwrap().clone();
    let i3_2 = i3.get(1).unwrap().clone();
    let i3_3 = i3.get(2).unwrap().clone();
    assert!(i1.get(1).is_none());
    assert!(i2.get(2).is_none());
    assert!(i3.get(3).is_none());
    assert_eq!(i1_1.normalized_path(), p1);
    assert_eq!(i2_1.normalized_path(), p2);
    assert_eq!(i2_2.normalized_path(), p2);
    assert_eq!(i3_1.normalized_path(), p3);
    assert_eq!(i3_2.normalized_path(), p3);
    assert_eq!(i3_3.normalized_path(), p3);

    // ---------
    let p1 = metadata.plantuml().get(0).unwrap().clone();
    assert!(metadata.plantuml().get(1).is_none());
    assert_eq!(p1.title(), &Some("a 2".to_string()));
    assert_eq!(p1.header(), &Some("a 2".to_string()));
    assert_eq!(p1.footer(), &Some("a 2".to_string()));

    // ----
    const INPUT_A: &str = "./tests/assets/metadata/dir_a/a.puml";

    let input = Locate::new(Some(INPUT_A.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/dir_a/a.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/dir_a/a.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;
    assert_eq!(metadata.path(), &PathBuf::from(INPUT_A));
    assert_eq!(metadata.includes().len(), 2);
    assert_eq!(metadata.plantuml().len(), 1);

    // ---------
    println!("includes `dir_a`: {:?}", metadata.includes());
    let mut iter = metadata.includes().iter();
    let (p1, i1) = iter.next().unwrap();
    let (p2, i2) = iter.next().unwrap();
    assert!(iter.next().is_none());
    assert_eq!(i1.len(), 1);
    assert_eq!(i2.len(), 2);

    let i1_1 = i1.first().unwrap().clone();
    let i2_1 = i2.first().unwrap().clone();
    let i2_2 = i2.get(1).unwrap().clone();
    assert!(i1.get(1).is_none());
    assert!(i2.get(2).is_none());
    assert_eq!(i1_1.normalized_path(), p1);
    assert_eq!(i2_1.normalized_path(), p2);
    assert_eq!(i2_2.normalized_path(), p2);

    // ---------
    let p1 = metadata.plantuml().get(0).unwrap().clone();
    assert!(metadata.plantuml().get(1).is_none());
    assert_eq!(p1.title(), &Some("dir a simple_include".to_string()));
    assert_eq!(p1.header(), &Some("dir a simple_include".to_string()));
    assert_eq!(p1.footer(), &Some("dir a simple_include".to_string()));

    // ----
    const INPUT_B: &str = "./tests/assets/metadata/dir_b/a.puml";

    let input = Locate::new(Some(INPUT_B.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/dir_b/a.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/dir_b/a.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;
    assert_eq!(metadata.path(), &PathBuf::from(INPUT_B));
    assert_eq!(metadata.includes().len(), 1);
    assert_eq!(metadata.plantuml().len(), 2);

    // ---------
    println!("includes `dir_b`: {:?}", metadata.includes());
    let mut iter = metadata.includes().iter();
    let (p1, i1) = iter.next().unwrap();
    assert!(iter.next().is_none());
    assert_eq!(i1.len(), 2);

    let i1_1 = i1.first().unwrap().clone();
    let i1_2 = i1.get(1).unwrap().clone();
    assert!(i1.get(2).is_none());
    assert_eq!(i1_1.normalized_path(), p1);
    assert_eq!(i1_2.normalized_path(), p1);

    // ---------
    let p1 = metadata.plantuml().get(0).unwrap().clone();
    let p2 = metadata.plantuml().get(1).unwrap().clone();
    assert!(metadata.plantuml().get(2).is_none());
    assert_eq!(p1.title(), &Some("dir c foo".to_string()));
    assert_eq!(p1.header(), &Some("dir c foo".to_string()));
    assert_eq!(p1.footer(), &Some("dir c foo".to_string()));
    assert_eq!(p2.title(), &Some("dir b overwrite_metadata".to_string()));
    assert_eq!(p2.header(), &Some("dir b overwrite_metadata".to_string()));
    assert_eq!(p2.footer(), &Some("dir b overwrite_metadata".to_string()));

    // ----
    const INPUT_C: &str = "./tests/assets/metadata/dir_c/a.puml";

    let input = Locate::new(Some(INPUT_C.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/dir_c/a.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![client.request(input, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected = read_to_string("./tests/snapshot/metadata/dir_c/a.json").await?;
    assert_eq!(metadata_str, expected);

    let metadata = serde_json::from_str::<Metadata>(&metadata_str)?;
    assert_eq!(metadata.path(), &PathBuf::from(INPUT_C));
    assert_eq!(metadata.includes().len(), 0);
    assert_eq!(metadata.plantuml().len(), 2);

    // ---------
    println!("includes `dir_c`: {:?}", metadata.includes());
    let mut iter = metadata.includes().iter();
    assert!(iter.next().is_none());

    // ---------
    let p1 = metadata.plantuml().get(0).unwrap().clone();
    let p2 = metadata.plantuml().get(1).unwrap().clone();
    assert!(metadata.plantuml().get(2).is_none());
    assert_eq!(p1.title(), &Some("dir c many_metadata 3".to_string()));
    assert_eq!(p1.header(), &Some("dir c many_metadata 2".to_string()));
    assert_eq!(p1.footer(), &Some("dir c many_metadata 1".to_string()));
    assert_eq!(p2.title(), &Some("dir c foo".to_string()));
    assert_eq!(p2.header(), &Some("dir c foo".to_string()));
    assert_eq!(p2.footer(), &Some("dir c foo".to_string()));

    Ok(())
}

/// Checks `MetadataStore`.
#[tokio::test]
async fn store() -> anyhow::Result<()> {
    const INPUT_0: &str = "./tests/assets/simple.puml";
    const INPUT_1: &str = "./tests/assets/multiple.puml";

    let input_0 = Locate::new(Some(INPUT_0.into()));
    let input_1 = Locate::new(Some(INPUT_1.into()));
    let output = Locate::new(Some(OUTPUT.into()));
    let metadata: PathBuf = "./outputs/tests/metadata/store.json".into();
    let config = config!(metadata.clone());

    let client = Client::new(config);
    let _ = remove_file(&metadata).await;
    let requested_metadata = vec![
        client.request(input_0, &output).await?,
        client.request(input_1, &output).await?,
    ];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    let expected_str = read_to_string("./tests/snapshot/store_0.json").await?;
    assert_eq!(metadata_str, expected_str); // metadata has input_0 and input_1

    let input_1 = Locate::new(Some(INPUT_1.into()));
    let requested_metadata = vec![client.request(input_1, &output).await?];
    client.write_metadata_if_needed(requested_metadata).await?;

    let metadata_str = read_to_string(&metadata).await?;
    assert_eq!(metadata_str, expected_str); // no change

    Ok(())
}
