use crate::IncludeToken;
use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr, wr2};
use nom::character::complete::space0;
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a [`IncludeToken`]. (like `"\t!include foo.puml  \n"` or `"\r!include bar.iuml!buz  \n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{IncludeLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "!include foo.puml \n";
/// let (rest, (raws, token)) = IncludeLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "!include foo.puml \n");
/// assert_eq!(token.filepath(), "foo.puml");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), None);
///
/// let input = "!include_many bar.iuml!1  \n";
/// let (rest, (raws, token)) = IncludeLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "!include_many bar.iuml!1  \n");
/// assert_eq!(token.filepath(), "bar.iuml");
/// assert_eq!(token.index(), Some(1));
/// assert_eq!(token.id(), Some("1"));
///
/// let input = "  !include_once baz.txt!qux\n";
/// let (rest, (raws, token)) = IncludeLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "  !include_once baz.txt!qux\n");
/// assert_eq!(token.filepath(), "baz.txt");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), Some("qux"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct IncludeLine {
    token: IncludeToken,
    ibc: Option<InlineBlockCommentToken>,
}

impl IncludeLine {
    /// Tries to parse [`IncludeLine`]. (e.g. `" !include foo.puml\n"`, `"!include  bar.iuml!buz \n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (token, ibc) = lwc.into();

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self { token, ibc };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns the [`IncludeToken`] included.
    pub fn token(&self) -> &IncludeToken {
        &self.token
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

impl std::ops::Deref for IncludeLine {
    type Target = IncludeToken;
    fn deref(&self) -> &Self::Target {
        &self.token
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, IncludeToken)> {
    map(
        (wr!(space0), wr2!(IncludeToken::parse), wr!(space0)),
        |(p0, (p1, token), p2)| (vec![p0, p1, p2], token),
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_start_line() -> anyhow::Result<()> {
        let testdata = [
            (" !include i1.puml\n", ("i1.puml", None, None, None)),
            (
                " !include i2.puml!1\n",
                ("i2.puml", Some(1), Some("1"), None),
            ),
            (
                "\t !include i3.puml!id3\n",
                ("i3.puml", None, Some("id3"), None),
            ),
            (
                " /' comment '/ !include i4.puml!4  \n",
                ("i4.puml", Some(4), Some("4"), Some(" /' comment '/ ")),
            ),
            (
                " !include i5.puml!5 /' comment '/ \n",
                ("i5.puml", Some(5), Some("5"), Some("/' comment '/ ")),
            ),
        ];

        for (testdata, expected) in testdata.into_iter() {
            let (expected_file, expected_index, expected_id, expected_ibc) = expected;
            let (rest, (parsed, IncludeLine { token, ibc })) = IncludeLine::parse(testdata.into())?;

            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(token.filepath(), expected_file);
            assert_eq!(token.index(), expected_index);
            assert_eq!(token.id(), expected_id);
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_ibc);
        }

        Ok(())
    }
}
