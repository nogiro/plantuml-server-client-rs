pub mod method;

use crate::encode::Encoder;
use crate::{Format, Method, UrlBuilder};
use anyhow::{Context as _, Result};
use bytes::Bytes;
use deflate::deflate_bytes;
use futures::Stream;

pub struct Request<'a> {
    method: &'a Method,
    url_prefix: &'a String,
    format: &'a Format,

    client: &'a reqwest::Client,
    encoder: &'a Encoder,
}

impl<'a> Request<'a> {
    pub fn new(
        method: &'a Method,
        url_prefix: &'a String,
        format: &'a Format,
        client: &'a reqwest::Client,
        encoder: &'a Encoder,
    ) -> Self {
        Self {
            method,
            url_prefix,
            format,
            client,
            encoder,
        }
    }

    pub async fn request(
        &self,
        input: String,
    ) -> Result<impl Stream<Item = Result<Bytes, reqwest::Error>> + Unpin + use<>> {
        match self.method {
            Method::Post => {
                let url = UrlBuilder::new()
                    .prefix(self.url_prefix.clone())
                    .format(self.format.clone())
                    .build("".into());
                tracing::info!("url = {url:?}");

                let resp = self
                    .client
                    .post(url)
                    .body(input)
                    .send()
                    .await
                    .context("failed to request (POST)")?
                    .bytes_stream();
                Ok(resp)
            }

            Method::Get => {
                let compressed = deflate_bytes(input.as_bytes());
                let encoded = self.encoder.encode(&compressed);
                let url = UrlBuilder::new()
                    .prefix(self.url_prefix.clone())
                    .format(self.format.clone())
                    .build(encoded);
                tracing::info!("url = {url:?}");

                let resp = self
                    .client
                    .get(url)
                    .send()
                    .await
                    .context("failed to request (GET)")?
                    .bytes_stream();
                Ok(resp)
            }
        }
    }
}
