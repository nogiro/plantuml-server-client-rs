use crate::Error;
use normalize_path::NormalizePath;
use std::path::PathBuf;

/// A resolver that keeps relative paths on a stack for recursive include process.
///
/// # Examples
///
/// ```
/// use plantuml_parser::PathResolver;
/// use std::path::PathBuf;
///
/// # fn main() -> anyhow::Result<()> {
/// let resolver_0 = PathResolver::new("./dir-1/x1.puml");
/// assert_eq!(resolver_0.build()?, PathBuf::from("dir-1/x1.puml"));
/// assert_eq!(resolver_0.build_without_normalize()?, PathBuf::from("./dir-1/x1.puml"));
///
/// // For example, `./dir-1/x1.puml` includes `./dir-2/x2.puml`.
/// let mut resolver_0_0 = resolver_0.clone();
/// resolver_0_0.add("./dir-2/x2.puml".into());
/// assert_eq!(resolver_0_0.build()?, PathBuf::from("dir-1/dir-2/x2.puml"));
/// assert_eq!(resolver_0_0.build_without_normalize()?, PathBuf::from("./dir-1/dir-2/x2.puml"));
///
/// // For example, `./dir-1/x1.puml` includes `./dir-3/x3.puml`.
/// let mut resolver_0_1 = resolver_0.clone();
/// resolver_0_1.add("./dir-3/x3.puml".into());
/// assert_eq!(resolver_0_1.build()?, PathBuf::from("dir-1/dir-3/x3.puml"));
/// assert_eq!(resolver_0_1.build_without_normalize()?, PathBuf::from("./dir-1/dir-3/x3.puml"));
///
/// // For example, `./dir-1/dir-3/x3.puml` includes `../../dir-4/x4.puml`.
/// let mut resolver_0_1_0 = resolver_0_1.clone();
/// resolver_0_1_0.add("../../dir-4/x4.puml".into());
/// assert_eq!(resolver_0_1_0.build()?, PathBuf::from("dir-4/x4.puml"));
/// assert_eq!(resolver_0_1_0.build_without_normalize()?, PathBuf::from("./dir-1/dir-3/../../dir-4/x4.puml"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct PathResolver {
    paths: Vec<PathBuf>,
}

/// The error type for [`PathResolver`]'s operations.
#[derive(thiserror::Error, Debug)]
pub enum PathResolverError {
    #[error("PathResolver has no paths (unreachable)")]
    None,
}

impl PathResolver {
    /// Creates a new [`PathResolver`].
    ///
    /// * `path` - A base path.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::PathResolver;
    /// # fn main() {
    /// let _ = PathResolver::new("path");
    /// # }
    /// ```
    pub fn new<T>(path: T) -> Self
    where
        T: Into<PathBuf>,
    {
        Self {
            paths: vec![path.into()],
        }
    }

    /// Adds a [`PathBuf`] onto an inner stack.
    ///
    /// * `path` - A new path.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::PathResolver;
    /// # use std::path::PathBuf;
    /// #
    /// # fn main() -> anyhow::Result<()> {
    /// let mut resolver = PathResolver::new("dir/file");
    /// resolver.add("file-2".into());
    /// assert_eq!(resolver.build()?, PathBuf::from("dir/file-2"));
    /// # Ok(())
    /// # }
    /// ```
    pub fn add(&mut self, path: PathBuf) {
        self.paths.push(path);
    }

    /// Returns the resolved normalized path. All `PathBuf`s in the stack are treated as paths relative to the file.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::PathResolver;
    /// # use std::path::PathBuf;
    /// #
    /// # fn main() -> anyhow::Result<()> {
    /// let mut resolver = PathResolver::new("dir-1/file-1");
    /// assert_eq!(resolver.build()?, PathBuf::from("dir-1/file-1"));
    /// resolver.add("../dir-2/file-2".into());
    /// assert_eq!(resolver.build()?, PathBuf::from("dir-2/file-2"));
    /// resolver.add("../dir-3/file-3".into());
    /// assert_eq!(resolver.build()?, PathBuf::from("dir-3/file-3"));
    /// # Ok(())
    /// # }
    /// ```
    pub fn build(&self) -> Result<PathBuf, Error> {
        let ret = self.build_without_normalize()?.normalize();
        Ok(ret)
    }

    /// Returns the resolved the path not normalized.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::PathResolver;
    /// # use std::path::PathBuf;
    /// #
    /// # fn main() -> anyhow::Result<()> {
    /// let mut resolver = PathResolver::new("dir-1/file-1");
    /// assert_eq!(
    ///     resolver.build_without_normalize()?,
    ///     PathBuf::from("dir-1/file-1")
    /// );
    ///
    /// resolver.add("../dir-2/file-2".into());
    /// assert_eq!(
    ///     resolver.build_without_normalize()?,
    ///     PathBuf::from("dir-1/../dir-2/file-2")
    /// );
    ///
    /// resolver.add("../dir-3/file-3".into());
    /// assert_eq!(
    ///     resolver.build_without_normalize()?,
    ///     PathBuf::from("dir-1/../dir-2/../dir-3/file-3")
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn build_without_normalize(&self) -> Result<PathBuf, Error> {
        let mut iter = self.paths.iter();

        let mut last_file_name;
        let mut ret = iter.next().ok_or(PathResolverError::None)?.clone();
        last_file_name = ret.file_name().map(|x| x.to_os_string());
        ret.pop();

        for path in iter {
            let mut path = path.clone();
            last_file_name = path.file_name().map(|x| x.to_os_string());
            path.pop();
            ret.push(path);
        }

        if let Some(last_file_name) = last_file_name {
            ret.push(last_file_name);
        }

        Ok(ret)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_path_resolver() -> anyhow::Result<()> {
        let path_base = PathResolver::new("./base_dir/base_file.puml");
        let mut path1 = path_base.clone();
        path1.add("./dir1-1/file1-1.puml".into());
        println!("path1 = {path1:?}, {:?}", path1.build()?);
        assert_eq!(
            path1.build()?,
            PathBuf::from("base_dir/dir1-1/file1-1.puml")
        );

        let mut path2 = path1.clone();
        path1.add("./dir2-1/file2-1.puml".into());
        println!("path1 = {path1:?}, {:?}", path1.build()?);
        assert_eq!(
            path1.build()?,
            PathBuf::from("base_dir/dir1-1/dir2-1/file2-1.puml")
        );
        path2.add("./dir1-1/file1-1.puml".into());
        println!("path2 = {path2:?}, {:?}", path2.build()?);
        assert_eq!(
            path2.build()?,
            PathBuf::from("base_dir/dir1-1/dir1-1/file1-1.puml")
        );

        let mut path1 = path_base.clone();
        path1.add("./dir1-2/file1-2.puml".into());
        println!("path1 = {path1:?}, {:?}", path1.build()?);
        assert_eq!(
            path1.build()?,
            PathBuf::from("base_dir/dir1-2/file1-2.puml")
        );

        let mut path2 = path1.clone();
        path1.add("./dir2-2/file2-2.puml".into());
        println!("path1 = {path1:?}, {:?}", path1.build()?);
        assert_eq!(
            path1.build()?,
            PathBuf::from("base_dir/dir1-2/dir2-2/file2-2.puml")
        );
        path2.add("./dir1-2/file1-2.puml".into());
        println!("path2 = {path2:?}, {:?}", path2.build()?);
        assert_eq!(
            path2.build()?,
            PathBuf::from("base_dir/dir1-2/dir1-2/file1-2.puml")
        );

        Ok(())
    }

    #[test]
    fn test_absolute_path_resolver() -> anyhow::Result<()> {
        let path_base = PathResolver::new("/base_dir/base_file.puml");
        let mut path = path_base.clone();
        path.add("./dir1-1/file1-1.puml".into());
        println!("path = {path:?}, {:?}", path.build()?);
        assert_eq!(
            path.build()?,
            PathBuf::from("/base_dir/dir1-1/file1-1.puml")
        );

        let mut path = path_base.clone();
        path.add("/dir1-1/file1-1.puml".into());
        println!("path = {path:?}, {:?}", path.build()?);
        assert_eq!(path.build()?, PathBuf::from("/dir1-1/file1-1.puml"));

        Ok(())
    }
}
