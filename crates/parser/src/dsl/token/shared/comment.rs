use crate::{ParseContainer, ParseResult, wr};
use nom::branch::alt;
use nom::bytes::complete::{is_not, tag, take_until};
use nom::character::complete::{space0, space1};
use nom::combinator::{map, not};
use nom::multi::many0;
use nom::{IResult, Parser};

/// A trimed strings without last block comment. (e.g. `"EXAMPLE COMMENT"`.)
///
/// - `"EXAMPLE COMMENT /' comment '/"` -> `"EXAMPLE COMMENT"` + `" /' comment '/"`
#[derive(Clone, Debug)]
pub struct Comment {
    comment: ParseContainer,
}

impl Comment {
    /// Tries to parse [`Comment`].
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        fn inner_parser(input: ParseContainer) -> IResult<ParseContainer, ParseContainer> {
            let (mut rest, (parsed0, mut parsed1)) = (
                wr!(is_not(" \t\r\n")),
                many0(alt((
                    map(one_oneline_block_comment, |p| (p, true)),
                    map((wr!(space1), wr!(is_not(" \t\r\n"))), |(p0, p1)| {
                        (vec![p0, p1].into(), false)
                    }),
                ))),
            )
                .parse(input)?;

            match parsed1.last() {
                Some(x) if x.1 => {
                    let restroe = parsed1.pop().unwrap();
                    rest = vec![restroe.0, rest].into();
                }
                _ => {}
            }

            let parsed = parsed1
                .into_iter()
                .fold(vec![parsed0], |mut acc, (item0, _)| {
                    acc.push(item0);
                    acc
                });

            Ok((rest, parsed.into()))
        }

        let (rest, parsed) = alt((
            map((inner_parser, not(one_oneline_block_comment)), |(p0, _)| p0),
            inner_parser,
        ))
        .parse(input)?;

        let ret = Comment {
            comment: parsed.clone(),
        };

        Ok((rest, (parsed, ret)))
    }

    /// Returns the string slice in [`Comment`].
    pub fn as_str(&self) -> &str {
        self.comment.as_str()
    }
}

/// parse like `" /' comment '/ "`. not `" /' c0 '/ /' c1 '/ "`.
fn one_oneline_block_comment(input: ParseContainer) -> IResult<ParseContainer, ParseContainer> {
    let (rest, parsed) = map(
        (
            wr!(space0),
            wr!(tag("/'")),
            wr!(take_until("'/")),
            wr!(tag("'/")),
        ),
        |(p0, p1, p2, p3)| vec![p0, p1, p2, p3].into(),
    )
    .parse(input)?;

    Ok((rest, parsed))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = [
            ("EXAMPLE COMMENT", ""),
            ("EXAMPLE \t COMMENT  \t", "  \t"),
            ("EXAMPLE COMMENT /' comment '/", " /' comment '/"),
            ("EXAMPLE COMMENT /' comment '/   ", " /' comment '/   "),
            ("EXAMPLE COMMENT /' c0 '/  /' c1 '/  ", "  /' c1 '/  "),
        ];

        for (testdata, expected_rest) in testdata.into_iter() {
            println!("try: testdata = {testdata:?}");
            let (rest, (parsed, Comment { comment })) = Comment::parse(testdata.into())?;
            println!("rest = {rest}");
            println!("expected_rest = {expected_rest}");
            assert_eq!(rest, expected_rest);
            let testdata = &testdata[0..(testdata.len() - rest.len())];
            assert_eq!(testdata, parsed.as_str());
            assert_eq!(testdata, comment.as_str());
        }

        Ok(())
    }

    #[test]
    fn test_parse_one_oneline_block_comment() -> anyhow::Result<()> {
        let (rest, parsed) = one_oneline_block_comment(" /' comment '/ \n".into())?;
        assert_eq!(rest, " \n");
        assert_eq!(parsed, " /' comment '/");

        let (rest, parsed) = one_oneline_block_comment(" /' c0 '/ /' c1 '/ \n".into())?;
        assert_eq!(rest, " /' c1 '/ \n");
        assert_eq!(parsed, " /' c0 '/");

        Ok(())
    }
}
