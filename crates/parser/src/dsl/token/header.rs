use crate::dsl::token::shared::{Comment, Delimiter};
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::Parser;
use nom::bytes::complete::tag_no_case;

/// A token sequence that is around the header keyword. (like `"header EXAMPLE HEADER"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{HeaderToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "header  EXAMPLE HEADER   "; /// The last "   " is rest parts
/// let (rest, (raws, token)) = HeaderToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "   ");
/// assert_eq!(combined_raw, "header  EXAMPLE HEADER");
/// assert_eq!(token.header(), "EXAMPLE HEADER");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct HeaderToken {
    header: Comment,
    delimiter: Delimiter,
}

impl HeaderToken {
    /// Tries to parse [`HeaderToken`]. (e.g. `"header EXAMPLE HEADER"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (header_tag, (delimiter_raw, delimiter), (header_raw, header))) = (
            wr!(tag_no_case("header")),
            wr2!(Delimiter::parse),
            wr2!(Comment::parse),
        )
            .parse(input)?;

        let ret0 = ParseContainer::from(vec![header_tag, delimiter_raw, header_raw]);
        let ret1 = Self { header, delimiter };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns header's text
    pub fn header(&self) -> &str {
        self.header.as_str()
    }

    /// Returns delimiter
    pub fn delimiter(&self) -> Option<&str> {
        self.delimiter.delimiter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "header EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "header EXAMPLE \t HEADER  \t";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "  \t");
        assert_eq!("header EXAMPLE \t HEADER", parsed);
        assert_eq!("EXAMPLE \t HEADER", token.header());

        Ok(())
    }

    #[test]
    fn test_parse_case_insensitive() -> anyhow::Result<()> {
        let testdata = "Header EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "HEADER EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "heAder EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        Ok(())
    }

    #[test]
    fn test_parse_with_delimiter() -> anyhow::Result<()> {
        let testdata = "header : EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "header: \tEXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "header\t :EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        let testdata = "header:EXAMPLE HEADER";
        let (rest, (parsed, token)) = HeaderToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE HEADER", token.header());

        Ok(())
    }
}
