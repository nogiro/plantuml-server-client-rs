#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"
ROOT_DIR="${SCRIPT_DIR}/../../"

# shellcheck source=utility/shared/shared.sh
source "${SCRIPT_DIR}/../shared/shared.sh"

# shellcheck source=utility/bump-version/functions.sh
source "${SCRIPT_DIR}/functions.sh"

TARGET_PART='patch'

case "${1:-}" in
  "major")
    TARGET_PART='major';;
  "minor")
    TARGET_PART='minor';;
  *)
    ;;
esac

if [ "$(current_versions "${ROOT_DIR}" | wc -l)" -gt 1 ]; then
  echo unmatch \"version\" in Cargo.toml >&2
  exit 1
fi

CURRENT_VERSION="$(current_versions "${ROOT_DIR}")"

if [ "$(echo "${CURRENT_VERSION}" | grep -o '[0-9]*\.[0-9]*\.[0-9]*')" != "${CURRENT_VERSION}" ]; then
  echo \"version\" is unexpected format: \""${CURRENT_VERSION}"\" >&2
  exit 1
fi

NEXT_VERSION="$(next_semver "${TARGET_PART}" "${CURRENT_VERSION}")"

echo "current: ${CURRENT_VERSION}" >&2
echo "next: ${NEXT_VERSION}" >&2

edit_versions "${ROOT_DIR}" "${NEXT_VERSION}"
cargo build
