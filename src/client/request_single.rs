use crate::client::load_metadata::LoadMetadataClient;
use crate::client::write_combined::WriteCombinedClient;
use crate::encode::Encoder;
use crate::request::Request;
use crate::{Client, CollectedContainer, Config, Locate, PlantUmlMetadataItem};
use anyhow::{Context as _, Result};
use plantuml_parser::PlantUmlContent;

pub(super) struct RequestSingleClient<'a> {
    pub config: &'a Config,

    pub client: &'a reqwest::Client,
    pub encoder: &'a Encoder,
}

impl RequestSingleClient<'_> {
    pub async fn request_single(
        &self,
        plantuml_metadata: &mut Vec<PlantUmlMetadataItem>,
        idx: usize,
        data: &PlantUmlContent,
        collected: &CollectedContainer,
        input: &Locate,
        output: &Locate,
    ) -> Result<()> {
        let method = &self.config.method;
        let url_prefix = &self.config.url_prefix;
        let format = &self.config.format;

        let id = data.id().map(|x| x.to_string());
        let identifier = id.clone().unwrap_or_else(|| idx.to_string());

        let output_path = output.output_path(input, &identifier, format);
        let input_data = data
            .construct(input.to_path_buf(), collected.includes())
            .context("failed to construct PlantUML content")?;
        tracing::trace!("data = {data:?}");
        tracing::debug!("input_data = {input_data:?}");

        let combined_output_path = WriteCombinedClient::from(self)
            .write_combined_and_then_return_path(&identifier, input_data.clone(), input, output)
            .await
            .context("failed to write combined in `request_single()`")?;

        let metadata_item = LoadMetadataClient::from(self)
            .load_metadata(&input_data, &output_path, combined_output_path, id, idx)
            .context("failed to load metadata in `request_single()`")?;
        plantuml_metadata.push(metadata_item);

        let resp = Request::new(method, url_prefix, format, self.client, self.encoder)
            .request(input_data)
            .await
            .with_context(|| format!("failed to request: idx = {idx}"))?;

        output_path
            .write(resp)
            .await
            .with_context(|| format!("write: data = {data:?}, output = {output_path:?}"))?;

        Ok(())
    }
}

impl<'a> From<&'a Client> for RequestSingleClient<'a> {
    fn from(client: &'a Client) -> Self {
        Self {
            config: &client.config,
            client: &client.client,
            encoder: &client.encoder,
        }
    }
}
