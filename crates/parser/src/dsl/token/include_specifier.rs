use crate::DiagramIdToken;
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::bytes::complete::take_while1;
use nom::{IResult, Parser};

/// A token sequence that is the include specifier ([`DiagramIdToken`]) around the include keyword. (like `"foo.puml"` or `"bar.iuml!buz"`.)
///
/// * `[relative filepath]`
/// * `[relative filepath]![index]`
/// * `[relative filepath]![id]`
///
/// # Examples
///
/// ```
/// use plantuml_parser::{IncludeSpecifierToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "filepath_0 rest";
/// let (rest, (raws, token)) = IncludeSpecifierToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "filepath_0");
/// assert_eq!(token.filepath(), "filepath_0");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), None);
///
/// let input = "file_path_1!diagram_2 rest";
/// let (rest, (raws, token)) = IncludeSpecifierToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "file_path_1!diagram_2");
/// assert_eq!(token.filepath(), "file_path_1");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), Some("diagram_2"));
///
/// let input = "file_path_2!4 rest";
/// let (rest, (raws, token)) = IncludeSpecifierToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "file_path_2!4");
/// assert_eq!(token.filepath(), "file_path_2");
/// assert_eq!(token.index(), Some(4));
/// assert_eq!(token.id(), Some("4"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct IncludeSpecifierToken {
    filepath: ParseContainer,
    id: Option<DiagramIdToken>,
}

impl IncludeSpecifierToken {
    /// Tries to parse [`IncludeSpecifierToken`]. (e.g. `"foo.puml"`, `"bar.iuml!buz"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let ret = alt((wr2!(parse_file_with_id), wr2!(parse_only_file))).parse(input)?;
        Ok(ret)
    }

    /// Returns the filepath specified by include keyword.
    pub fn filepath(&self) -> &str {
        self.filepath.as_str()
    }

    /// Returns the index specified by include keyword if existed.
    pub fn index(&self) -> Option<usize> {
        self.id.as_ref().and_then(|x| x.id().parse().ok())
    }

    /// Returns the ID specified by include keyword if existed.
    pub fn id(&self) -> Option<&str> {
        self.id.as_ref().map(|x| x.id())
    }
}

/// parse like `"foo.puml"`
///
/// TODO: accept escaped space charactor: like `"foo\ bar.puml"` ("\ "?)
fn parse_only_file(input: ParseContainer) -> ParseResult<IncludeSpecifierToken> {
    let result: IResult<_, _> = wr!(take_while1(|c: char| {
        !['!', ' ', '\t', '\n', '\r'].contains(&c)
    }))(input);
    let (rest, filepath) = result?;

    let parsed_raw = filepath.clone();
    let parsed = IncludeSpecifierToken { filepath, id: None };
    Ok((rest, (parsed_raw, parsed)))
}

/// parse like `"bar.iuml!buz"`
fn parse_file_with_id(input: ParseContainer) -> ParseResult<IncludeSpecifierToken> {
    let (input, ((file_raw, file), tag, (id_raw, id))) = (
        wr2!(parse_only_file),
        wr!(tag("!")),
        wr2!(DiagramIdToken::parse),
    )
        .parse(input)?;

    let parsed_raw = ParseContainer::from(vec![file_raw, tag, id_raw]);
    let parsed = IncludeSpecifierToken {
        filepath: file.filepath,
        id: Some(id),
    };

    Ok((input, (parsed_raw, parsed)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "foo.puml";
        let (rest, (parsed, include)) = IncludeSpecifierToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed, testdata);
        assert_eq!(include.filepath(), "foo.puml");
        assert!(include.id().is_none());

        let testdata = "foo.puml!bar";
        let (rest, (parsed, include)) = IncludeSpecifierToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed, testdata);
        assert_eq!(include.filepath(), "foo.puml");
        assert_eq!(include.id(), Some("bar"));

        Ok(())
    }
}
