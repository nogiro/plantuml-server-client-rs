# `./docs/include/README.md`

There are the Markdown files included from source code in this directory.

The `plantuml-parser` crate (places [`./../../crates/parser/`](`./../../crates/parser/`)) references by symbolic link (`./../../crates/parser/docs/` -> `./../../docs/`.)
