use crate::{ParseContainer, ParseResult, wr};
use nom::bytes::complete::take_while1;
use nom::{AsChar, Parser};

/// A token sequence of `ID` included in the around of start keyword (like `"@startuml ID"` or `"@startuml(id=ID)"`)
///
/// # Examples
///
/// ```
/// use plantuml_parser::DiagramIdToken;
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "id_0 rest";
/// let (rest, (raw, token)) = DiagramIdToken::parse(input.into())?;
/// assert_eq!(rest, " rest");
/// assert_eq!(raw, "id_0");
/// assert_eq!(token.id(), "id_0");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct DiagramIdToken {
    id: ParseContainer,
}

impl DiagramIdToken {
    /// Tries to parse [`DiagramIdToken`]
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (id,)) =
            (wr!(take_while1(|c: char| c.is_alphanum() || '_' == c)),).parse(input)?;

        Ok((rest, (id.clone(), Self { id })))
    }

    /// Returns parsed `ID`
    pub fn id(&self) -> &str {
        self.id.as_str()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        assert!(DiagramIdToken::parse("".into()).is_err());
        assert!(DiagramIdToken::parse(" ".into()).is_err());

        let testdata = "aaa  ";
        let (rest, (parsed, id)) = DiagramIdToken::parse(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, "aaa");
        assert_eq!(id.id(), "aaa");

        Ok(())
    }
}
