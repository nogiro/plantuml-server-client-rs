mod block_comment;

pub use block_comment::BlockComment;

use crate::{ParseContainer, ParseResult};

/// A block of PlantUML
#[derive(Clone, Debug)]
pub struct PlantUmlBlock {
    kind: PlantUmlBlockKind,
}

/// A kind of PlantUML blocks to be handled by `plantuml-parser`.
#[derive(Clone, Debug)]
pub enum PlantUmlBlockKind {
    /// ```puml
    /// /' begin comment
    /// in comment
    /// end comment '/
    /// ```
    BlockComment(BlockComment),
}

impl PlantUmlBlock {
    /// Tries to parse [`PlantUmlBlock`].
    /// NOTE: At the moment, only [`BlockComment`] is handled, so this implementation is specifically tailored for [`BlockComment`].
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, comment)) = BlockComment::parse(input)?;

        let ret = Self {
            kind: PlantUmlBlockKind::BlockComment(comment),
        };
        Ok((rest, (parsed, ret)))
    }

    /// Returns the reference of [`PlantUmlBlockKind`].
    pub fn kind(&self) -> &PlantUmlBlockKind {
        &self.kind
    }
}
