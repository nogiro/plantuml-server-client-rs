use crate::dsl::token::shared::{Comment, Delimiter};
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::Parser;
use nom::bytes::complete::tag_no_case;

/// A token sequence that is around the title keyword. (like `"title EXAMPLE TITLE"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{TitleToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "title  EXAMPLE TITLE   "; /// The last "   " is rest parts
/// let (rest, (raws, token)) = TitleToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "   ");
/// assert_eq!(combined_raw, "title  EXAMPLE TITLE");
/// assert_eq!(token.title(), "EXAMPLE TITLE");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct TitleToken {
    title: Comment,
    delimiter: Delimiter,
}

impl TitleToken {
    /// Tries to parse [`TitleToken`]. (e.g. `"title EXAMPLE TITLE"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (title_tag, (delimiter_raw, delimiter), (title_raw, title))) = (
            wr!(tag_no_case("title")),
            wr2!(Delimiter::parse),
            wr2!(Comment::parse),
        )
            .parse(input)?;

        let ret0 = ParseContainer::from(vec![title_tag, delimiter_raw, title_raw]);
        let ret1 = Self { title, delimiter };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns title's text
    pub fn title(&self) -> &str {
        self.title.as_str()
    }

    /// Returns delimiter
    pub fn delimiter(&self) -> Option<&str> {
        self.delimiter.delimiter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "title EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(None, token.delimiter());

        Ok(())
    }

    #[test]
    fn test_parse_with_trailing_spaces() -> anyhow::Result<()> {
        let testdata = "title EXAMPLE \t TITLE  \t";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "  \t");
        assert_eq!("title EXAMPLE \t TITLE", parsed);
        assert_eq!("EXAMPLE \t TITLE", token.title());
        assert_eq!(None, token.delimiter());

        Ok(())
    }

    #[test]
    fn test_parse_case_insensitive() -> anyhow::Result<()> {
        let testdata = "Title EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(None, token.delimiter());

        let testdata = "TITLE EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(None, token.delimiter());

        let testdata = "tiTle EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(None, token.delimiter());

        Ok(())
    }

    #[test]
    fn test_parse_with_delimiter() -> anyhow::Result<()> {
        let testdata = "title : EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(Some(":"), token.delimiter());

        let testdata = "title: \tEXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(Some(":"), token.delimiter());

        let testdata = "title \t:EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(Some(":"), token.delimiter());

        let testdata = "title:EXAMPLE TITLE";
        let (rest, (parsed, token)) = TitleToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!("EXAMPLE TITLE", token.title());
        assert_eq!(Some(":"), token.delimiter());

        Ok(())
    }
}
