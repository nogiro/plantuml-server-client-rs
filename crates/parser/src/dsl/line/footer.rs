use crate::FooterToken;
use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr, wr2};
use nom::character::complete::space0;
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a [`FooterToken`]. (like `"\tfooter EXAMPLE FOOTER  \n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{FooterLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = " footer EXAMPLE FOOTER\n";
/// let (rest, (raws, token)) = FooterLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, " footer EXAMPLE FOOTER\n");
/// assert_eq!(token.footer(), "EXAMPLE FOOTER");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct FooterLine {
    token: FooterToken,
    ibc: Option<InlineBlockCommentToken>,
}

impl FooterLine {
    /// Tries to parse [`FooterLine`]. (e.g. `" footer EXAMPLE FOOTER\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (token, ibc) = lwc.into();

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self { token, ibc };

        Ok((rest, (ret0, ret1)))
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

impl std::ops::Deref for FooterLine {
    type Target = FooterToken;
    fn deref(&self) -> &Self::Target {
        &self.token
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, FooterToken)> {
    map(
        (wr!(space0), wr2!(FooterToken::parse), wr!(space0)),
        |(p0, (p1, token), p2)| (vec![p0, p1, p2], token),
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_footer_line() -> anyhow::Result<()> {
        let testdata = [
            ("footer FOOTER\n", None),
            (" \tfooter FOOTER\n", None),
            ("footer FOOTER \t\n", None),
            (" \tfooter FOOTER \t\n", None),
            ("Footer FOOTER\n", None),
            ("FOOTER FOOTER\n", None),
            ("fooTer FOOTER\n", None),
            (" /' comment '/ footer FOOTER\n", Some(" /' comment '/ ")),
            ("footer FOOTER  /' comment '/  \n", Some("/' comment '/  ")),
        ];

        for (testdata, expected_comment) in testdata.into_iter() {
            println!("try: testdata = {testdata:?}");

            let (rest, (parsed, FooterLine { token, ibc })) = FooterLine::parse(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(token.footer(), "FOOTER");
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);
        }

        let testdata = "footer FOOTER /' c0 '/ /' c1 '/   \n";
        let expected_comment = Some("/' c1 '/   ");

        let (rest, (parsed, FooterLine { token, ibc })) = FooterLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!(token.footer(), "FOOTER /' c0 '/");
        assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);

        Ok(())
    }
}
