#!/usr/bin/env bash

set -u

SCRIPT_DIR="$(cd "$(dirname "${0}")" || exit 1; pwd)"
ROOT_DIR="${SCRIPT_DIR}/../../"

# shellcheck source=utility/shared/shared.sh
source "${SCRIPT_DIR}/../shared/shared.sh"

if [ "$(current_versions "${ROOT_DIR}" | wc -l)" -gt 1 ]; then
  echo unmatch \"version\" in Cargo.toml >&2
  exit 1
fi

TAGNAME="v$(current_versions "${ROOT_DIR}")"

if [ "$(git ls-remote --tags 2> /dev/null | grep -F "${TAGNAME}" | wc -c)" -ne 0 ]; then
  echo "tag (\"${TAGNAME}\") already existed" >&2
  exit 0
fi

git tag "${TAGNAME}"
git push --tags
