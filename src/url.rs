use crate::Format;

/// The default URL prefix
pub const URL_PREFIX: &str = "http://www.plantuml.com/plantuml/";

/// A builder to build URLs for PlantUML server
pub struct UrlBuilder {
    prefix: String,
    format: Format,
}

impl UrlBuilder {
    /// Creates a [`UrlBuilder`] with default values.
    pub fn new() -> Self {
        Self {
            prefix: URL_PREFIX.into(),
            format: Format::Svg,
        }
    }

    /// Creates a URL string
    ///
    /// * `data` - An encoded PlantUML content for GET request
    pub fn build(self, data: String) -> String {
        let format: &str = (&self.format).into();
        format!("{}{format}/{data}", self.prefix)
    }

    /// Replaces URL prefix
    pub fn prefix(self, prefix: String) -> Self {
        Self { prefix, ..self }
    }

    /// Replaces output format
    pub fn format(self, format: Format) -> Self {
        Self { format, ..self }
    }
}

impl Default for UrlBuilder {
    fn default() -> Self {
        Self::new()
    }
}
