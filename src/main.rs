use plantuml_server_client_rs as pscr;

use anyhow::{Context as _, Result};
use clap::Parser;
use pscr::Args;
use std::env::current_dir;
use std::sync::Arc;

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    pscr::init_logger(args.verbose).context("init_logger(): failed")?;
    tracing::info!("args = {args:?}");

    let mode = args.mode(current_dir()?);
    let output = args.output_arc();
    let config = args.config().await?;

    let client = Arc::new(pscr::Client::new(config));
    mode.run(client, output).await?;

    Ok(())
}
