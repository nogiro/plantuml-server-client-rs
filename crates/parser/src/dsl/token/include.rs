use crate::IncludeSpecifierToken;
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::Parser;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::space1;

/// A token sequence with [`IncludeSpecifierToken`] that is around the include keyword. (like `"!include foo.puml"` or `"!include bar.iuml!buz"` , `"!include_many foo.puml"` or `"!include_many bar.iuml!buz"`.)
///
/// * `!include [include specifier]`
/// * `!include_many [include specifier]`
/// * `!include_once [include specifier]`
///
/// The above `include specifier` is parsed by [`IncludeSpecifierToken`].
///
/// # Examples
///
/// ```
/// use plantuml_parser::{IncludeKind, IncludeToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "!include foo.puml rest";
/// let (rest, (raws, token)) = IncludeToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "!include foo.puml");
/// assert_eq!(token.filepath(), "foo.puml");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), None);
/// assert_eq!(token.kind(), &IncludeKind::Include);
///
/// let input = "!include_many bar.iuml!1 rest";
/// let (rest, (raws, token)) = IncludeToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "!include_many bar.iuml!1");
/// assert_eq!(token.filepath(), "bar.iuml");
/// assert_eq!(token.index(), Some(1));
/// assert_eq!(token.id(), Some("1"));
/// assert_eq!(token.kind(), &IncludeKind::IncludeMany);
///
/// let input = "!include_once baz.txt!qux rest";
/// let (rest, (raws, token)) = IncludeToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "!include_once baz.txt!qux");
/// assert_eq!(token.filepath(), "baz.txt");
/// assert_eq!(token.index(), None);
/// assert_eq!(token.id(), Some("qux"));
/// assert_eq!(token.kind(), &IncludeKind::IncludeOnce);
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct IncludeToken {
    specifier: IncludeSpecifierToken,
    kind: IncludeKind,
}

/// A kind of include keywords. `!include` | `!include_many` | `!include_once`
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum IncludeKind {
    Include,
    IncludeMany,
    IncludeOnce,
}

impl IncludeToken {
    /// Tries to parse [`IncludeToken`]. (e.g. `"!include foo.puml"`, `"!include bar.iuml!buz"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (include, spaces, (specifier_raws, specifier))) = (
            alt((
                wr!(tag("!include_many")),
                wr!(tag("!include_once")),
                wr!(tag("!include")),
            )),
            wr!(space1),
            wr2!(IncludeSpecifierToken::parse),
        )
            .parse(input)?;

        let kind = match include.as_str() {
            "!include" => IncludeKind::Include,
            "!include_many" => IncludeKind::IncludeMany,
            "!include_once" => {
                tracing::warn!("Multiple include errors with `include_once` are not supported.");
                IncludeKind::IncludeOnce
            }

            _ => unreachable!("IncludeToken"),
        };

        let ret0 = ParseContainer::from(vec![include, spaces, specifier_raws]);
        let ret1 = Self { specifier, kind };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns the [`IncludeKind`] of include keywords.
    pub fn kind(&self) -> &IncludeKind {
        &self.kind
    }
}

impl std::ops::Deref for IncludeToken {
    type Target = IncludeSpecifierToken;
    fn deref(&self) -> &Self::Target {
        &self.specifier
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "!include foo.puml  ";
        let (rest, (parsed, include)) = IncludeToken::parse(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, "!include foo.puml");
        assert_eq!(include.filepath(), "foo.puml");
        assert!(include.id().is_none());
        assert_eq!(include.kind(), &IncludeKind::Include);

        let testdata = "!include foo.puml!bar   ";
        let (rest, (parsed, include)) = IncludeToken::parse(testdata.into())?;
        assert_eq!(rest, "   ");
        assert_eq!(parsed, "!include foo.puml!bar");
        assert_eq!(include.filepath(), "foo.puml");
        assert_eq!(include.id(), Some("bar"));
        assert_eq!(include.kind(), &IncludeKind::Include);

        let testdata = "!include_many foo.puml!bar  ";
        let (rest, (parsed, include)) = IncludeToken::parse(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, "!include_many foo.puml!bar");
        assert_eq!(include.filepath(), "foo.puml");
        assert_eq!(include.id(), Some("bar"));
        assert_eq!(include.kind(), &IncludeKind::IncludeMany);

        let testdata = "!include_once foo.puml!bar  ";
        let (rest, (parsed, include)) = IncludeToken::parse(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, "!include_once foo.puml!bar");
        assert_eq!(include.filepath(), "foo.puml");
        assert_eq!(include.id(), Some("bar"));
        assert_eq!(include.kind(), &IncludeKind::IncludeOnce);

        Ok(())
    }
}
