pub mod args;
pub mod file;

use crate::{Args, FileConfig, Format, MetadataVersion, Method, URL_PREFIX};
use anyhow::Context as _;
use std::path::PathBuf;

/// A configuration of `plantuml-server-client`.
#[derive(Debug)]
pub struct Config {
    /// The HTTP Request Method ([`Method::Get`] or [`Method::Post`])
    pub method: Method,

    /// The URL prefix of the PlantUML Server (e.g. `http://localhost:8080/`)
    pub url_prefix: String,

    /// The output format ([`Format::Svg`] or [`Format::Png`] or [`Format::Ascii`] (`txt`))
    pub format: Format,

    /// The whether to output a `combined` diagram.
    pub combined: bool,

    /// The output path for metadata
    pub metadata: Option<PathBuf>,

    /// The version of metadata
    pub metadata_version: MetadataVersion,
}

trait ConfigTrait {
    /// Returns HTTP Request Method ([`Method::Get`] or [`Method::Post`])
    fn method(&self) -> Option<Method>;

    /// Returns URL prefix of the PlantUML Server (e.g. `http://localhost:8080/`)
    fn url_prefix(&self) -> Option<String>;

    /// Returns output format ([`Format::Svg`] or [`Format::Png`] or [`Format::Ascii`] (`txt`))
    fn format(&self) -> Option<Format>;

    /// Returns whether to output a `combined` diagram.
    fn combined(&self) -> Option<bool>;

    /// Returns the version of metadata.
    fn metadata_version(&self) -> Option<MetadataVersion>;
}

/// Treats priority (default configuration < file configuration < args configuration)
macro_rules! overwrite_item {
    ($config:expr_2021, $args:expr_2021, $from_file:expr_2021, $member:ident) => {
        let x = $args
            .$member()
            .or_else(|| $from_file.as_ref().and_then(|x| x.$member()));
        if let Some(x) = x {
            $config.$member = x;
        }
    };
}

impl Config {
    /// Creates a [`Config`] from default value -> file -> args with to read file.
    pub async fn load(args: &Args) -> anyhow::Result<Self> {
        let from_file = match &args.config {
            Some(filepath) => {
                let config = FileConfig::read_from_file(filepath)
                    .await
                    .with_context(|| {
                        format!("failed to read `{filepath:?}` that specified by args.")
                    })?;
                Some(config)
            }
            None => FileConfig::read_from_default_path().await,
        };

        Self::merge_from(args, from_file)
    }

    /// Creates a [`Config`] from default value -> file -> args
    ///
    /// * `args` - A configuration from command-line arguments.
    /// * `from_file` - A configuration from the file.
    fn merge_from(args: &Args, from_file: Option<FileConfig>) -> anyhow::Result<Self> {
        let mut config = Self::default();

        overwrite_item!(config, args, from_file, method);
        overwrite_item!(config, args, from_file, url_prefix);
        overwrite_item!(config, args, from_file, format);
        overwrite_item!(config, args, from_file, combined);
        config.metadata = args.metadata.clone();
        overwrite_item!(config, args, from_file, metadata_version);

        if config.metadata.is_some() && config.metadata_version.is_v1() {
            tracing::warn!(
                "The `v1` metadata format is deprecated. Please specify `--metadata-version v2` and change post process."
            );
        }

        Ok(config)
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            method: Method::Get,
            url_prefix: URL_PREFIX.into(),
            format: Format::Svg,
            combined: false,
            metadata: None,
            metadata_version: MetadataVersion::V1,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use clap::Parser;

    #[test]
    fn test_combined() -> anyhow::Result<()> {
        // args -> false, file -> false
        let mut args = Args::parse();
        args.combined = false;
        let from_file: FileConfig = toml::from_str("[pscr]\ncombined = false")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(!config.combined);

        // args -> false, file -> true
        let mut args = Args::parse();
        args.combined = false;
        let from_file: FileConfig = toml::from_str("[pscr]\ncombined = true")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(config.combined);

        // args -> false, file -> None
        let mut args = Args::parse();
        args.combined = false;
        let from_file: FileConfig = toml::from_str("[pscr]")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(!config.combined);

        // args -> true, file -> false
        let mut args = Args::parse();
        args.combined = true;
        let from_file: FileConfig = toml::from_str("[pscr]\ncombined = false")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(config.combined);

        // args -> true, file -> true
        let mut args = Args::parse();
        args.combined = true;
        let from_file: FileConfig = toml::from_str("[pscr]\ncombined = true")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(config.combined);

        // args -> true, file -> None
        let mut args = Args::parse();
        args.combined = true;
        let from_file: FileConfig = toml::from_str("[pscr]")?;
        let config = Config::merge_from(&args, Some(from_file))?;
        assert!(config.combined);

        Ok(())
    }
}
