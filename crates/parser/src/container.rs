use nom::Input;
use std::sync::Arc;

/// An intermediate representation to parse.
///
/// The [`ParseContainer`] has a shared pointer of the original [`String`] and the pair of sub-string position (`begin` and `end`),
#[derive(Clone, Debug)]
pub struct ParseContainer {
    raw: Arc<String>,
    begin: usize,
    end: usize,
}

/// The error type to fail to parse.
#[derive(thiserror::Error, Debug)]
#[error("parse error: {0}")]
pub struct ParseError(#[from] pub(crate) nom::Err<nom::error::Error<ParseContainer>>);

/// The `Result` type for parsing process.
pub type ParseResult<T> = Result<(ParseContainer, (ParseContainer, T)), ParseError>;

impl ParseContainer {
    /// Creates a new [`ParseContainer`] from the original string.
    /// The inner positions are held in the entirety of the original string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// # use std::sync::Arc;
    /// #
    /// # fn main() {
    /// let _ = ParseContainer::new(Arc::new("foo".to_string()));
    /// # }
    /// ```
    pub fn new(raw: Arc<String>) -> Self {
        let end = raw.len();
        Self { raw, begin: 0, end }
    }

    /// Returns `true` if `self` has a length of zero
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// #
    /// # fn main() {
    /// let container = ParseContainer::from("");
    /// assert!(container.is_empty());
    /// # }
    /// ```
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// # use std::sync::Arc;
    /// #
    /// # fn main() {
    /// let container = ParseContainer::from("foo");
    /// let (_rest, parsed) = container.split_at(0);
    /// assert!(parsed.is_empty());
    /// let (rest, _parsed) = container.split_at(container.len());
    /// assert!(rest.is_empty());
    /// # }
    /// ```
    pub fn is_empty(&self) -> bool {
        self.as_str().is_empty()
    }

    /// Returns the length of `self`
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// #
    /// # fn main() {
    /// let container = ParseContainer::from("foo");
    /// assert_eq!(container.len(), 3);
    /// # }
    /// ```
    pub fn len(&self) -> usize {
        self.as_str().len()
    }

    /// Returns a string slice of the sub-string
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// #
    /// # fn main() {
    /// let container = ParseContainer::from("foo");
    /// let s: &str = container.as_str(); // Returns `&str`
    /// assert_eq!(s, "foo");
    /// # }
    /// ```
    pub fn as_str(&self) -> &str {
        &self.raw[self.begin..self.end]
    }

    /// Splits into two sub-strings by `mid`
    ///
    /// * `mid` - A position to split.
    ///
    /// # Panics
    ///
    /// Panics if `mid` is past the end of the sub-string.
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_parser::ParseContainer;
    /// #
    /// # fn main() {
    /// let container = ParseContainer::from("Hello, World!");
    /// let (rest, parsed) = container.split_at(7);
    /// assert_eq!(rest, "World!");
    /// assert_eq!(parsed, "Hello, ");
    ///
    /// let (rest, parsed) = container.split_at(0);
    /// assert_eq!(rest, "Hello, World!");
    /// assert_eq!(parsed, "");
    ///
    /// let (rest, parsed) = container.split_at(container.len());
    /// assert_eq!(rest, "");
    /// assert_eq!(parsed, "Hello, World!");
    /// # }
    /// ```
    pub fn split_at(&self, mid: usize) -> (Self, Self) {
        #[rustfmt::skip]
        assert!(self.len() >= mid, "the `mid` is past the end: len = {}, mid = {}", self.len(), mid);

        let rest = Self {
            begin: self.begin + mid,
            ..self.clone()
        };

        let parsed = Self {
            end: self.begin + mid,
            ..self.clone()
        };

        (rest, parsed)
    }
}

impl PartialEq for ParseContainer {
    fn eq(&self, rhs: &ParseContainer) -> bool {
        self.as_str() == rhs.as_str()
    }
}

impl PartialEq<&str> for ParseContainer {
    fn eq(&self, rhs: &&str) -> bool {
        self.as_str() == *rhs
    }
}

impl PartialEq<ParseContainer> for &str {
    fn eq(&self, rhs: &ParseContainer) -> bool {
        *self == rhs.as_str()
    }
}

impl std::borrow::Borrow<str> for ParseContainer {
    fn borrow(&self) -> &str {
        self.as_str()
    }
}

impl std::fmt::Display for ParseContainer {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        self.as_str().fmt(formatter)
    }
}

impl From<&str> for ParseContainer {
    fn from(s: &str) -> Self {
        Self::new(Arc::new(s.into()))
    }
}

impl From<String> for ParseContainer {
    fn from(s: String) -> Self {
        Self::new(Arc::new(s))
    }
}

impl From<Vec<ParseContainer>> for ParseContainer {
    fn from(x: Vec<ParseContainer>) -> Self {
        x.as_slice().into()
    }
}

impl From<&[ParseContainer]> for ParseContainer {
    // TODO: move to `ParseContainer::from_vec() -> Result` because to check each element are neighboring
    fn from(x: &[ParseContainer]) -> Self {
        if x.is_empty() {
            return Self::new(Arc::new("".into()));
        }

        let x0 = x.first().unwrap();
        let len = x.iter().fold(0, |stack, item| stack + item.len());

        Self {
            end: x0.begin + len,
            ..x0.clone()
        }
    }
}

impl<const N: usize> From<&[ParseContainer; N]> for ParseContainer {
    fn from(x: &[ParseContainer; N]) -> Self {
        x.as_slice().into()
    }
}

impl Input for ParseContainer {
    type Item = char;
    type Iter = std::vec::IntoIter<Self::Item>;
    type IterIndices = std::vec::IntoIter<(usize, Self::Item)>;

    fn input_len(&self) -> usize {
        self.len()
    }

    fn take(&self, index: usize) -> Self {
        let end = self.begin + index;
        assert!(end <= self.end);
        Self {
            end,
            ..self.clone()
        }
    }

    fn take_from(&self, index: usize) -> Self {
        let begin = self.begin + index;
        assert!(begin <= self.end);
        Self {
            begin,
            ..self.clone()
        }
    }

    fn take_split(&self, index: usize) -> (Self, Self) {
        self.split_at(index)
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
    where
        P: Fn(Self::Item) -> bool,
    {
        self.as_str().find(predicate)
    }

    fn iter_elements(&self) -> <Self as Input>::Iter {
        self.as_str().chars().collect::<Vec<_>>().into_iter()
    }

    fn iter_indices(&self) -> <Self as Input>::IterIndices {
        self.as_str().char_indices().collect::<Vec<_>>().into_iter()
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        self.as_str().slice_index(count)
    }
}

/// wrap nom's parser
macro_rules! wr {
    ($func:expr) => {
        |input: ParseContainer| {
            use std::sync::Arc;

            let input_str = input.as_str();
            let (_rest, parsed) =
                $func(input_str).map_err(|e: nom::Err<nom::error::Error<&str>>| {
                    e.map_input(|x| ParseContainer::new(Arc::new(x.to_owned())))
                })?;
            let (rest, parsed) = input.split_at(parsed.len());
            Ok((rest, parsed))
        }
    };
}
pub(crate) use wr;

/// wrap crate's result ([`ParseResult`])
macro_rules! wr2 {
    ($func:expr) => {
        |input: ParseContainer| $func(input).map_err(|e: $crate::ParseError| e.0)
    };
}
pub(crate) use wr2;

impl ParseError {
    /// Returns [`ParseError`] contains [`nom::error::ErrorKind::Fail`].
    pub fn fail(input: ParseContainer) -> Self {
        Self(nom::Err::Error(nom::error::Error {
            input,
            code: nom::error::ErrorKind::Fail,
        }))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let (rest0, parsed0) = ParseContainer::from("foobar").split_at(2);
        println!("rest0 = {rest0}");
        println!("parsed0 = {parsed0}");

        assert_eq!(rest0, "obar");
        assert_eq!(rest0.is_empty(), false);
        assert_eq!(rest0.len(), 4);
        assert_eq!(rest0.as_str(), "obar");
        assert_eq!(parsed0, "fo");
        assert_eq!(parsed0.is_empty(), false);
        assert_eq!(parsed0.len(), 2);
        assert_eq!(parsed0.as_str(), "fo");

        let (rest1, parsed1) = rest0.split_at(3);
        println!("rest1 = {rest1}");
        println!("parsed1 = {parsed1}");

        assert_eq!(rest1, "r");
        assert_eq!(rest1.is_empty(), false);
        assert_eq!(rest1.len(), 1);
        assert_eq!(rest1.as_str(), "r");
        assert_eq!(parsed1, "oba");
        assert_eq!(parsed1.is_empty(), false);
        assert_eq!(parsed1.len(), 3);
        assert_eq!(parsed1.as_str(), "oba");

        let (rest2, parsed2) = parsed1.split_at(3);
        println!("rest2 = {rest2}");
        println!("parsed2 = {parsed2}");

        assert_eq!(rest2, "");
        assert_eq!(rest2.is_empty(), true);
        assert_eq!(rest2.len(), 0);
        assert_eq!(rest2.as_str(), "");
        assert_eq!(parsed2, "oba");
        assert_eq!(parsed2.is_empty(), false);
        assert_eq!(parsed2.len(), 3);
        assert_eq!(parsed2.as_str(), "oba");

        // It has the same `Arc<String>` after repeated division
        assert!(Arc::ptr_eq(&rest0.raw, &rest1.raw));
        assert!(Arc::ptr_eq(&rest0.raw, &rest2.raw));
        assert!(Arc::ptr_eq(&rest0.raw, &parsed0.raw));
        assert!(Arc::ptr_eq(&rest0.raw, &parsed1.raw));
        assert!(Arc::ptr_eq(&rest0.raw, &parsed2.raw));
    }

    #[test]
    fn test_split_at() {
        let (rest, parsed) = ParseContainer::from("foobar").split_at(3);
        println!("rest = {rest}");
        println!("parsed = {parsed}");

        assert_eq!(rest, "bar");
        assert_eq!(parsed, "foo");
    }

    #[test]
    #[should_panic(expected = "the `mid` is past the end: len = 0, mid = 1")]
    fn test_panic_split_at_0() {
        ParseContainer::from("").split_at(1);
    }

    #[test]
    #[should_panic(expected = "the `mid` is past the end: len = 1, mid = 2")]
    fn test_panic_split_at_1() {
        let (_rest, parsed) = ParseContainer::from("test").split_at(1);
        println!("parsed = {parsed}");
        parsed.split_at(1); // not panic
        parsed.split_at(2);
    }

    #[test]
    #[should_panic(expected = "the `mid` is past the end: len = 3, mid = 4")]
    fn test_panic_split_at_2() {
        let (rest, _parsed) = ParseContainer::from("test").split_at(1);
        println!("rest = {rest}");
        rest.split_at(3); // not panic
        rest.split_at(4);
    }
}
