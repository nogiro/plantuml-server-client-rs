use crate::DiagramIdToken;
use crate::{ParseContainer, ParseResult, wr, wr2};
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, space1};
use nom::{IResult, Parser};

/// A token sequence that is around the start keyword (`"@startXYZ"`). (like `"@startuml"` or `"@startuml ID"`, `"@startuml(id=ID)"`.)
///
/// * `@startuml`
/// * `@startuml ID`
/// * `@startuml(id=ID)`
///
/// The above `ID` parts is parsed by [`DiagramIdToken`].
///
/// # Examples
///
/// ```
/// use plantuml_parser::{StartDiagramToken, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "@startuml   "; /// the last "   " is rest parts
/// let (rest, (raws, token)) = StartDiagramToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "   ");
/// assert_eq!(combined_raw, "@startuml");
/// assert_eq!(token.diagram_kind(), "uml");
/// assert_eq!(token.id(), None);
///
/// let input = "@startXYZ diagram_0  rest";
/// let (rest, (raws, token)) = StartDiagramToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "  rest");
/// assert_eq!(combined_raw, "@startXYZ diagram_0");
/// assert_eq!(token.diagram_kind(), "XYZ");
/// assert_eq!(token.id(), Some("diagram_0"));
///
/// let input = "@startsalt(id=diagram_1) rest";
/// let (rest, (raws, token)) = StartDiagramToken::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, " rest");
/// assert_eq!(combined_raw, "@startsalt(id=diagram_1)");
/// assert_eq!(token.diagram_kind(), "salt");
/// assert_eq!(token.id(), Some("diagram_1"));
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct StartDiagramToken {
    diagram_kind: ParseContainer,
    id: Option<DiagramIdToken>,
}

impl StartDiagramToken {
    /// Tries to parse [`StartDiagramToken`]. (e.g. `"@startuml"`, `"@startuml ID"`, `"@startuml(id=ID)"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (start, diagram_kind, (id_raw, id))) =
            (wr!(tag("@start")), wr!(alpha1), parse_id_part).parse(input)?;

        let ret0 = ParseContainer::from(vec![start, diagram_kind.clone(), id_raw]);
        let ret1 = Self { diagram_kind, id };

        Ok((rest, (ret0, ret1)))
    }

    /// Returns the kind of diagrams. (e.g. `XYZ` in `@startXYZ`.)
    pub fn diagram_kind(&self) -> &str {
        self.diagram_kind.as_str()
    }

    /// Returns the ID of the diagram.
    pub fn id(&self) -> Option<&str> {
        self.id.as_ref().map(|x| x.id())
    }
}

type ParsedResult = IResult<ParseContainer, (ParseContainer, Option<DiagramIdToken>)>;

/// parse like `" ID"` or `"(id=ID)"`, `""`
fn parse_id_part(input: ParseContainer) -> ParsedResult {
    alt((parse_spaced_id, parse_parened_id, parse_none)).parse(input)
}

/// parse like `"@startuml ID"`
fn parse_spaced_id(input: ParseContainer) -> ParsedResult {
    let (input, (space, (id_raw, id))) = (wr!(space1), wr2!(DiagramIdToken::parse)).parse(input)?;

    let parsed = Vec::from([space, id_raw]);
    let parsed = ParseContainer::from(parsed);
    Ok((input, (parsed, Some(id))))
}

/// parse like `"@startuml(id=ID)"`
fn parse_parened_id(input: ParseContainer) -> ParsedResult {
    let (input, (open, (id_raw, id), close)) =
        (wr!(tag("(id=")), wr2!(DiagramIdToken::parse), wr!(tag(")"))).parse(input)?;

    let parsed = Vec::from([open, id_raw, close]);
    let parsed = ParseContainer::from(parsed);
    Ok((input, (parsed, Some(id))))
}

/// parse like `"@startuml"`
fn parse_none(input: ParseContainer) -> ParsedResult {
    let (input, parsed) = input.split_at(0);
    Ok((input, (parsed, None)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = "@startuml";
        let (rest, (raws, StartDiagramToken { diagram_kind, id })) =
            StartDiagramToken::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(raws, testdata);
        assert_eq!(diagram_kind, "uml");
        assert!(id.is_none());

        let testdata = "@startjson \t\n";
        let (rest, (parsed, StartDiagramToken { diagram_kind, id })) =
            StartDiagramToken::parse(testdata.into())?;
        assert_eq!(rest, " \t\n");
        assert_eq!(parsed, "@startjson");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(diagram_kind, "json");
        assert!(id.is_none());

        let testdata = "@startuml \t id aaa\n";
        let (rest, (parsed, StartDiagramToken { diagram_kind, id })) =
            StartDiagramToken::parse(testdata.into())?;
        assert_eq!(rest, " aaa\n");
        assert_eq!(parsed, "@startuml \t id");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(diagram_kind, "uml");
        assert_eq!(id.as_ref().map(|x| x.id()), Some("id"));

        let testdata = "@startuml(id=foo_BAR) bbb\n";
        let (rest, (parsed, StartDiagramToken { diagram_kind, id })) =
            StartDiagramToken::parse(testdata.into())?;
        assert_eq!(rest, " bbb\n");
        assert_eq!(parsed, "@startuml(id=foo_BAR)");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(diagram_kind, "uml");
        assert_eq!(id.as_ref().map(|x| x.id()), Some("foo_BAR"));

        Ok(())
    }

    #[test]
    fn test_parse_id_part() -> anyhow::Result<()> {
        let testdata = "";
        let (rest, (parsed, id)) = parse_id_part(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed, testdata);
        assert!(id.is_none());

        let testdata = " aaa  ";
        let (rest, (parsed, id)) = parse_id_part(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, " aaa");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(id.as_ref().map(|x| x.id()), Some("aaa"));

        let testdata = "(id=FOO_bar) \txxx";
        let (rest, (parsed, id)) = parse_id_part(testdata.into())?;
        assert_eq!(rest, " \txxx");
        assert_eq!(parsed, "(id=FOO_bar)");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(id.as_ref().map(|x| x.id()), Some("FOO_bar"));

        Ok(())
    }

    #[test]
    fn test_parse_spaced_id() -> anyhow::Result<()> {
        let testdata = " aaa  ";
        let (rest, (parsed, id)) = parse_spaced_id(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, " aaa");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(id.as_ref().map(|x| x.id()), Some("aaa"));

        Ok(())
    }

    #[test]
    fn test_parse_parened_id() -> anyhow::Result<()> {
        let testdata = "(id=FOO_bar) \txxx";
        let (rest, (parsed, id)) = parse_parened_id(testdata.into())?;
        assert_eq!(rest, " \txxx");
        assert_eq!(parsed, "(id=FOO_bar)");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert_eq!(id.as_ref().map(|x| x.id()), Some("FOO_bar"));

        Ok(())
    }

    #[test]
    fn test_parse_none() -> anyhow::Result<()> {
        let testdata = "  ";
        let (rest, (parsed, id)) = parse_none(testdata.into())?;
        assert_eq!(rest, "  ");
        assert_eq!(parsed, "");
        assert_eq!(format!("{parsed}{rest}"), testdata);
        assert!(id.is_none());

        Ok(())
    }
}
