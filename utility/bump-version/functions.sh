#!/usr/bin/env bash

function edit_versions() {
  set -u

  # inputs
  ROOT_DIR="${1}"
  NEXT_VERSION="${2}"

  # generated
  TMPFILE="$(mktemp "$(realpath --canonicalize-missing "${ROOT_DIR}/outputs/XXXXXX.Cargo.toml")")" \
    || return 1
  TARGET_FILE="${ROOT_DIR}/Cargo.toml"

  # main
  sed \
    -e '/^version\>/s#"[^"]*"#"'"${NEXT_VERSION}"'"#' \
    -e '/^plantuml-parser\>/s#"[0-9.]*"#"'"${NEXT_VERSION}"'"#' \
    "${TARGET_FILE}" \
    > "${TMPFILE}"
  mv "${TMPFILE}" "${TARGET_FILE}"
}

function semver_parts() {
  set -u

  # inputs
  INDICATOR="${1}"
  VERSION="${2}"

  # main
  MAJOR_VERSION="${VERSION%%.*}"
  WITHOUT_MAJOR="${VERSION:"$(("${#MAJOR_VERSION}" + 1))"}"

  MINOR_VERSION="${WITHOUT_MAJOR%%.*}"
  PATCH_VERSION="${WITHOUT_MAJOR:"$(("${#MINOR_VERSION}" + 1))"}"

  case "${INDICATOR}" in
    "major")
      echo "${MAJOR_VERSION}";;
    "minor")
      echo "${MINOR_VERSION}";;
    *)
      echo "${PATCH_VERSION}";;
  esac
}

function next_semver() {
  set -u

  # inputs
  INDICATOR="${1}"
  VERSION="${2}"

  # main
  MAJOR_VERSION="$(semver_parts major "${VERSION}")"
  MINOR_VERSION="$(semver_parts minor "${VERSION}")"
  PATCH_VERSION="$(semver_parts patch "${VERSION}")"

  case "${INDICATOR}" in
    "major")
      MAJOR_VERSION="$(("${MAJOR_VERSION}" + 1))"
      MINOR_VERSION='0'
      PATCH_VERSION='0'
      ;;
    "minor")
      MINOR_VERSION="$(("${MINOR_VERSION}" + 1))"
      PATCH_VERSION='0'
      ;;
    *)
      PATCH_VERSION="$(("${PATCH_VERSION}" + 1))"
      ;;
  esac

  echo "${MAJOR_VERSION}.${MINOR_VERSION}.${PATCH_VERSION}"
}
