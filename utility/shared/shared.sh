#!/usr/bin/env bash

function current_versions() {
  set -u

  # inputs
  ROOT_DIR="${1}"

  # main
  grep '^version *= *"[^"]*"$' "${ROOT_DIR}/Cargo.toml" \
    | sed 's_.*"\([^"]*\)".*_\1_'
}
