use crate::{ParseContainer, ParseError, ParseResult, wr};
use nom::Parser;
use nom::bytes::complete::tag;
use nom::character::complete::{space0, space1};

/// A token sequence between a keyword (e.g. `title`) and a comment ([`Comment`][`crate::dsl::token::shared::Comment`]).
#[derive(Clone, Debug)]
pub struct Delimiter {
    delimiter: Option<ParseContainer>,
}

impl Delimiter {
    /// Tries to parse [`Delimiter`]. (e.g. `" "`, `":"`, `" : "`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        if let Ok(parsed) = parse_dellimiter(input.clone()) {
            return Ok(parsed);
        }

        parse_only_space(input)
    }

    pub fn delimiter(&self) -> Option<&str> {
        self.delimiter.as_ref().map(|x| x.as_str())
    }
}

fn parse_only_space(input: ParseContainer) -> ParseResult<Delimiter> {
    let (rest, space) = wr!(space1)(input).map_err(|e: ParseError| e.0)?;
    Ok((rest, (space, Delimiter { delimiter: None })))
}

fn parse_dellimiter(input: ParseContainer) -> ParseResult<Delimiter> {
    let (rest, (space_0, delimiter, space_1)) =
        (wr!(space0), wr!(tag(":")), wr!(space0)).parse(input)?;

    let ret0 = ParseContainer::from(vec![space_0, delimiter.clone(), space_1]);
    let ret1 = Delimiter {
        delimiter: Some(delimiter),
    };

    Ok((rest, (ret0, ret1)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = " ";
        let (rest, (parsed, token)) = Delimiter::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!(None, token.delimiter());

        let testdata = ":";
        let (rest, (parsed, token)) = Delimiter::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!(Some(":".into()), token.delimiter());

        let testdata = " \t:";
        let (rest, (parsed, token)) = Delimiter::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!(Some(":".into()), token.delimiter());

        let testdata = ":\t ";
        let (rest, (parsed, token)) = Delimiter::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed);
        assert_eq!(Some(":".into()), token.delimiter());

        Ok(())
    }
}
