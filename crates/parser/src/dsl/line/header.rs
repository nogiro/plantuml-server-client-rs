use crate::HeaderToken;
use crate::dsl::line::LineWithComment;
use crate::{InlineBlockCommentToken, ParseContainer, ParseResult, wr, wr2};
use nom::character::complete::space0;
use nom::combinator::map;
use nom::{IResult, Parser};

/// A token sequence that is a line containing a [`HeaderToken`]. (like `"\theader EXAMPLE HEADER  \n"`.)
///
/// # Examples
///
/// ```
/// use plantuml_parser::{HeaderLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = " header EXAMPLE HEADER\n";
/// let (rest, (raws, token)) = HeaderLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, " header EXAMPLE HEADER\n");
/// assert_eq!(token.header(), "EXAMPLE HEADER");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct HeaderLine {
    token: HeaderToken,
    ibc: Option<InlineBlockCommentToken>,
}

impl HeaderLine {
    /// Tries to parse [`HeaderLine`]. (e.g. `" header EXAMPLE HEADER\n"`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (parsed, lwc)) = LineWithComment::parse(inner_parser, input)?;

        let (token, ibc) = lwc.into();

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self { token, ibc };

        Ok((rest, (ret0, ret1)))
    }

    pub fn inline_block_comment(&self) -> Option<&InlineBlockCommentToken> {
        self.ibc.as_ref()
    }
}

impl std::ops::Deref for HeaderLine {
    type Target = HeaderToken;
    fn deref(&self) -> &Self::Target {
        &self.token
    }
}

fn inner_parser(
    input: ParseContainer,
) -> IResult<ParseContainer, (Vec<ParseContainer>, HeaderToken)> {
    map(
        (wr!(space0), wr2!(HeaderToken::parse), wr!(space0)),
        |(p0, (p1, token), p2)| (vec![p0, p1, p2], token),
    )
    .parse(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_header_line() -> anyhow::Result<()> {
        let testdata = [
            ("header HEADER\n", None),
            (" \theader HEADER\n", None),
            ("header HEADER \t\n", None),
            (" \theader HEADER \t\n", None),
            ("Header HEADER\n", None),
            ("HEADER HEADER\n", None),
            ("heaDer HEADER\n", None),
            (" /' comment '/ header HEADER\n", Some(" /' comment '/ ")),
            ("header HEADER /' comment '/ \n", Some("/' comment '/ ")),
        ];

        for (testdata, expected_comment) in testdata.into_iter() {
            let (rest, (parsed, HeaderLine { token, ibc })) = HeaderLine::parse(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(testdata, parsed);
            assert_eq!(token.header(), "HEADER");
            assert_eq!(ibc.as_ref().map(|x| x.comment()), expected_comment);
        }

        Ok(())
    }
}
