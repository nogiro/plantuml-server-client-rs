use crate::{ParseContainer, ParseResult, wr};
use nom::branch::alt;
use nom::bytes::complete::{tag, take_till, take_until};
use nom::character::complete::{line_ending, space0};
use nom::combinator::{eof, map};
use nom::multi::many_till;
use nom::{IResult, Parser};

/// A token sequence that is an empty line. (like `" \t  \n"`, `"' comment\n"`, `" /' oneline block comment '/ "`.)
///
/// * `\n`
/// * `  \n`
/// * `\t  \n`
/// * `' comment\n`
/// * " /' oneline block comment '/ "
///
/// # Examples
///
/// ```
/// use plantuml_parser::{EmptyLine, ParseContainer};
///
/// # fn main() -> anyhow::Result<()> {
/// let input = "  \n";
/// let (rest, (raws, _token)) = EmptyLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "  \n");
///
/// let input = "  ' comment \n";
/// let (rest, (raws, _token)) = EmptyLine::parse(input.into())?;
/// let combined_raw: ParseContainer = raws.into();
/// assert_eq!(rest, "");
/// assert_eq!(combined_raw, "  ' comment \n");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct EmptyLine;

impl EmptyLine {
    /// Tries to parse [`EmptyLine`]. (e.g. `"   \n"`, `"   ' comment \n"`, `" /' oneline block comment '/ "`.)
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, parsed) =
            alt((parse_only_space, parse_comment, parse_oneline_block_comment)).parse(input)?;

        let ret0 = ParseContainer::from(parsed);
        let ret1 = Self;

        Ok((rest, (ret0, ret1)))
    }
}

fn parse_only_space(input: ParseContainer) -> IResult<ParseContainer, Vec<ParseContainer>> {
    let (rest, parsed) = (wr!(space0), alt((wr!(eof), wr!(line_ending)))).parse(input)?;

    let parsed = Vec::from(<[ParseContainer; 2]>::from(parsed));
    Ok((rest, parsed))
}

fn parse_comment(input: ParseContainer) -> IResult<ParseContainer, Vec<ParseContainer>> {
    let (rest, parsed) = (
        wr!(space0),
        wr!(tag("'")),
        wr!(take_till(|c| c == '\n' || c == '\r')),
        alt((wr!(eof), wr!(line_ending))),
    )
        .parse(input)?;

    let parsed = Vec::from(<[ParseContainer; 4]>::from(parsed));
    Ok((rest, parsed))
}

fn parse_oneline_block_comment(
    input: ParseContainer,
) -> IResult<ParseContainer, Vec<ParseContainer>> {
    let (rest, parsed) = (
        wr!(space0),
        wr!(tag("/'")),
        map(
            many_till(
                (wr!(take_until("'/")), wr!(tag("'/")), wr!(space0)),
                alt((wr!(eof), wr!(line_ending))),
            ),
            |(many_closes, ending)| {
                let mut flattened: Vec<_> = many_closes
                    .into_iter()
                    .flat_map(|(comment, close_tag, space0_1)| [comment, close_tag, space0_1])
                    .collect();
                flattened.push(ending);
                flattened
            },
        ),
    )
        .parse(input)?;

    let (space0_0, open_tag, flattened) = parsed;

    let mut parsed = Vec::from(<[ParseContainer; 2]>::from((space0_0, open_tag)));
    parsed.extend(flattened);

    Ok((rest, parsed))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_only_space() -> anyhow::Result<()> {
        let testdata = "\n";
        let (rest, parsed) = parse_only_space(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, ParseContainer::from(parsed).as_str());

        let testdata = "  \n";
        let (rest, parsed) = parse_only_space(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, ParseContainer::from(parsed).as_str());

        Ok(())
    }

    #[test]
    fn test_parse_comment() -> anyhow::Result<()> {
        let testdata = "' comment\n";
        let (rest, parsed) = parse_comment(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, ParseContainer::from(parsed).as_str());

        let testdata = "  ' comment \n";
        let (rest, parsed) = parse_comment(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, ParseContainer::from(parsed).as_str());

        Ok(())
    }

    #[test]
    fn test_parse_oneline_block_comment() -> anyhow::Result<()> {
        let testdata = [
            " /' oneline block comment '/ \n",
            "  /' oneline block comment '/   \n",
            "/' oneline /' block '/ comment '/\n",
            "  /' oneline '/ /' block '/ /' comment '/   \n",
        ];

        for testdata in testdata.into_iter() {
            let (rest, parsed) = parse_oneline_block_comment(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(testdata, ParseContainer::from(parsed).as_str());
        }

        let testdata = "  /' oneline '/ /' block '/ /' comment '/   \n/' line 2 '/\n";
        let (rest, parsed) = parse_oneline_block_comment(testdata.into())?;
        assert_eq!(rest, "/' line 2 '/\n");
        assert_eq!(
            &testdata[0..testdata.len() - rest.len()],
            ParseContainer::from(parsed).as_str()
        );

        let testdata = rest.as_str();
        let (rest, parsed) = parse_oneline_block_comment(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, ParseContainer::from(parsed).as_str());

        Ok(())
    }

    #[test]
    fn test_parse() -> anyhow::Result<()> {
        let testdata = [
            "\n",
            "  \n",
            "' comment\n",
            "  ' comment \n",
            "  /' oneline block comment '/   \n",
        ];

        for testdata in testdata.into_iter() {
            println!("try: testdata = {testdata:?}");
            let (rest, (parsed, _)) = EmptyLine::parse(testdata.into())?;
            assert_eq!(rest, "");
            assert_eq!(testdata, ParseContainer::from(parsed).as_str());
        }

        Ok(())
    }
}
