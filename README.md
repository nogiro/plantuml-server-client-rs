# plantuml-server-client-rs

The client of PlantUML Server.

This project aims to make a tool that helps to run PlantUML diagram generation in a CI/CD environment.
If you are generating locally with VS Code plugins, it is difficult to run them in a CI/CD environment.

## short usage

Request to [`http://www.plantuml.com/plantuml/`](http://www.plantuml.com/plantuml/).

```shell
cargo install plantuml-server-client-rs
{
  echo '@startuml'
  echo 'Bob -> Alice : hello'
  echo '@enduml'
} | plantuml-server-client > out.svg
```

## features

```
$ plantuml-server-client --help
The client of PlantUML Server.

Usage: plantuml-server-client [OPTIONS] [INPUT_FILES]...

Arguments:
  [INPUT_FILES]...  input file paths. if not specified, use stdin

Options:
  -i, --input <INPUT>
          (deprecated) input file path. if not specified, use stdin
  -o, --output <OUTPUT>
          output file prefix. if both of `[INPUT_FILES]` and `--output` are not specified, use stdout
  -c, --config <CONFIG>
          config file path. if `--config` option is specified and the configuration file fails to load, this program will exit with an error for security reasons
  -m, --method <METHOD>
          HTTP Method: `"get"` | `"post"`
  -u, --url-prefix <URL_PREFIX>
          Server's URL Prefix. default: `"http://www.plantuml.com/plantuml/"`. example: `"http://localhost:8080/"`
  -f, --format <FORMAT>
          output format: `"svg"` | `"png"` | `"txt"`
  -C, --combined
          output pre-proccessed data has `.puml` extension
      --metadata <METADATA>
          output path for metadata
  -W, --watch
          watch over input files
  -T, --watch-throttle <WATCH_THROTTLE>
          watch throttle msec [default: 50]
  -v, --verbose...
          increase log level: `-v` -> info, `-vv` -> debug. if the `PSCR_LOG` environment variable is specified, this `-v` option will ignore. related: [`init_logger`][`crate::init_logger`]
      --metadata-version <METADATA_VERSION>
          The version of metadata [default: v1]
  -h, --help
          Print help
  -V, --version
          Print version
```

### configuration from file

You can omit CLI option by configuration file (`.pscr.conf`).

```shell
cp sample.pscr.conf .pscr.conf
vim .pscr.conf
```

priority: `[specified by "--config" option]` -> `.pscr.conf` -> `.config/.pscr.conf` -> `${HOME}/.pscr.conf` -> `${HOME}/.config/.pscr.conf`

### multiple diagrams in one file

Generates multiple diagrams if multiple diagram definitions are included in a single specified file.

If `a.puml` is below,

```
@startuml a1
title a1
a -> b
@enduml

@startuml a2
title a2
a -> b
a <- b
@enduml
```

you execute the following command.

```sh
plantuml-server-client --output outputs a.puml
```

Finally, you will get `outputs/a/a1.svg` and `outputs/a/a2.svg`.

#### no output prefix

If you execute the following command (`--output` is not specified),

```sh
plantuml-server-client a.puml
```

you get `a/a1.svg` and `a/a2.svg`. (`[source file basename]/[diagram ID].[extension]`)

### `!include` pre-process

The diagram is generated after pre-processing.

If `a.puml` is below

```
@startuml a0
!include b.iuml!b2
!include b.iuml!1
!include b.iuml
@enduml
```

and `b.iuml` is below,

```
@startuml b0
Bob -> Bravo
@enduml

@startuml b1
Bob <- Bravo
@enduml

@startuml b2
Alice -> Alpha
@enduml
```

you execute the following command.

```sh
plantuml-server-client--output outputs a.puml
```

Finally, you will get `outputs/a/1.svg` that generated from the below PlantUML source text.

```
@startuml a0
Alice -> Alpha
Bob <- Bravo
Bob -> Bravo
@enduml
```

`!include_many` and `!include_once` are also supported, but multiple include with `!include_once` is not supported (only warning).

#### write combined file

With the `--combined` optional argument, `plantuml-server-client` will output **combined** diagrams with the `.puml` extension.
A **combined** diagram is a diagram that has been pre-processed.

### write metadata

With the `--metadata` optional argument, `plantuml-server-client` will output the metadata during generation.
The output path is specified by the `--metadata` optional argument.
The metadata is in JSON format and contains information gathered for the include process, the title in the diagram, and so on.

## TODO list

- `!includesub` preprocess
- `!$A=B` preprocess (around `include`)
