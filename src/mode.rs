mod no_watch;
mod watch;

use crate::mode::no_watch::no_watch;
use crate::mode::watch::WatchInner;
use crate::{Client, Locate};
use anyhow::{Context as _, Result};
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;

/// The mode of execution.
pub enum Mode {
    /// Takes the input data from standard input.
    Stdin,

    /// Takes the input data from files.
    NoWatch(Vec<PathBuf>),

    /// Watches over the files, and requests when the file changes.
    Watch(WatchInner),
}

impl Mode {
    /// Executes processing according to the variant of [`Mode`].
    pub async fn run(self, client: Arc<Client>, output: Arc<Locate>) -> Result<()> {
        match self {
            Mode::Stdin => {
                client
                    .request(None.into(), &output)
                    .await
                    .context("failed to request from stdin")?;
            }

            Mode::NoWatch(input_files) => {
                no_watch(client, input_files, output)
                    .await
                    .context("failed to request from files")?;
            }

            Mode::Watch(mode) => {
                mode.run(client, output).await.context("failed to watch")?;
            }
        }

        Ok(())
    }

    pub fn new_watch(current_dir: PathBuf, input_files: Vec<PathBuf>, throttle: Duration) -> Self {
        Self::Watch(WatchInner::new(current_dir, input_files, throttle))
    }
}
