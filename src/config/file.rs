use crate::config::ConfigTrait;
use crate::{Format, MetadataVersion, Method};
use anyhow::Context as _;
use dirs::home_dir;
use serde::Deserialize;
use std::path::PathBuf;
use tokio::fs::read_to_string;

/// The default configuration file path list
const PATHS: [&str; 2] = [".pscr.conf", ".config/.pscr.conf"];

/// A representation of configuration from file
///
/// # Examples:
///
/// ```toml
#[doc = include_str!("../../tests/assets/config/.pscr.conf")]
/// ```
#[derive(Deserialize, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct FileConfig {
    pscr: Config,
}

#[derive(Deserialize, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
struct Config {
    #[serde(default, deserialize_with = "de::deserialize_option_from_str")]
    method: Option<Method>,

    #[serde(default)]
    url_prefix: Option<String>,

    #[serde(default, deserialize_with = "de::deserialize_option_from_str")]
    format: Option<Format>,

    #[serde(default)]
    combined: Option<bool>,

    #[serde(default)]
    metadata_version: Option<MetadataVersion>,
}

impl FileConfig {
    /// Reads [`FileConfig`] from specified path.
    ///
    /// * `filepath` - A configuration file path
    ///
    /// # Examples
    ///
    /// ```
    /// # use plantuml_server_client_rs as pscr;
    /// #
    /// # use std::path::PathBuf;
    /// # use pscr::FileConfig;
    /// # #[tokio::main]
    /// async fn main() -> anyhow::Result<()> {
    ///     let filepath: PathBuf = "./tests/assets/config/.pscr.conf".into();
    ///     let _config = FileConfig::read_from_file(&filepath).await?;
    ///     Ok(())
    /// }
    /// ```
    pub async fn read_from_file(filepath: &PathBuf) -> anyhow::Result<Self> {
        let data = read_to_string(filepath)
            .await
            .map_err(|e| {
                tracing::warn!("failed to read '{filepath:?}': {e:?}");
                e
            })
            .with_context(|| format!("failed to read '{filepath:?}'"))?;

        toml::from_str(&data)
            .map_err(|e| {
                tracing::warn!("failed to parse '{filepath:?}': {e:?}");
                e
            })
            .with_context(|| format!("failed to parse '{filepath:?}'"))
    }

    /// Reads the configuration from file in order of default path list.
    ///
    /// # Priority:
    ///
    /// 1. (out of function) specified by `--config` option (If `--config` specified and cannot read the file, the program exit by error for security.)
    /// 1. `".pscr.conf"`
    /// 1. `".config/.pscr.conf"`
    /// 1. `"${HOME}/.pscr.conf"`
    /// 1. `"${HOME}/.config/.pscr.conf"`
    pub async fn read_from_default_path() -> Option<Self> {
        let paths = Self::default_path_list();

        for filepath in paths.into_iter() {
            match Self::read_from_file(&filepath).await {
                Ok(config) => {
                    return Some(config);
                }
                Err(_) => {
                    continue;
                }
            }
        }

        None
    }

    fn default_path_list() -> Vec<PathBuf> {
        let prefixs: Vec<_> = [PathBuf::from(".")].into_iter().chain(home_dir()).collect();
        let mut ret = vec![];

        for prefix in prefixs.into_iter() {
            for basepath in PATHS.iter() {
                let mut filepath = prefix.clone();
                filepath.push(basepath);
                ret.push(filepath);
            }
        }

        ret
    }
}

impl ConfigTrait for FileConfig {
    /// Returns HTTP Request Method ([`Method::Get`] or [`Method::Post`])
    fn method(&self) -> Option<Method> {
        self.pscr.method.clone()
    }

    /// Returns URL prefix of the PlantUML Server (e.g. `http://localhost:8080/`)
    fn url_prefix(&self) -> Option<String> {
        self.pscr.url_prefix.clone()
    }

    /// Returns output format ([`Format::Svg`] or [`Format::Png`] or [`Format::Ascii`] (`txt`))
    fn format(&self) -> Option<Format> {
        self.pscr.format.clone()
    }

    /// Returns whether to output a `combined` diagram.
    fn combined(&self) -> Option<bool> {
        self.pscr.combined
    }

    /// Returns whether to output a `combined` diagram.
    fn metadata_version(&self) -> Option<MetadataVersion> {
        self.pscr.metadata_version
    }
}

mod de {
    use serde::de::{Deserialize, Deserializer, Error as DeError};
    use std::str::FromStr;

    pub fn deserialize_option_from_str<'de, D, T>(deserializer: D) -> Result<Option<T>, D::Error>
    where
        D: Deserializer<'de>,
        T: FromStr,
        <T as FromStr>::Err: std::fmt::Display,
    {
        Option::<String>::deserialize(deserializer)?
            .map(|s| T::from_str(&s).map_err(DeError::custom))
            .transpose()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use itertools::Itertools;

    #[tokio::test]
    async fn test_de() -> anyhow::Result<()> {
        let method_list = [
            ("", None),
            (r#"method = "post""#, Some(Method::Post)),
            (r#"method = "get""#, Some(Method::Get)),
        ];

        let url_prefix_list = [
            ("", None),
            (
                r#"url-prefix = "http://localhost:8080/""#,
                Some("http://localhost:8080/"),
            ),
        ];

        let format_list = [
            ("", None),
            (r#"format = "svg""#, Some(Format::Svg)),
            (r#"format = "png""#, Some(Format::Png)),
            (r#"format = "txt""#, Some(Format::Ascii)),
        ];

        let combined_list = [
            ("", None),
            (r#"combined = false"#, Some(false)),
            (r#"combined = true"#, Some(true)),
        ];

        let metadata_version_list = [
            ("", None),
            (r#"metadata-version = "v2""#, Some(MetadataVersion::V2)),
        ];

        for ((((method, url_prefix), format), combined), metadata_version) in method_list
            .iter()
            .cartesian_product(url_prefix_list)
            .cartesian_product(format_list)
            .cartesian_product(combined_list)
            .cartesian_product(metadata_version_list)
        {
            let (method_line, expected_method) = method;
            let (url_prefix_line, expected_url_prefix) = url_prefix;
            let (format_line, expected_format) = format;
            let (combined_line, expected_combined) = combined;
            let (metadata_version_line, expected_metadata_format_version) = metadata_version;

            let testdata = format!(
                r#"
                [pscr]
                {method_line}
                {url_prefix_line}
                {format_line}
                {combined_line}
                {metadata_version_line}
                "#
            );

            println!("testdata: {testdata}");
            let config: FileConfig = toml::from_str(&testdata)?;

            assert_eq!(expected_method, &config.method());
            assert_eq!(expected_url_prefix, config.url_prefix().as_deref());
            assert_eq!(expected_format, config.format());
            assert_eq!(expected_combined, config.combined());
            assert_eq!(expected_metadata_format_version, config.metadata_version());
        }

        Ok(())
    }

    #[tokio::test]
    async fn test_default_path_list() -> anyhow::Result<()> {
        let paths = FileConfig::default_path_list();

        assert_eq!(paths[0], PathBuf::from("./.pscr.conf"));
        assert_eq!(paths[1], PathBuf::from("./.config/.pscr.conf"));

        if let Some(homedir) = home_dir() {
            let home = homedir.to_str().unwrap();

            let path: PathBuf = [home, ".pscr.conf"].iter().collect();
            assert_eq!(paths[2], path);

            let path: PathBuf = [home, ".config", ".pscr.conf"].iter().collect();
            assert_eq!(paths[3], path);
        }

        Ok(())
    }
}
