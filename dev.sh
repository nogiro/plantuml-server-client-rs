#!/usr/bin/env bash

set -u

docker compose up -d

cargo watch \
  -x fmt \
  -x build \
  -x 'doc --no-deps --workspace --document-private-items' \
  -x 'test --workspace' \
  -x 'clippy --workspace' \
  -s 'utility/test/main.sh'
