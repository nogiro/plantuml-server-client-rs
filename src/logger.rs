use anyhow::{Context as _, Result};
use tracing_subscriber::EnvFilter;

const ENV_NAME: &str = "PSCR_LOG";

/// Initializes tracing logger
///
/// * `level` - A log level. If the `PSCR_LOG` environment variable is given, `PSCR_LOG` is preferred and `level` is ignored.
///   * 0 - `""`
///   * 1 - `"info"`
///   * 2 - `"debug"`
///   * 3 and more - `"trace"`
///
/// releated: [`Args::verbose`][`crate::Args::verbose`]
pub fn init_logger(level: u8) -> Result<()> {
    let env_filter = env_filter(level);

    let subscriber = tracing_subscriber::fmt()
        .compact()
        .with_writer(std::io::stderr)
        .with_file(true)
        .with_line_number(true)
        .with_thread_ids(true)
        .with_target(true)
        .with_env_filter(env_filter)
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("tracing::subscriber::set_global_default(): failed")?;
    Ok(())
}

fn env_filter(level: u8) -> EnvFilter {
    let log_level = std::env::var(ENV_NAME).unwrap_or_else(|_| {
        match level {
            0 => "warn",
            1 => "info",
            2 => "debug",
            _ => "trace",
        }
        .to_string()
    });
    EnvFilter::new(log_level)
}
