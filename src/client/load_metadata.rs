use crate::client::request_single::RequestSingleClient;
use crate::{Config, Locate, PlantUmlMetadataItem};
use anyhow::{Context as _, Result, bail};
use plantuml_parser::{PlantUmlContent, PlantUmlFileData};
use std::path::PathBuf;

pub(super) struct LoadMetadataClient<'a> {
    pub config: &'a Config,
}

/// An owned type to calculate.
struct Content<'a>(&'a PlantUmlContent);

impl LoadMetadataClient<'_> {
    pub fn load_metadata(
        &self,
        data: &str,
        output_path: &Locate,
        combined: Option<PathBuf>,
        id: Option<String>,
        idx: usize,
    ) -> Result<PlantUmlMetadataItem> {
        let builder = PlantUmlMetadataItem::builder()
            .id(id)
            .index(idx)
            .output_path(output_path.to_path_buf());

        if self.config.metadata.is_none() {
            return Ok(builder.build());
        }

        let builder = builder.combined_output_path(combined);

        let filedata = self.parse(data)?;
        let content = Content(filedata.get(0).unwrap());

        let metadata = builder
            .titles(content.titles())
            .headers(content.headers())
            .footers(content.footers())
            .build();
        Ok(metadata)
    }

    fn parse(&self, data: &str) -> Result<PlantUmlFileData> {
        let filedata = PlantUmlFileData::parse_from_str(data)
            .map_err(|e| {
                tracing::error!("failed to parse PlantUML filedata for metadata: {e:?}");
                e
            })
            .context("failed to parse PlantUML filedata for metadata")?;

        if filedata.len() != 1 {
            tracing::error!(
                "unexpected state: combined PlantUml data does not have single content: len = {}",
                filedata.len()
            );
            bail!("unexpected state")
        }

        Ok(filedata)
    }
}

impl Content<'_> {
    fn titles(&self) -> Vec<String> {
        self.0.titles().iter().map(|x| x.title().into()).collect()
    }

    fn headers(&self) -> Vec<String> {
        self.0.headers().iter().map(|x| x.header().into()).collect()
    }

    fn footers(&self) -> Vec<String> {
        self.0.footers().iter().map(|x| x.footer().into()).collect()
    }
}

impl<'a> From<&'a RequestSingleClient<'a>> for LoadMetadataClient<'a> {
    fn from(client: &'a RequestSingleClient<'a>) -> Self {
        Self {
            config: client.config,
        }
    }
}
