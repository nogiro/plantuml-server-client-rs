mod block_comment_close;
mod block_comment_open;
mod empty;
mod end;
mod footer;
mod header;
mod include;
mod shared;
mod start;
mod title;

pub use block_comment_close::BlockCommentCloseLine;
pub use block_comment_open::BlockCommentOpenLine;
pub use empty::EmptyLine;
pub use end::EndLine;
pub use footer::FooterLine;
pub use header::HeaderLine;
pub use include::IncludeLine;
pub use start::StartLine;
pub use title::TitleLine;

use crate::{ParseContainer, ParseResult, wr};
use nom::branch::alt;
use nom::bytes::complete::take_till;
use nom::character::complete::line_ending;
use nom::combinator::eof;
use nom::{IResult, Parser};
use shared::LineWithComment;

/// A line of PlantUML
///
/// # Examples
///
/// ```
/// use plantuml_parser::PlantUmlLine;
///
/// # fn main() -> anyhow::Result<()> {
/// // StartLine
/// let input = "@startuml\n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.start().is_some());
/// assert!(parsed.diagram_kind().is_some()); // Returns only `StartLine`
///
/// // EndLine
/// let input = "@enduml\n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.end().is_some());
///
/// // IncludeLine
/// let input = "!include foo.puml \n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.include().is_some());
///
/// // TitleLine
/// let input = " title EXAMPLE TITLE\n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.title().is_some());
///
/// // HeaderLine
/// let input = " header EXAMPLE HEADER\n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.header().is_some());
///
/// // FooterLine
/// let input = " footer EXAMPLE FOOTER\n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.footer().is_some());
///
/// // EmptyLine
/// let input = "  ' comment \n";
/// let (rest, (_, parsed)) = PlantUmlLine::parse(input.into())?;
/// assert_eq!(rest, "");
/// assert_eq!(parsed.raw_str(), input);
/// assert!(parsed.empty().is_some());
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct PlantUmlLine {
    content: PlantUmlLineKind,
    raw: ParseContainer,
}

/// A kind of PlantUML lines to be handled by `plantuml-parser`. A line that cannot be handled is set to [`Others`][`PlantUmlLineKind::Others`].
#[derive(Clone, Debug)]
pub enum PlantUmlLineKind {
    /// `@start[a-z]*(id=MY_OWN_ID)` | `@start[a-z]* MY_OWN_ID` (e.g. `@startuml`)
    Start(StartLine),

    /// `@end[a-z]*` (e.g. `@enduml`)
    End(EndLine),

    /// * `"/' begin\n"`
    BlockCommentOpen(BlockCommentOpenLine),

    /// * `"end '/\n"`
    BlockCommentClose(BlockCommentCloseLine),

    /// `!include <filepath>` | `!include <filepath>!<id>` (e.g. `!include foo.puml!1`)
    Include(IncludeLine),

    /// `title <text>` (e.g. `title TITLE`)
    Title(TitleLine),

    /// `header <text>` (e.g. `header HEADER`)
    Header(HeaderLine),

    /// `footer <text>` (e.g. `footer FOOTER`)
    Footer(FooterLine),

    /// empty line
    Empty,

    /// in comment
    InComment(Box<PlantUmlLineKind>),

    /// others
    Others,
}

impl PlantUmlLine {
    /// Tries to parse [`PlantUmlLine`].
    pub fn parse(input: ParseContainer) -> ParseResult<Self> {
        let (rest, (raw, content)) = PlantUmlLineKind::parse(input.clone())?;
        let ret = (raw.clone(), Self { content, raw });
        Ok((rest, ret))
    }

    /// Returns the strings.
    pub fn raw_str(&self) -> &str {
        self.raw.as_str()
    }

    /// Returns the [`PlantUmlLineKind`].
    pub fn kind(&self) -> &PlantUmlLineKind {
        &self.content
    }

    /// Returns the [`StartLine`] if it is [`StartLine`].
    pub fn start(&self) -> Option<&StartLine> {
        if let PlantUmlLineKind::Start(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`EndLine`] if it is [`EndLine`].
    pub fn end(&self) -> Option<&EndLine> {
        if let PlantUmlLineKind::End(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`BlockCommentOpenLine`] if it is [`BlockCommentOpenLine`].
    pub fn block_comment_open(&self) -> Option<&BlockCommentOpenLine> {
        if let PlantUmlLineKind::BlockCommentOpen(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`BlockCommentCloseLine`] if it is [`BlockCommentCloseLine`].
    pub fn block_comment_close(&self) -> Option<&BlockCommentCloseLine> {
        if let PlantUmlLineKind::BlockCommentClose(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`IncludeLine`] if it is [`IncludeLine`].
    pub fn include(&self) -> Option<&IncludeLine> {
        if let PlantUmlLineKind::Include(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`TitleLine`] if it is [`TitleLine`].
    pub fn title(&self) -> Option<&TitleLine> {
        if let PlantUmlLineKind::Title(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`HeaderLine`] if it is [`HeaderLine`].
    pub fn header(&self) -> Option<&HeaderLine> {
        if let PlantUmlLineKind::Header(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the [`FooterLine`] if it is [`FooterLine`].
    pub fn footer(&self) -> Option<&FooterLine> {
        if let PlantUmlLineKind::Footer(x) = &self.content {
            Some(x)
        } else {
            None
        }
    }

    /// Returns the kind of the diagram if it is [`StartLine`].
    pub fn diagram_kind(&self) -> Option<&str> {
        self.start().map(|x| x.diagram_kind())
    }

    /// Returns `Some(())` if it is [`EmptyLine`].
    pub fn empty(&self) -> Option<()> {
        if let PlantUmlLineKind::Empty = &self.content {
            Some(())
        } else {
            None
        }
    }

    /// Convert into [`PlantUmlLineKind::InComment`].
    pub fn into_in_comment(self) -> Self {
        Self {
            content: PlantUmlLineKind::InComment(Box::new(self.content)),
            raw: self.raw,
        }
    }
}

impl PlantUmlLineKind {
    fn parse(input: ParseContainer) -> ParseResult<Self> {
        if let Ok((rest, (raw, content))) = StartLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Start(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = EndLine::parse(input.clone()) {
            let content = PlantUmlLineKind::End(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = BlockCommentOpenLine::parse(input.clone()) {
            let content = PlantUmlLineKind::BlockCommentOpen(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = BlockCommentCloseLine::parse(input.clone()) {
            let content = PlantUmlLineKind::BlockCommentClose(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = IncludeLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Include(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = TitleLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Title(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = HeaderLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Header(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, content))) = FooterLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Footer(content);
            return Ok((rest, (raw, content)));
        }

        if let Ok((rest, (raw, _))) = EmptyLine::parse(input.clone()) {
            let content = PlantUmlLineKind::Empty;
            return Ok((rest, (raw, content)));
        }

        let (rest, raws) = Self::parse_others(input.clone())?;
        let content = PlantUmlLineKind::Others;
        Ok((rest, (raws.into(), content)))
    }

    fn parse_others(input: ParseContainer) -> IResult<ParseContainer, Vec<ParseContainer>> {
        let (rest, (not_end, end)) = (
            wr!(take_till(|c| c == '\n' || c == '\r')),
            alt((wr!(eof), wr!(line_ending))),
        )
            .parse(input)?;
        Ok((rest, vec![not_end, end]))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_start_line() -> anyhow::Result<()> {
        let testdata = "@startuml\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        let testdata = " \t@startuml\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        let testdata = "@startuml \t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        let testdata = " \t@startuml \t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        let testdata = " \t@startuml id_foo\t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        let testdata = " \t@startuml(id=id_bar)\t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.start().is_some());

        Ok(())
    }

    #[test]
    fn test_parse_end_line() -> anyhow::Result<()> {
        let testdata = "@enduml\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.end().is_some());

        let testdata = " \t@enduml\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.end().is_some());

        let testdata = "@enduml \t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.end().is_some());

        let testdata = " \t@enduml \t\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.end().is_some());

        Ok(())
    }

    #[test]
    fn test_parse_include_line() -> anyhow::Result<()> {
        let testdata = "!include foo.puml\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.include().is_some());

        let testdata = " !include foo.puml!1\n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.include().is_some());

        let testdata = "\t!include foo.puml!bar \n";
        let (rest, (_, parsed)) = PlantUmlLine::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(testdata, parsed.raw);
        assert!(parsed.include().is_some());

        Ok(())
    }
}
