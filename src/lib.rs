#![doc = include_str!("../docs/include/about.md")]
//!
//! # Getting Started
//!
//! Request to [`http://www.plantuml.com/plantuml/`](http://www.plantuml.com/plantuml/).
//!
//! ```shell
//! cargo install plantuml-server-client-rs
//! {
//!   echo '@startuml'
//!   echo 'Bob -> Alice : hello'
//!   echo '@enduml'
//! } | plantuml-server-client > out.svg
//! ```
//!
//! # Usages
//!
//! See [`README (crates.io)`](https://crates.io/crates/plantuml-server-client-rs)

mod client;
mod collector;
mod config;
mod encode;
mod format;
mod locate;
mod logger;
mod metadata;
mod mode;
mod request;
mod url;

pub use client::Client;
pub use collector::{CollectedContainer, Collector};
pub use config::Config;
pub use config::args::Args;
pub use config::file::FileConfig;
pub use format::Format;
pub use locate::Locate;
pub use logger::init_logger;
pub use metadata::{
    IncludesMetadata, IncludesMetadataItem, Metadata, MetadataItem, MetadataV2, MetadataVersion,
    PlantUmlMetadata, PlantUmlMetadataItem,
};
pub use mode::Mode;
pub use request::method::Method;
pub use url::{URL_PREFIX, UrlBuilder};

/// The package name from Cargo.toml: [`plantuml-server-client-rs`](https://docs.rs/plantuml-server-client-rs/)
pub const PKG_NAME: &str = env!("CARGO_PKG_NAME");
/// The package version from Cargo.toml
pub const PKG_VERSION: &str = env!("CARGO_PKG_VERSION");
