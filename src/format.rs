/// A representation of output format of PlantUML Server.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Format {
    /// PNG file
    Png,
    /// SVG XML file
    Svg,
    /// ASCII Art representation
    Ascii,
}

impl From<&Format> for &'static str {
    /// Converts to URL path parts or extension
    fn from(format: &Format) -> &'static str {
        match format {
            Format::Png => "png",
            Format::Svg => "svg",
            Format::Ascii => "txt",
        }
    }
}

impl std::fmt::Display for Format {
    /// Converts to URL path parts or extension
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let x: &str = self.into();
        x.fmt(f)
    }
}

impl std::str::FromStr for Format {
    type Err = anyhow::Error;
    /// Converts frin URL path parts or extension
    fn from_str(s: &str) -> anyhow::Result<Self> {
        match s.to_string().to_lowercase().as_str() {
            "png" => Ok(Format::Png),
            "svg" => Ok(Format::Svg),
            "txt" => Ok(Format::Ascii),
            _ => anyhow::bail!("unsupported format"),
        }
    }
}
