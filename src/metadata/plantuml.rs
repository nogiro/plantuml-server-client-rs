use derive_getters::Getters;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use typed_builder::TypedBuilder;

/// Information in the generated PlantUML diagrams
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct PlantUmlMetadata(Vec<PlantUmlMetadataItem>);

/// Information in the generated PlantUML diagram
///
/// * `index` - The index of the order of the diagrams in the file
/// * `id` - The id of the diagram
/// * `output_path` - The path of output file
/// * `combined_output_path` - The path of `combined` PlantUML content's output file
/// * `title` - The last title in the PlantUML content
/// * `titles` - The titles in the PlantUML content
/// * `header` - The last header in the PlantUML content
/// * `headers` - The headers in the PlantUML content
/// * `footer` - The last footer in the PlantUML content
/// * `footers` - The footers in the PlantUML content
#[derive(Deserialize, Serialize, TypedBuilder, Getters, Clone, Debug)]
#[builder(mutators(
    pub fn titles(&mut self, titles: Vec<String>){
        self.title = titles.last().cloned();
        self.titles = titles;
    }

    pub fn headers(&mut self, headers: Vec<String>){
        self.header = headers.last().cloned();
        self.headers = headers;
    }

    pub fn footers(&mut self, footers: Vec<String>){
        self.footer = footers.last().cloned();
        self.footers = footers;
    }
))]
pub struct PlantUmlMetadataItem {
    index: usize,
    #[serde(skip_serializing_if = "Option::is_none")]
    id: Option<String>,

    output_path: PathBuf,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    combined_output_path: Option<PathBuf>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(via_mutators(init = None))]
    title: Option<String>,
    #[builder(via_mutators(init = Vec::new()))]
    titles: Vec<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(via_mutators(init = None))]
    header: Option<String>,
    #[builder(via_mutators(init = Vec::new()))]
    headers: Vec<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(via_mutators(init = None))]
    footer: Option<String>,
    #[builder(via_mutators(init = Vec::new()))]
    footers: Vec<String>,
}

impl PlantUmlMetadata {
    /// Returns a reference to an item of index.
    pub fn get(&self, index: usize) -> Option<&PlantUmlMetadataItem> {
        self.0.get(index)
    }

    /// Returns `true` if the metadata has a length of 0.
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns the number of elements in the metadata.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns an iterator over the metadata item.
    pub fn iter(&self) -> impl std::iter::Iterator<Item = &PlantUmlMetadataItem> {
        self.0.iter()
    }
}

impl From<Vec<PlantUmlMetadataItem>> for PlantUmlMetadata {
    fn from(inner: Vec<PlantUmlMetadataItem>) -> Self {
        Self(inner)
    }
}

impl PlantUmlMetadataItem {
    /// An identifier of the diagram
    pub fn identifier(&self) -> String {
        self.id.clone().unwrap_or_else(|| self.index.to_string())
    }
}
