use crate::Error;
use crate::ParseContainer;
use crate::PathResolver;
use crate::PlantUmlFileData;
use crate::{
    FooterLine, HeaderLine, IncludeToken, PlantUmlBlock, PlantUmlBlockKind, PlantUmlLine,
    PlantUmlLineKind, StartLine, TitleLine,
};
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;

/// A token sequence that is a single PlantUML diagram, that is, from the [`StartLine`] to the [`EndLine`][`crate::EndLine`] (lines inclusive).
///
/// # Examples
///
/// ```
/// use plantuml_parser::{IncludesCollections, PlantUmlContent, PlantUmlFileData};
/// # use std::collections::HashMap;
///
/// # fn main() -> anyhow::Result<()> {
/// let data_for_include = r#"
/// @startuml
/// title Example Title Included
/// Bob -> Alice: Hi
/// @enduml
/// "#;
///
/// let data = r#"
/// @startuml
/// title Example Title
/// Alice -> Bob: Hello
/// !include foo.puml!0
/// @enduml
/// "#;
///
/// let filedata = PlantUmlFileData::parse_from_str(data)?;
/// let content: &PlantUmlContent = filedata.get(0).unwrap();
///
/// // Parsed and collected `!include`, `title`, `header`, `footer`
/// let include_0 = content.includes().get(0).unwrap();
/// assert!(content.includes().get(1).is_none());
/// assert_eq!(content.includes().len(), 1);
/// assert_eq!(include_0.filepath(), "foo.puml");
/// assert_eq!(include_0.index(), Some(0));
/// assert_eq!(include_0.id(), Some("0"));
///
/// let title_0 = content.titles().get(0).unwrap();
/// assert!(content.titles().get(1).is_none());
/// assert_eq!(content.titles().len(), 1);
/// assert_eq!(title_0.title(), "Example Title");
///
/// // `construct()`
/// let filedata_for_include = PlantUmlFileData::parse_from_str(data_for_include)?;
/// let includes = IncludesCollections::new(HashMap::from([
///     ("bar/foo.puml".into(), filedata_for_include),
/// ]));
/// let constructed = content.construct("bar/x.puml".into(), &includes)?;
/// assert_eq!(
///     constructed,
///     concat!(
///         "@startuml\n",
///         "title Example Title\n",
///         "Alice -> Bob: Hello\n",
///         "title Example Title Included\n",
///         "Bob -> Alice: Hi\n",
///         "@enduml\n",
///     ),
/// );
///
/// // Increased the element of titles in `constructed`
/// let filedata = PlantUmlFileData::parse_from_str(constructed)?;
/// let content: &PlantUmlContent = filedata.get(0).unwrap();
/// let title_0 = content.titles().get(0).unwrap();
/// let title_1 = content.titles().get(1).unwrap();
/// assert!(content.titles().get(2).is_none());
/// assert_eq!(content.titles().len(), 2);
/// assert_eq!(title_0.title(), "Example Title");
/// assert_eq!(title_1.title(), "Example Title Included");
/// # Ok(())
/// # }
/// ```
#[derive(Clone, Debug)]
pub struct PlantUmlContent {
    lines: Vec<Arc<PlantUmlLine>>,
    blocks: Vec<PlantUmlBlock>,
    includes: Vec<IncludeToken>,
    titles: Vec<TitleLine>,
    headers: Vec<HeaderLine>,
    footers: Vec<FooterLine>,
}

/// Data collected on the file path and PlantUML diagrams in the file for the include process.
#[derive(Clone, Debug)]
pub struct IncludesCollections(HashMap<PathBuf, PlantUmlFileData>);

impl PlantUmlContent {
    /// Tries to parse a single PlantUML diagram, that is, from the [`StartLine`] to the [`EndLine`][`crate::EndLine`] (lines inclusive).
    pub(crate) fn parse(input: ParseContainer) -> Result<(ParseContainer, Self), Error> {
        let mut lines = vec![];
        let mut blocks = vec![];
        let mut includes = vec![];
        let mut titles = vec![];
        let mut headers = vec![];
        let mut footers = vec![];

        let (mut rest, (_, mut line)) = PlantUmlLine::parse(input.clone())?;
        if line.start().is_none() {
            StartLine::parse(input.clone())?;
            // unreachable
            return Err(Error::Unreachable("The first line is not `StartLine` provided to `PlantUmlContent::parse()`, but `PlantUmlContent::parse()` is `pub(crate)`.".into()));
        }
        let start_line = line.clone();
        let diagram_kind = start_line.diagram_kind().unwrap();
        lines.push(Arc::new(line.clone()));

        while line.end().is_none() && !rest.is_empty() {
            // TODO: refactor: too specialized to treat `BlockComment`
            match PlantUmlBlock::parse(rest.clone()) {
                Ok((tmp_rest, (_, block))) => {
                    rest = tmp_rest;
                    blocks.push(block.clone());

                    match block.kind() {
                        PlantUmlBlockKind::BlockComment(comment) => {
                            let comment_lines = comment.lines().to_vec();
                            lines.extend(comment_lines);
                            continue;
                        }
                    }
                }
                _ => { // do nothing
                }
            }

            let (tmp_rest, (_, tmp_line)) = PlantUmlLine::parse(rest)?;
            (rest, line) = (tmp_rest, tmp_line);
            if line.end().map(|x| x.eq_diagram_kind(diagram_kind)) == Some(false) {
                let start_line = start_line.start().cloned().unwrap();
                let end_line = line.end().cloned().unwrap();
                return Err(Error::DiagramKindNotMatch(start_line, end_line));
            }

            match line.kind() {
                PlantUmlLineKind::Include(line) => {
                    includes.push(line.token().clone());
                }

                PlantUmlLineKind::Title(line) => {
                    titles.push(line.clone());
                }

                PlantUmlLineKind::Header(line) => {
                    headers.push(line.clone());
                }

                PlantUmlLineKind::Footer(line) => {
                    footers.push(line.clone());
                }

                _ => {}
            }

            lines.push(Arc::new(line.clone()));
        }

        if rest.is_empty() && line.end().is_none() {
            Err(Error::ContentUnclosed("".into()))
        } else {
            let ret = Self {
                lines,
                blocks,
                includes,
                titles,
                headers,
                footers,
            };

            Ok((rest, ret))
        }
    }

    /// Returns the ID of the diagram.
    pub fn id(&self) -> Option<&str> {
        self.lines[0].start().unwrap().id()
    }

    /// Returns the string after the include process of [`PlantUmlContent`].
    ///
    /// * `base` - A base path of `self`.
    /// * `includes` - A pre read [`PlantUmlFileData`] collection for the include process.
    ///
    /// # Examples
    ///
    /// ```
    /// use plantuml_parser::{IncludesCollections, PlantUmlContent, PlantUmlFileData};
    /// # use std::collections::HashMap;
    ///
    /// # fn main() -> anyhow::Result<()> {
    /// let data_for_include = r#"
    /// @startuml
    /// Bob -> Alice: Hi
    /// @enduml
    /// "#;
    ///
    /// let data = r#"
    /// @startuml
    /// Alice -> Bob: Hello
    /// !include foo.puml!0
    /// @enduml
    /// "#;
    ///
    /// let filedata_for_include = PlantUmlFileData::parse_from_str(data_for_include)?;
    /// let includes = IncludesCollections::new(HashMap::from([
    ///     ("bar/foo.puml".into(), filedata_for_include),
    /// ]));
    ///
    /// let filedata = PlantUmlFileData::parse_from_str(data)?;
    /// let content: &PlantUmlContent = filedata.get(0).unwrap();
    /// let constructed = content.construct("bar/x.puml".into(), &includes)?;
    /// assert_eq!(
    ///     constructed,
    ///     concat!(
    ///         "@startuml\n",
    ///         "Alice -> Bob: Hello\n",
    ///         "Bob -> Alice: Hi\n",
    ///         "@enduml\n",
    ///     ),
    /// );
    ///
    /// // include paths are not matched
    /// let constructed = content.construct("bar.puml".into(), &includes)?;
    /// assert_eq!(
    ///     constructed,
    ///     concat!(
    ///         "@startuml\n",
    ///         "Alice -> Bob: Hello\n",
    ///         "!include foo.puml!0\n",
    ///         "@enduml\n",
    ///     ),
    /// );
    /// # Ok(())
    /// # }
    /// ```
    pub fn construct(
        &self,
        base: PathBuf,
        includes: &IncludesCollections,
    ) -> Result<String, Error> {
        let constructed = [
            self.lines[0].raw_str(),
            &self.construct_inner(base, includes)?,
            self.lines[self.lines.len() - 1].raw_str(),
        ]
        .join("");
        Ok(constructed)
    }

    fn construct_inner(
        &self,
        base: PathBuf,
        includes: &IncludesCollections,
    ) -> Result<String, Error> {
        let resolver = PathResolver::new(base);

        let constructed = self.lines[1..self.lines.len() - 1]
            .iter()
            .map(|x| {
                let Some(include) = x.include() else {
                    // line is not `!include`
                    return Ok(x.raw_str().to_string());
                };

                let mut inner_resolver = resolver.clone();
                inner_resolver.add(include.filepath().into());
                let path = inner_resolver.build()?;

                let Some(data) = includes.0.get(&path) else {
                    tracing::info!("file not loaded: path = {path:?}");
                    // file not found
                    return Ok(x.raw_str().to_string());
                };

                let Some(content) = data.get_by_token(include.token()) else {
                    tracing::info!(
                        "specified content not found in file: path = {path:?}, token.id = {:?}, token.index = {:?}",
                        include.token().id(),
                        include.token().index(),
                    );
                    // specified `PlantUmlContent` not found
                    return Ok(x.raw_str().to_string());
                };

                // recursive construct
                content.construct_inner(path, includes)
            })
            .collect::<Result<Vec<_>, Error>>()?
            .join("");

        Ok(constructed)
    }

    /// Returns the string in the [`PlantUmlContent`] without [`StartLine`] and [`EndLine`][`crate::EndLine`].
    ///
    /// # Examples
    ///
    /// ```
    /// use plantuml_parser::{PlantUmlContent, PlantUmlFileData};
    ///
    /// # fn main() -> anyhow::Result<()> {
    /// let data = r#"
    /// @startuml
    /// Alice -> Bob: Hello
    /// @enduml
    /// "#;
    ///
    /// let filedata = PlantUmlFileData::parse_from_str(data)?;
    /// let content: &PlantUmlContent = filedata.get(0).unwrap();
    /// assert_eq!(content.inner(), "Alice -> Bob: Hello\n");
    /// # Ok(())
    /// # }
    /// ```
    pub fn inner(&self) -> String {
        self.lines[1..self.lines.len() - 1]
            .iter()
            .map(|x| x.raw_str())
            .collect::<Vec<_>>()
            .join("")
    }

    /// Returns the list of [`PlantUmlBlock`] in the [`PlantUmlContent`].
    pub fn blocks(&self) -> &[PlantUmlBlock] {
        &self.blocks
    }

    /// Returns the includes in the [`PlantUmlContent`].
    pub fn includes(&self) -> &[IncludeToken] {
        &self.includes
    }

    /// Returns the titles in the [`PlantUmlContent`].
    pub fn titles(&self) -> &[TitleLine] {
        &self.titles
    }

    /// Returns the headers in the [`PlantUmlContent`].
    pub fn headers(&self) -> &[HeaderLine] {
        &self.headers
    }

    /// Returns the footers in the [`PlantUmlContent`].
    pub fn footers(&self) -> &[FooterLine] {
        &self.footers
    }
}

impl IncludesCollections {
    /// Creates a new [`IncludesCollections`].
    pub fn new(inner: HashMap<PathBuf, PlantUmlFileData>) -> Self {
        Self(inner)
    }
}

impl From<HashMap<PathBuf, PlantUmlFileData>> for IncludesCollections {
    fn from(inner: HashMap<PathBuf, PlantUmlFileData>) -> Self {
        Self(inner)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_plant_uml_content() -> anyhow::Result<()> {
        // OK
        let testdata = r#"@startuml
        @enduml"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed.lines.len(), 2);
        assert_eq!(parsed.inner(), "");

        // OK
        let testdata = r#"@startuml
            a -> b
        @enduml"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed.lines.len(), 3);
        assert_eq!(parsed.inner(), "            a -> b\n");

        // OK (with id)
        let testdata = r#"@startuml aaa
            a -> b
        @enduml"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed.lines.len(), 3);
        assert_eq!(parsed.inner(), "            a -> b\n");
        assert_eq!(parsed.id(), Some("aaa"));

        // OK (with id 2)
        let testdata = r#"@startuml(id=bbb)
            a -> b
        @enduml"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed.lines.len(), 3);
        assert_eq!(parsed.inner(), "            a -> b\n");
        assert_eq!(parsed.id(), Some("bbb"));

        // NG. diagram_kind not match
        let testdata = r#"@startuml
            a -> b
        @endfoo"#;
        assert!(PlantUmlContent::parse(testdata.into()).is_err());

        // OK
        let testdata = r#"@startuml(id=ccc)
            a -> b
            !include shared.iuml
            a -> b
            !include functions.puml!aaa
            a -> b
        @enduml"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        assert_eq!(parsed.lines.len(), 7);
        assert_eq!(
            parsed.inner(),
            "            a -> b\n            !include shared.iuml\n            a -> b\n            !include functions.puml!aaa\n            a -> b\n"
        );
        assert_eq!(parsed.id(), Some("ccc"));
        let mut includes = parsed.includes().iter();
        let include = includes.next().unwrap();
        assert_eq!(include.filepath(), "shared.iuml");
        assert_eq!(include.index(), None);
        assert_eq!(include.id(), None);
        let include = includes.next().unwrap();
        assert_eq!(include.filepath(), "functions.puml");
        assert_eq!(include.index(), None);
        assert_eq!(include.id(), Some("aaa"));
        assert!(includes.next().is_none());

        Ok(())
    }

    #[test]
    fn test_parse_block_comment() -> anyhow::Result<()> {
        // OK
        let testdata = r#"@startuml(id=ccc)
            a -> b: 1
            /' begin
            !include shared.iuml
            end '/
            a -> b: 2
            !include functions.puml!aaa
            a -> b: 3
        @enduml"#;
        let expected_inner = r#"            a -> b: 1
            /' begin
            !include shared.iuml
            end '/
            a -> b: 2
            !include functions.puml!aaa
            a -> b: 3
"#;
        let (rest, parsed) = PlantUmlContent::parse(testdata.into())?;
        assert_eq!(rest, "");
        println!("parsed.blocks() = {:?}", parsed.blocks());
        for line in parsed.lines.iter() {
            println!("parsed.lines[].kind() = {:?}", line.kind());
            println!("parsed.lines[].raw_str() = {}", line.raw_str());
        }
        assert_eq!(parsed.lines.len(), 9);
        assert_eq!(parsed.inner(), expected_inner);
        assert_eq!(parsed.id(), Some("ccc"));
        let mut includes = parsed.includes().iter();
        let include = includes.next().unwrap();
        assert_eq!(include.filepath(), "functions.puml");
        assert_eq!(include.index(), None);
        assert_eq!(include.id(), Some("aaa"));
        assert!(includes.next().is_none());

        Ok(())
    }
}
