The parser for [`plantuml-server-client`](https://docs.rs/plantuml-server-client-rs/)

# Examples

```
use anyhow::Result;
use plantuml_parser::{IncludesCollections, PlantUmlFileData};
use std::collections::HashMap;

fn main() -> Result<()> {
    // Multiple PlantUml contents
    let data = r#"
        @startuml diagram_0
        Alice -> Bob: Hello
        @enduml

        @startuml
        !include foo.puml!diagram_0
        Bob -> Alice: Hi
        @enduml
    "#;

    // Parses string
    let parsed = PlantUmlFileData::parse_from_str(data)?;

    // Parsed 1st content
    let parsed0 = parsed.get(0).unwrap();
    let empty = IncludesCollections::new(HashMap::new());
    assert_eq!(
        parsed0.construct(".".into(), &empty)?,
        concat!(
            "        @startuml diagram_0\n",
            "        Alice -> Bob: Hello\n",
            "        @enduml\n",
        )
    );
    assert_eq!(parsed0.inner(), "        Alice -> Bob: Hello\n");
    // Parsed 2nd content
    let parsed1 = parsed.get(1).unwrap();
    assert_eq!(
        parsed1.inner(),
        concat!(
            "        !include foo.puml!diagram_0\n",
            "        Bob -> Alice: Hi\n",
        )
    );

    // Embeds 1st content in 2nd content's `!include` line.
    let includes = IncludesCollections::new(HashMap::from([("foo.puml".into(), parsed.clone())]));
    let constructed = parsed1.construct("base.puml".into(), &includes)?;
    assert_eq!(
        constructed,
        concat!(
            "        @startuml\n",
            "        Alice -> Bob: Hello\n",
            "        Bob -> Alice: Hi\n",
            "        @enduml\n",
        )
    );

    Ok(())
}
```
