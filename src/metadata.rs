mod format_version;
mod include;
mod plantuml;

pub use format_version::MetadataVersion;
pub use include::{IncludesMetadata, IncludesMetadataItem};
pub use plantuml::{PlantUmlMetadata, PlantUmlMetadataItem};

use derive_getters::Getters;
use format_version::{MetadataVersionV1, MetadataVersionV2};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use typed_builder::TypedBuilder;

/// A metadata (v1) collected by `plantuml-server-client` during processing
///
/// * (deprecated: v1 format) `path` - the path of the input file
/// * (deprecated: v1 format) `include` - the metadata about files collected for includes
/// * (deprecated: v1 format) `plantuml` - the information in the generated plantuml diagrams
/// * `files` - The information in the generated PlantUML diagrams
#[derive(Deserialize, Serialize, TypedBuilder, Getters, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Metadata {
    #[builder(default)]
    #[serde(default)]
    format_version: MetadataVersionV1,

    path: PathBuf,
    includes: IncludesMetadata,
    plantuml: PlantUmlMetadata,

    #[serde(default)]
    files: Vec<MetadataItem>,
}

/// A metadata collected by `plantuml-server-client` during processing
///
/// * `files` - The information in the generated PlantUML diagrams
#[derive(Deserialize, Serialize, TypedBuilder, Getters, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MetadataV2 {
    #[builder(default)]
    #[serde(default)]
    format_version: MetadataVersionV2,

    #[serde(default)]
    files: Vec<MetadataItem>,
}

/// A metadata collected by `plantuml-server-client` during processing
///
/// * `path` - the path of the input file
/// * `include` - the metadata about files collected for includes
/// * `plantuml` - the information in the generated plantuml diagrams
#[derive(Deserialize, Serialize, TypedBuilder, Getters, Clone, Debug)]
pub struct MetadataItem {
    path: PathBuf,
    includes: IncludesMetadata,
    plantuml: PlantUmlMetadata,
}

impl From<Vec<MetadataItem>> for MetadataV2 {
    fn from(files: Vec<MetadataItem>) -> Self {
        Self {
            format_version: MetadataVersionV2::V2,
            files,
        }
    }
}

impl From<MetadataV2> for Metadata {
    fn from(metadata: MetadataV2) -> Self {
        let MetadataItem {
            path,
            includes,
            plantuml,
        } = metadata.files.last().unwrap().clone(); // SAFETY: `unwrap()` is safe here since there is always at least one input file.

        Self {
            format_version: MetadataVersionV1::V1,
            path,
            includes,
            plantuml,
            files: metadata.files,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test() -> anyhow::Result<()> {
        let testdata = r#"
            {
                "formatVersion": "v1",
                "path": "./root-path.puml",
                "includes": [
                    {
                        "path": "./include_1.iuml",
                        "normalized_path": "include_1.iuml",
                        "base_path": "root-path.puml",
                        "relative_path": "include_1.iuml",
                        "id": "include_1"
                    }
                ],
                "plantuml": [
                    {
                        "index": 0,
                        "id": "figure_1",
                        "output_path": "./output_1.svg",
                        "titles": [],
                        "headers": [],
                        "footers": []
                    },
                    {
                        "index": 1,
                        "id": "figure_2",
                        "output_path": "./output_2.svg",
                        "titles": [],
                        "headers": [],
                        "footers": []
                    }
                ]
            }"#;
        serde_json::from_str::<Metadata>(&testdata)?;

        Ok(())
    }
}
